package gusl.cassandra.migrate;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import gusl.cassandra.utils.CassandraEvolve;

/**
 *
 * @author dhudson
 */
public class DrawMigrateR8 implements CassandraEvolve {

    public DrawMigrateR8() {
    }

    @Override
    public void evolve(Session session) {
        ResultSet result = session.execute("select * from main.lotto_draw");
        Row row = result.one();
        if (row != null) {
            // Its populated, so grumble ..
            String name = row.getString("name");
            if (name == null) {
                System.err.println(" ****************   Need to populate lotto-draw with lotto   *************************** ");
            }
        }
    }

}
