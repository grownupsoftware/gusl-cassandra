package gusl.cassandra.schema;

import gusl.cassandra.builder.codecs.CodecRegistra;

/**
 *
 * @author grant
 */
public abstract class AbstractSchema implements Schema {

    @Override
    public void registerCodecs(CodecRegistra codecRegistra) {
    }

}
