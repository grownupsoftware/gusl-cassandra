package gusl.cassandra.schema;

import gusl.cassandra.builder.CassandraContext;
import gusl.cassandra.builder.CassandraDAO;
import gusl.cassandra.builder.Table;
import gusl.cassandra.builder.codecs.CodecRegistra;
import gusl.cassandra.config.CassandraConnectionConfig;
import gusl.cassandra.utils.CassandraHelper;
import gusl.core.exceptions.GUSLException;
import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.util.*;

import static gusl.core.utils.Utils.safeStream;

/**
 * @author grant
 */
@Service
@CustomLog
public class SchemaControllerImpl implements SchemaController {

    private final Map<String, CassandraContext> theCassandraContextMap = new HashMap<>();
    private final Map<String, Schema> theSchemaMap = new HashMap<>();

    private List<CassandraConnectionConfig> theCassandraConnectionConfigs;

    private final Set<NodeModule> loadedNodeModules = new HashSet<>();

    private List<String> theKeyspaces;

    public List<Table> getTables() {
        return new ArrayList<>(tables.values());
    }

    private final Map<String, Table> tables = new HashMap<>();

    private CodecRegistra theCodecRegistra;

    @Override
    public void configure(List<CassandraConnectionConfig> cassandraConnectionConfigs, String... keyspaces) {
        logger.info("[{}]-- Configuring {} Using: {}", this.hashCode(), Arrays.asList(keyspaces), cassandraConnectionConfigs);

        this.theCassandraConnectionConfigs = cassandraConnectionConfigs;
        if (keyspaces == null) {
            logger.error("Must specify the nodetype(s) that must be loaded");
            throw new RuntimeException("Must specify the nodetype(s) that must be loaded");
        }
        theKeyspaces = Arrays.asList(keyspaces);
    }

    public void destroy() {
        safeStream(theCassandraContextMap.entrySet()).forEach(entry -> {
            entry.getValue().close();
        });
    }

    @Override
    public void loadSchemas(List<Schema> schemaInstances,
                            List<CassandraDAO> daoInstances,
                            List<Table> tableInstances) {
        if (theKeyspaces == null) {
            logger.error("Schema Controller has not been configured");
            throw new RuntimeException("Schema Controller has not been configured");
        }

        // This is done at a cluster level and not keyspace
        theCodecRegistra = new CodecRegistra();

        for (String keyspace : theKeyspaces) {
            Optional<CassandraConnectionConfig> optional = safeStream(theCassandraConnectionConfigs)
                    .filter(connectionConfig -> connectionConfig.getKeyspace().equals(keyspace.toLowerCase())).findFirst();
            if (optional.isPresent()) {
                loadSchema(keyspace, optional.get(), schemaInstances, daoInstances, tableInstances);
            } else {
                logger.error("Schema Controller failed to find a matching keyspace in the config when loading  nodeType: {} keyspace: {}", keyspace, keyspace.toLowerCase());
            }
        }

        // This is done at cluster level
        CassandraContext context = getCassandraContext(theKeyspaces.get(0));
        theCodecRegistra.registerWithCassandra(context.getSession().getCluster(), context.getSession().getCluster().getConfiguration().getCodecRegistry());
    }

    public void loadSchema(String keyspace,
                           CassandraConnectionConfig CassandraConnectionConfig,
                           List<Schema> schemaInstances,
                           List<CassandraDAO> daoInstances,
                           List<Table> tableInstances) {
        loadSchema(keyspace, CassandraConnectionConfig, null, schemaInstances, daoInstances, tableInstances);
    }

    public void loadSchema(String keyspace, CassandraConnectionConfig cassandraConnectionConfig, String module,
                           List<Schema> schemaInstances,
                           List<CassandraDAO> daoInstances,
                           List<Table> tableInstances
    ) {

        while (!CassandraHelper.checkCassandraConnection(cassandraConnectionConfig)) {
            logger.warn("Waiting for Cassandra to start ... Will retry in 2 seconds ...");
            Utils.sleep(2000);
        }

        NodeModule nodeModule = new NodeModule(keyspace, module);

        if (loadedNodeModules.contains(nodeModule)) {
            logger.info("Schema for Node: {} module: {} - is already loaded", keyspace, module);
            return;
        }

        // step 1 - scan for schema definitions and bootstrap the tables
        StringBuilder builder = scanForSchemasDefinitions(keyspace, module, schemaInstances);

        // step 2 - create a cassandra context for this node
        CassandraContext cassandraContext = theCassandraContextMap.get(cassandraConnectionConfig.getKeyspace());
        if (cassandraContext == null) {
            cassandraContext = new CassandraContext();
            cassandraContext.configure(cassandraConnectionConfig);
            theCassandraContextMap.put(cassandraConnectionConfig.getKeyspace(), cassandraContext);
        }

        Schema schema = theSchemaMap.get(keyspace);
        if (schema != null) {
            schema.registerCodecs(theCodecRegistra);
        }

        // step 3 - scan tables and validate columns
        builder.append(scanForTables(keyspace, module, cassandraContext, tableInstances));

        long start = System.currentTimeMillis();
        // step 4 - scan for dao definitions and init with associated cassandra context
        builder.append(scanForDAODefinitions(keyspace, module, cassandraContext, daoInstances));
        //logger.info(" .............................    scanForDAODefinitions {}", (System.currentTimeMillis() - start));
        loadedNodeModules.add(nodeModule);

        if (cassandraContext.isSantiyChecking()) {
            logger.info(builder.toString());
        }
        logger.debug(builder.toString());
    }

    private StringBuilder scanForSchemasDefinitions(String keyspace, String module, List<Schema> schemaInstances) {
        // List<Schema> schemaInstances = LmServiceLocator.getInstancesWithSubTypeOf(Schema.class);

        StringBuilder builder = new StringBuilder();
        if (module == null) {
            builder.append("\n").append("Found the following schemas: ").append("\n");
        } else {
            builder.append("\n").append("Found the following schemas for module: ").append(module).append("\n");
        }

        safeStream(schemaInstances).filter(instance -> {
                    @SuppressWarnings("unchecked")
                    GUSLSchema schema = instance.getClass().getAnnotation(GUSLSchema.class);
                    return schema != null && schema.schema().toLowerCase().equals(keyspace.toLowerCase()) && ((module == null && schema.module().isEmpty()) || (module != null && schema.module().equals(module)));
                })
                .forEach(instance -> {
                    theSchemaMap.put(keyspace, instance);
                    builder.append("\t").append("Schema Class: ").append(instance.getClass().getCanonicalName()).append("\n");
                });

        return builder;

    }

    public StringBuilder scanForDAODefinitions(String keyspace, String module, CassandraContext cassandraContext, List<CassandraDAO> daoInstances) {
        // List<CassandraDAO> daoInstances = LmServiceLocator.getInstancesWithSubTypeOf(CassandraDAO.class);

        //List<? extends Annotation> daoInstances = LmServiceLocator.getInstancesWithAnnotatedClasses(LmDAO.class);
        StringBuilder builder = new StringBuilder(400);

        builder.append("\t\t").append("Found the following DAOs: ").append("\n");

        safeStream(daoInstances).filter(instance -> {
                    @SuppressWarnings("unchecked")
                    GUSLDAO dao = instance.getClass().getAnnotation(GUSLDAO.class);
                    return dao != null && dao.schema().toLowerCase().equals(keyspace.toLowerCase()) && ((module == null && dao.module().isEmpty()) || (module != null && dao.module().equals(module)));
                })
                .forEach(instance -> {
                    CassandraDAO cassandraDAO = instance;
                    builder.append(addDAO(cassandraContext, cassandraDAO));
                });

        return builder;
    }

    private StringBuilder addDAO(CassandraContext cassandraContext, CassandraDAO cassandraDAO) {
        StringBuilder builder = new StringBuilder(300);

        try {
            cassandraContext.addDAO(cassandraDAO);
            builder.append("\t\t\t Loaded DAO Class: ").append(cassandraDAO.getClass().getCanonicalName()).append("\n");

        } catch (Throwable ex) {
            logger.error("Failed to bootstrap schema", ex);
            builder.append("\t\t\t Failed DAO Class: ").append(cassandraDAO.getClass().getCanonicalName()).append("Failed to load - error: ").append(ex.getMessage()).append("\n");
        }
        return builder;
    }

    private StringBuilder scanForTables(String keyspace, String module, CassandraContext cassandraContext, List<Table> tableInstances) {

        StringBuilder builder = new StringBuilder(300);
        builder.append("\t\t").append("Found the following tables: ").append("\n");

//        List<Table> tableInstances = LmServiceLocator.getInstancesWithSubTypeOf(Table.class
//        );

        safeStream(tableInstances).filter(instance -> {
                    @SuppressWarnings("unchecked")
                    GUSLTable table = instance.getClass().getAnnotation(GUSLTable.class);
                    return table != null && table.schema().toLowerCase().equals(keyspace.toLowerCase()) && ((module == null && table.module().isEmpty()) || (module != null && table.module().equals(module)));
                })
                .forEach(instance -> {
                    Table table = instance;
                    tables.putIfAbsent(table.getKeyspace() + table.getTableName(), table);
                    builder.append(addTablesInstance(cassandraContext, table));
                });

        return builder;

    }

    private StringBuilder addTablesInstance(CassandraContext cassandraContext, Table table) {
        StringBuilder builder = new StringBuilder();

        try {
            table.performValidation();
            table.registerCodecs(theCodecRegistra);

            //TODO - cassandraContext get table meta data from cassandra driver and validate against table definition
            builder.append("\t\t\t Table Class: ").append(table.getClass().getCanonicalName()).append("\n");
        } catch (GUSLException ex) {
            logger.error("Failed to validate table", ex);
            builder.append("\t\t]t Table Class: ").append(table.getClass().getCanonicalName()).append("Validation failed - error: ").append(ex.getMessage()).append("\n");
        }

        return builder;
    }

    @Override
    public CassandraContext getCassandraContext(String keyspace) {
        return theCassandraContextMap.get(keyspace.toLowerCase());
    }

    private class NodeModule {

        private final String keyspace;
        private final String module;

        public NodeModule(String keyspace, String module) {
            this.keyspace = keyspace;
            this.module = module;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + Objects.hashCode(this.keyspace);
            hash = 53 * hash + Objects.hashCode(this.module);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final NodeModule other = (NodeModule) obj;
            if (!this.keyspace.equals(other.keyspace)) {
                return false;
            }
            return Objects.equals(this.module, other.module);
        }

    }

}
