package gusl.cassandra.schema;

import gusl.cassandra.builder.codecs.CodecRegistra;

/**
 *
 * @author grant
 */
public interface Schema {

    public void registerCodecs(CodecRegistra codecRegistra);
}
