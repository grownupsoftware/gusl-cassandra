/* Copyright lottomart */
package gusl.cassandra.schema;

import gusl.cassandra.builder.CassandraContext;
import gusl.cassandra.builder.CassandraDAO;
import gusl.cassandra.builder.Table;
import gusl.cassandra.config.CassandraConnectionConfig;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 * @author grant
 */
@Contract
public interface SchemaController {

    void configure(List<CassandraConnectionConfig> cassandraConnectionConfigs, String... keyspaces);

    void loadSchemas(List<Schema> schemaInstances, List<CassandraDAO> daoInstances, List<Table> tableInstances);

    CassandraContext getCassandraContext(String keyspace);

}
