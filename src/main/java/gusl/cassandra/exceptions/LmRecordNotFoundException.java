package gusl.cassandra.exceptions;

import gusl.core.exceptions.GUSLException;

/**
 *
 * @author grant
 */
public class LmRecordNotFoundException extends GUSLException {

    public LmRecordNotFoundException(String message) {
        super(message);
    }

    public LmRecordNotFoundException(String message, Throwable ex) {
        super(message, ex);
    }

}
