package gusl.cassandra.exceptions;

import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorType;
/**
 *
 * @author grant
 */
public class NoFieldsForUpdateException extends RuntimeException {

    private final ErrorType errorType;
    private final ErrorDO errorDO;

    public NoFieldsForUpdateException(ErrorType errorType, ErrorDO errorDO) {
        this.errorType = errorType;
        this.errorDO = errorDO;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public ErrorDO getErrorDO() {
        return errorDO;
    }

}
