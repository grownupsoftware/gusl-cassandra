package gusl.cassandra.exceptions;

import gusl.core.errors.ErrorDO;
import gusl.core.errors.ErrorType;
import gusl.core.exceptions.GUSLErrorException;

/**
 *
 * @author grant
 */
public class LmValidationException extends GUSLErrorException {

    private static final long serialVersionUID = 1L;

    protected ErrorType errorType;

//    public LmValidationException() {
//    }

    public LmValidationException(ErrorType errorType, ErrorDO errorDO) {
        super(errorDO);
        this.errorType = errorType;
        // this.addError(errorDO);
    }

    public ErrorType getErrorType() {
        return errorType;
    }

}
