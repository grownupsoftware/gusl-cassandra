package gusl.cassandra.builder;

import gusl.core.exceptions.GUSLException;
import gusl.core.exceptions.GUSLErrorException;

/**
 *
 * @author grant
 */
public interface CassandraDAO {

    public void prepare() throws Exception;

    public void setCassandraContext(CassandraContext cassandraContext);

    public void initialise() throws GUSLException;
}
