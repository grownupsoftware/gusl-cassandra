package gusl.cassandra.builder;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PageResponse;
import gusl.core.exceptions.GUSLException;
import gusl.core.metrics.Latency;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

/**
 * @param <T> the DO class
 * @author grant
 */
public class LmSelectExecutor<T> extends AbstractBaseExecutor<T> {

    private final Condition theWhereCondition;
    private final Condition[] theAndColumns;
    private final ConsistencyLevel theConsistencyLevel;

    public LmSelectExecutor(CassandraContext context, String statementString, Table table, List<Column> columns,
                            Class<T> dataClass, Condition whereCondition, Condition[] andColumns, String metricName,
                            boolean requiresApollo, boolean ignoreApollo, ConsistencyLevel consistencyLevel) throws GUSLException {
        super(context, statementString, table, columns, dataClass, metricName, true, requiresApollo, (whereCondition != null ? 1 : 0) + (andColumns != null ? andColumns.length : 0), ignoreApollo);
        theWhereCondition = whereCondition;
        theAndColumns = andColumns;
        theConsistencyLevel = consistencyLevel;
        loadSetterMethods(columns, dataClass);
    }

    public Selecter select() throws GUSLException {
        if (!isInit()) {
            throw new GUSLException("DAO has not been initialised");
        }
        return new Selecter(getCassandraContext().getSession(), theWhereCondition, theAndColumns, getTable().getStaticPrimaryKey());
    }

    public class Selecter {

        private final Session session;
        private final Object[] values;
        private final int numberAndColumns;

        private int andIndex = 0;
        private int andCounter = 0;
        private boolean doneWhere = false;
        private boolean doneAnd = false;
        private boolean doneStatic = false;
        private boolean hasWhere = false;
        private final Column theStaticPrimaryKey;
        private final Condition theWhereCondition;

        public Selecter(Session session, Condition whereCondition, Condition[] andColumns, Column staticPrimaryKey) {
            this.session = session;
            this.numberAndColumns = andColumns.length;
            this.theStaticPrimaryKey = staticPrimaryKey;
            this.theWhereCondition = whereCondition;

            hasWhere = whereCondition != null;
            this.values = new Object[(hasWhere ? 1 : 0) + andColumns.length];
            andIndex = hasWhere ? 1 : 0;
        }

        public Selecter where(Object whereValue) throws GUSLException {
            doneWhere = true;
            doneStatic = true;
            if (theStaticPrimaryKey != null) {
                values[0] = theStaticPrimaryKey.getStaticPartitionKeyValue();
                values[1] = whereValue;
                andIndex++;
                if (andIndex == values.length) {
                    doneAnd = true;
                }
            } else {
                if (theWhereCondition.getColumn().isEnum()) {
                    values[0] = whereValue.toString();
                } else if (theWhereCondition.getColumn().getDataType() == Column.DataType.DATE && !theWhereCondition.isIn()) {
                    values[0] = convertToNoTime((Date) whereValue);
                } else {
                    values[0] = whereValue;
                }
            }
            return this;
        }

        public Selecter using(T entity) throws GUSLException {

            Table table = getTable();

            try {
                int index = 0;
                for (Column column : table.getPrimaryKeys()) {
                    if (column.isStaticPartitionKey()) {
                        values[index] = theStaticPrimaryKey.getStaticPartitionKeyValue();
                    } else {
                        values[index] = column.getGetterMethod().invoke(entity);
                    }
                    if (index > 0) {
                        andIndex++;
                    }
                    index++;
                }

                doneWhere = true;
                doneStatic = true;
                doneAnd = true;

                return this;
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                logger.error("Error invoking getter", ex);
                throw new GUSLException("Error invoking getter", ex);
            }

        }

        public Selecter and(Object value) throws GUSLException {
            if (!doneWhere) {
                throw new GUSLException("Cannot specify 'and' until 'where' has been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (andIndex > (numberAndColumns + (theStaticPrimaryKey == null ? 0 : 1))) {
                throw new GUSLException("Trying to have more 'and' columns thans was specified in 'prepare'Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (theAndColumns[andCounter].getColumn().getDataType() == Column.DataType.DATE) {
                values[andIndex++] = convertToNoTime((Date) value);
            } else {
                values[andIndex++] = value;
            }
            andCounter++;
            doneAnd = true;
            return this;
        }

        protected void validate() throws GUSLException {
            try {
                if (!doneStatic && theStaticPrimaryKey != null) {
                    // no where - so need to add
                    values[0] = theStaticPrimaryKey.getStaticPartitionKeyValue();
                    if (andIndex < values.length) {
                        andIndex++;
                    }
                    doneWhere = true;
                }

                if (hasWhere && !doneWhere) {
                    logger.error("Error: Cannot 'execute' until 'where' have been added. Statement: {}", theStatementString);
                    throw new GUSLException("Cannot 'execute' until 'where' have been addedTable["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
                }
                if (!doneAnd && numberAndColumns > 0) {
                    logger.error("Error: Cannot 'execute' until all 'and' have been added. Statement: {} {}", theStatementString, values);
                    throw new GUSLException("Cannot 'execute' until all 'and' have been addedTable["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
                }
                if (andIndex != values.length) {
                    logger.error("Error: Cannot 'execute' until all 'and' have been added. [{} = {}] Statement: {}", andIndex, values.length, theStatementString);
                    throw new GUSLException("Cannot 'execute' until all 'and' have been added[ " + andIndex + "] [" + values.length + "]Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
                }
            } catch (Throwable t) {
                logger.error("Statement: {} Values: {} Error: {}", theStatementString, values, t.getMessage());
                throw new GUSLException("Error validating statementTable["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]", t);
            }
        }

        public SingletonSelector asSingleton() throws GUSLException {
            validate();
            if (requiresApollo) {
                return new SingletonSelector(session, theStatementString, values);
            }

            logger.debug("theStatementString: {} values: {}", theStatementString, values);

            BoundStatement boundStatement = createBoundStatement(theWhereCondition, theAndColumns, Optional.empty(), values);
            if (theConsistencyLevel != null) {
                if (theConsistencyLevel == ConsistencyLevel.LOCAL_SERIAL || theConsistencyLevel == ConsistencyLevel.SERIAL) {
                    boundStatement.setSerialConsistencyLevel(theConsistencyLevel);
                } else {
                    boundStatement.setConsistencyLevel(theConsistencyLevel);
                }
            }
            return new SingletonSelector(session, boundStatement);
        }

        public ListSelector asList() throws GUSLException {
            validate();
            logger.debug("---> {} {} {}", theStatementString, values, requiresApollo);
            if (requiresApollo) {
                return new ListSelector(session, values);
            }
            BoundStatement boundStatement = createBoundStatement(theWhereCondition, theAndColumns, Optional.empty(), values);
            if (theConsistencyLevel != null) {
                if (theConsistencyLevel == ConsistencyLevel.LOCAL_SERIAL || theConsistencyLevel == ConsistencyLevel.SERIAL) {
                    boundStatement.setSerialConsistencyLevel(theConsistencyLevel);
                } else {
                    boundStatement.setConsistencyLevel(theConsistencyLevel);
                }
            }
            return new ListSelector(session, boundStatement);
        }

    }

    public class SingletonSelector {

        private final Session session;
        private final BoundStatement boundStatement;
        private final Object[] values;
        private final boolean requiresApollo;

        public SingletonSelector(Session session, BoundStatement boundStatement) {
            this.session = session;
            this.boundStatement = boundStatement;
            this.requiresApollo = false;
            this.values = null;
        }

        public SingletonSelector(Session session, String sqlStatement, Object[] values) {
            this.session = session;
            this.boundStatement = null;
            this.requiresApollo = true;
            this.values = values;
        }

        public Optional<T> execute() throws GUSLException {
            try (Latency.Context ctx = getLatencyMetric().time()) {
                if (requiresApollo) {
                    return executeForObjectUsingApollo(session, values);
                }
                return executeForObject(session, boundStatement);
            }
        }

        public CompletableFuture<Optional<T>> executeAsAsync() throws GUSLException {
            CompletableFuture<Optional<T>> future = new CompletableFuture<>();
            if (requiresApollo) {
                executeForObjectAsAsyncUsingApollo(session, values, future, getLatencyMetric().time());
            } else {
                executeForObjectAsAsync(session, boundStatement, future, getLatencyMetric().time());
            }
            return future;
        }
    }

    public class ListSelector {

        private final Session session;
        private final BoundStatement boundStatement;
        private final Object[] values;
        private final boolean requiresApollo;
        private PageRequest pageRequest;
        boolean usePagination;

        public ListSelector(Session session, BoundStatement boundStatement) {
            this.session = session;
            this.boundStatement = boundStatement;
            this.requiresApollo = false;
            this.values = null;
        }

        public ListSelector(Session session, Object[] values) {
            this.session = session;
            this.boundStatement = null;
            this.requiresApollo = true;
            this.values = values;
        }

        public Stream<T> stream() throws GUSLException {
            if (requiresApollo) {
                return streamUsingApollo(session, values);
            }

            return executeForStream(session, boundStatement);
        }

        public ListSelector pageState(PageRequest pageRequest) {
            this.pageRequest = pageRequest;
            this.usePagination = true;
            return this;
        }

        public List<T> execute() throws GUSLException {
            try (Latency.Context ctx = getLatencyMetric().time()) {
                logger.debug("requires apollo: {}", requiresApollo);
                if (requiresApollo) {
                    return executeForListUsingApollo(session, values);
                }
                return executeForList(session, boundStatement);
            }
        }

        public PageResponse<T> executePaged(PageRequest pageRequest) throws GUSLException {
            try (Latency.Context ctx = getLatencyMetric().time()) {
                return executeForList(session, boundStatement, pageRequest);
            }
        }

        public CompletableFuture<List<T>> executeAsAsync() throws GUSLException {
            CompletableFuture<List<T>> future = new CompletableFuture<>();
            logger.debug("--->{} {} {} {}", theStatementString, values, boundStatement, requiresApollo);
            if (requiresApollo) {
                executeForListAsAsyncUsingApollo(session, values, future, getLatencyMetric().time());
            } else {
                executeForListAsAsync(session, boundStatement, future, getLatencyMetric().time());
            }
            return future;
        }
    }
}
