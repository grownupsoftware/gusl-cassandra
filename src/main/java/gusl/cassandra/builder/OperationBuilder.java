package gusl.cassandra.builder;

import gusl.core.exceptions.GUSLException;

/**
 *
 * @author grant
 */
public interface OperationBuilder<T> {

    public T where(Condition condition) throws GUSLException;

    public T and(Condition condition) throws GUSLException;
}
