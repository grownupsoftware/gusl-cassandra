package gusl.cassandra.builder;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.Row;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.CaseFormat;
import gusl.cassandra.utils.RowMapper;
import gusl.core.exceptions.GUSLException;
import gusl.core.json.ObjectMapperFactory;
import gusl.core.security.InvalidOperationException;
import gusl.core.security.PasswordStorage;
import gusl.core.utils.DateFormats;
import gusl.core.utils.Utils;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.*;

@CustomLog
public class CassandraBinder implements RowMapper {

    protected ObjectMapper theObjectMapper;

    private static final TypeReference<HashMap<String, Object>> TYPE_REF = new TypeReference<HashMap<String, Object>>() {
    };

    public CassandraBinder() {
        theObjectMapper = ObjectMapperFactory.getDefaultObjectMapper();
    }

    public void bindValue(BoundStatement boundStatement, int index, Column column, Object value, boolean incrLock) throws GUSLException {
        if (null == column.getDataType()) {
            if (value == null && column.hasNullValue()) {
                // translate value
                setValue(boundStatement, column, column.getNullValue(), index);
            } else if (value == null) {
                boundStatement.setToNull(index);
            } else {
                setValue(boundStatement, column, value, index);
            }
        } else {
            switch (column.getDataType()) {
                case LIST:
                    if (value != null) {
                        boundStatement.setList(index, (List) value, column.getClasses()[0]);
                    } else {
                        boundStatement.setList(index, null);
                    }
                    break;
                case SET:
                    if (value != null) {
                        boundStatement.setSet(index, (Set) value, column.getClasses()[0]);
                    } else {
                        boundStatement.setSet(index, null);
                    }
                    break;
                case PASSWORD:
                    if (value != null) {
                        try {
                            if (PasswordStorage.isHashed(value.toString())) {
                                boundStatement.setString(index, value.toString());
                            } else {
                                boundStatement.setString(index, PasswordStorage.createHash(value.toString()));
                            }
                        } catch (InvalidOperationException ex) {
                            logger.warn("Unable to sha password", ex.getMessage());
                            boundStatement.setString(index, null);
                        }
                    } else {
                        boundStatement.setString(index, null);
                    }
                    break;
                case ENUM:
                    if (value != null) {
                        boundStatement.setString(index, value.toString());
                    } else {
                        boundStatement.setString(index, null);
                    }
                    break;
                case JSON:
                    if (value != null) {
                        boundStatement.set(index, value, (Class<Object>) column.getClasses()[0]);
                    } else {
                        boundStatement.setString(index, null);
                    }
                    break;
                case LOCAL_DATE:
                    if (value != null) {
                        boundStatement.set(index, (LocalDate) value, LocalDate.class);
                    } else {
                        boundStatement.setDate(index, null);
                    }
                    break;

                case LOCAL_TIME:
                    if (value != null) {
                        boundStatement.set(index, (LocalTime) value, LocalTime.class);
                    } else {
                        boundStatement.setDate(index, null);
                    }
                    break;
                case TIMESTAMP_INSTANT:
                    if (value != null) {
                        boundStatement.set(index, (Instant) value, Instant.class);
                    } else {
                        boundStatement.setDate(index, null);
                    }
                    break;
                case ZONED_DATE_TIME:
                    if (value != null) {
                        boundStatement.set(index, (ZonedDateTime) value, ZonedDateTime.class);
                    } else {
                        boundStatement.set(index, null, ZonedDateTime.class);
                    }
                    break;
                case DATE:
                    if (value != null) {
                        if (value instanceof List) {
                            boundStatement.bind(value);
                        } else {
                            boundStatement.setTimestamp(index, convertToNoTime((Date) value));
                        }
                    } else {
                        boundStatement.setTimestamp(index, null);
                    }
                    break;
                case LOCK:
                    if (value == null) {
                        boundStatement.setInt(index, 1);
                    } else {
                        if (incrLock) {
                            int lockVal = (Integer) value;
                            lockVal++;
                            boundStatement.setInt(index, lockVal);
                        } else {
                            boundStatement.setInt(index, (Integer) value);
                        }
                    }
                    break;
                default:
                    if (value == null && column.hasNullValue()) {
                        // translate value
                        setValue(boundStatement, column, column.getNullValue(), index);
                    } else if (value == null) {
                        boundStatement.setToNull(index);
                    } else {
                        setValue(boundStatement, column, value, index);
                    }
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    void setValue(BoundStatement boundStatement, Column column, Object value, int index) throws GUSLException {
        logger.debug("value: {} with data type: {} for column: {}", value, column.getDataType(), column.getName());
        try {
            switch (column.getDataType()) {
                case PLONG:
                case LONG:
                    // don't like this
                    if (value instanceof Set) {
                        boundStatement.setSet(index, (Set<Long>) value);
                    } else {
                        boundStatement.setLong(index, (Long) value);
                    }
                    break;
                case PASSWORD:
                case STRING:
                    boundStatement.setString(index, (String) value);
                    break;
                case BOOLEAN:
                    boundStatement.setBool(index, (Boolean) value);
                    break;
                case PINT:
                case INTEGER:
                    boundStatement.setInt(index, (Integer) value);
                    break;
                case BIGDECIMAL:
                    boundStatement.setDecimal(index, (BigDecimal) value);
                    break;
//                case DATE:
//                    boundStatement.setDate(index, (LocalDate) value);
//                    break;
                case LOCAL_TIME:
                    boundStatement.setTime(index, ((Date) value).getTime());
                    break;
                case TIMESTAMP:
                    boundStatement.setTimestamp(index, (Date) value);
                    break;
                case TIMESTAMP_INSTANT:
                    boundStatement.setTimestamp(index, (Date) value);
                    break;
                case PDOUBLE:
                case DOUBLE:
                    boundStatement.setDouble(index, (Double) value);
                    break;
                case FLOAT:
                    boundStatement.setFloat(index, (Float) value);
                    break;
                case ENUM:
                    boundStatement.setString(index, ((Enum) value).name());
                    break;
                case LIST:
                    boundStatement.setList(index, (List<?>) value);
                    break;
                case SET:
                    boundStatement.setSet(index, (Set<?>) value);
                    break;
                case MAP:
                    boundStatement.setMap(index, (Map<?, ?>) value);
                    break;
                case OBJECT_MAP:
                    try {
                        if (value != null) {
                            boundStatement.setString(index, theObjectMapper.writeValueAsString(value));
                        } else {
                            boundStatement.setString(index, null);
                        }
                    } catch (JsonProcessingException ex) {
                        logger.error("Failed to cast value: {} with data type: {} for column {}", value, column.getDataType(), column.getName(), ex);
                        boundStatement.setString(index, null);
                    }
                    break;

                default:
                    // do nothing
            }
        } catch (ClassCastException ex) {
            logger.error("Failed to cast value: {} with data type: {} for column: {}", value, column.getDataType(), column.getName(), ex);
            throw new GUSLException("Failed to cast value: " + value + " of type: " + column.getDataType() + " for column: " + column.getName());
        }
    }

    public static Date convertToNoTime(Date inputDate) throws GUSLException {
        String dateStr = DateFormats.formatUTCDate(Utils.DEFAULT_DATE_FORMAT, inputDate);

        try {
            Date newDate = DateFormats.getUTCFormatFor(Utils.DEFAULT_DATE_FORMAT).parse(dateStr);
            //logger.info("Converted date from: {} to: {}", inputDate, newDate);
            return newDate;
        } catch (ParseException ex) {
            throw new GUSLException("Failed to parse date: " + dateStr);
        }
    }

    @Override
    public <T> T mapRow(Row row, Class<T> theDataClass, List<Column> columns) {
        return mapRow(row, theDataClass, columns, false);
    }

    public <T> T mapRow(Row row, Class<T> theDataClass, List<Column> columns, boolean dynamicSetter) {
        T destInstance = getInstance(theDataClass);

        ColumnDefinitions columnDefinitions = row.getColumnDefinitions();

        for (Column column : columns) {
            if (!column.isStaticPartitionKey()) {
                Object value;

                if (columnDefinitions.contains(column.getName())) {
                    try {
                        value = getValue(row, column.getName(), column.getDataType(), column.getClasses());
                    } catch (GUSLException ex) {
                        logger.error("Failed to get value for column: {} of dataType: {} in class: {}", column.getName(), column.getDataType(), column.getClasses(), ex);
                        throw new RuntimeException("Failed to get value column [" + column.getName() + "] with dataType [" + column.getDataType() + "]", ex);
                    }
                    try {
                        // matches null value translater so don't set setter
                        if (!column.hasNullValue() || !column.getNullValue().equals(value)) {
                            Method setter = dynamicSetter ? findSetterMethod(column, theDataClass) : column.getSetterMethod();
                            setter.invoke(destInstance, value);
                        }

                    } catch (Throwable ex) {
                        logger.error("Failed to set  column: {} [{}] with value {} [{}]", column.getName(), column.getSetterMethod(), value, value.getClass(), ex);
                        throw new RuntimeException("Failed to set column [" + column.getName() + "] with value [" + value + "] ", ex);
                    }
                }
            }

        }

        return destInstance;
    }

    private <T> T getInstance(Class<T> destClass) {
        T destInstance;
        try {
            destInstance = destClass.getConstructor().newInstance();

            return destInstance;

        } catch (InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException ex) {
            logger.error(" 1... Failed to create an instance of class {}. Does it have a default constructor? Error: {}",
                    destClass.getCanonicalName(), ex.getMessage());
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private Object getValue(Row row, String name, Column.DataType dataType, Class<?>[] classes) throws GUSLException {
        try {
            switch (dataType) {
                case PLONG:
                case LONG:
                    return row.getLong(name);
                case PASSWORD:
                case STRING:
                    return row.getString(name);
                case BOOLEAN:
                    return row.getBool(name);
                case PINT:
                case INTEGER:
                    return row.getInt(name);
                case BIGDECIMAL:
                    return row.getDecimal(name);
                case TIMESTAMP:
                case DATE:
                    return row.getTimestamp(name);
                case TIMESTAMP_INSTANT:
                    return row.get(name, Instant.class);
                case LOCAL_TIME:
                    return row.get(name, LocalTime.class);
                case LOCAL_DATE:
                    return row.get(name, LocalDate.class);
                case ZONED_DATE_TIME:
                    return row.get(name, ZonedDateTime.class);
                case PDOUBLE:
                case DOUBLE:
                    return row.getDouble(name);
                case FLOAT:
                    return row.getFloat(name);
                case ENUM:
                    String val = row.getString(name);
                    return val == null ? null : Enum.valueOf((Class<? extends Enum>) classes[0], val);
                case SET:
                    return row.getSet(name, classes[0]);
                case LIST:
                    return row.getList(name, classes[0]);
                case MAP:
                    return row.getMap(name, classes[0], classes[1]);
                case JSON:
                    return row.get(name, classes[0]);
                case LOCK:
                    return row.getInt(name);
                case OBJECT_MAP:
                    String value = row.getString(name);
                    if (value == null) {
                        return null;
                    } else {
                        return theObjectMapper.readValue(value, TYPE_REF);
                    }
                default:
                    throw new GUSLException("Do not know how to extract data for type: " + dataType);
            }
        } catch (GUSLException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("Failed to read value for field: {}", name, ex);
            throw new GUSLException("Failed to read value for field: " + name + ": " + dataType, ex);
        }
    }

    protected <T> void loadSetterMethods(List<Column> theColumns, Class<T> dataClass) throws GUSLException {
        for (Column column : theColumns) {
            if (!column.isStaticPartitionKey()) {

                Method setterMethod = findSetterMethod(column, dataClass);

                if (setterMethod == null) {
                    Class<?> paramClazz;
                    if (column.getDataType() == Column.DataType.ENUM) {
                        paramClazz = column.getEnumClass();
                    } else {
                        paramClazz = column.getJavaClass();
                    }

                    throw new GUSLException("No setter method found for column: " + column.getName() + " of col type: " + column.getDataType() + " - looking for " + getMethodName("set", normaliseName(column.getName()) + " with signature: " + paramClazz + " in class: " + dataClass.getCanonicalName()));
                }
                column.setSetterMethod(setterMethod);
            }
        }
    }

    protected <T> Method findSetterMethod(Column column, Class<T> theDataClass) throws GUSLException {
        String name = normaliseName(column.getName());

        Class<?> clazz;
        if (null == column.getDataType()) {
            clazz = column.getJavaClass();
        } else {
            switch (column.getDataType()) {
                case ENUM:
                    clazz = column.getEnumClass();
                    break;
                case MAP:
                    clazz = column.getClasses()[0];
                    break;
                default:
                    clazz = column.getJavaClass();
                    break;
            }
        }

        try {
            Method setterMethod;
            try {
                setterMethod = theDataClass.getMethod(getMethodName("set", name), clazz);

            } catch (NoSuchMethodException ex) {
                // try auto boxing
                if ("boolean".equals(clazz.getCanonicalName())) {
                    try {
                        setterMethod = theDataClass.getMethod(getMethodName("set", name), Boolean.class);
                    } catch (NoSuchMethodException e) {
                        return null;
                    }
                } else {
                    return null;
                }

            }

            if (setterMethod != null) {
                return setterMethod;
            }
            logger.warn("Failed to find setter " + getMethodName("set", name) + " in Class: " + theDataClass.getCanonicalName()
                    + " ignoring and carrying on");
        } catch (NullPointerException ex) {
            logger.warn("What the fuck ...   {} : {} : ", column, theDataClass);
        }
        return null;
    }

    protected static String normaliseName(String name) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
    }

    protected static String getMethodName(String prefix, String name) {
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

}
