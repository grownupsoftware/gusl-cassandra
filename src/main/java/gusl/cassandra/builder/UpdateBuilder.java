package gusl.cassandra.builder;

import com.datastax.driver.core.ConsistencyLevel;
import java.util.ArrayList;
import java.util.List;
import gusl.core.exceptions.GUSLException;

/**
 *
 * @author grant
 * @param <T> the DO class
 */
public class UpdateBuilder<T> implements OperationBuilder<UpdateBuilder<T>> {

    private static final String SPACE = " ";
    private static final String COMMA = ",";
    private static final String VALUE_PARAM = "=?";
    private static final String SEMI_COLON = ";";

    private final CassandraContext theContext;
    private final Table theTable;
    private final List<Column> theSetColumns = new ArrayList<>();
    private Condition theWhereCondition = null;
    private final List<Condition> theAndConditions = new ArrayList<>();
    private Class<T> theDataClass;
    private String theMetricName;
    private ConsistencyLevel theConsistencyLevel;

    public UpdateBuilder(CassandraContext context, Table table) {
        this.theContext = context;
        this.theTable = table;
    }

    public UpdateBuilder<T> set(Column column) {
        theSetColumns.add(column);
        return this;
    }

    public UpdateBuilder<T> updateRecord(Class<T> dataClass) {
        this.theDataClass = dataClass;
        return this;
    }

    @Override
    public UpdateBuilder<T> where(Condition condition) {
        theWhereCondition = condition;
        return this;
    }

    /**
     *
     * @param condition
     * @return
     */
    @Override
    public UpdateBuilder<T> and(Condition condition) throws GUSLException {
        theAndConditions.add(condition);
        return this;
    }

    public UpdateBuilder<T> withPrimaryKeyClauses() throws GUSLException {
        if (theTable.getPrimaryKeys().length > 0) {
            where(theTable.getPrimaryKeys()[0].equal());
        }
        for (int x = 1; x < theTable.getPrimaryKeys().length; x++) {
            and(theTable.getPrimaryKeys()[x].equal());
        }
        return this;
    }

    public UpdateBuilder<T> withMetric(String metricName) {
        this.theMetricName = metricName;
        return this;
    }

    public LmUpdateExecutor<T> prepare() throws GUSLException {
        validate();
        String statement = buildStatementString();
        return new LmUpdateExecutor<>(theContext, statement, theTable, theSetColumns.toArray(new Column[theSetColumns.size()]), theWhereCondition, theAndConditions.toArray(new Condition[theAndConditions.size()]), theDataClass, theMetricName, theConsistencyLevel);
    }

    private void validate() throws GUSLException {
        if (theTable == null) {
            throw new GUSLException("No table specified");
        }
        if (theAndConditions.size() > 0 && theWhereCondition == null) {
            throw new GUSLException("Cannot have 'and' conditions without a where - change one of the 'and' to a where");
        }
        if (theWhereCondition == null) {
            throw new GUSLException("No 'where' clause specified");
        }
        if (theDataClass == null && theSetColumns.isEmpty()) {
            throw new GUSLException("No columns specified to update - must use 'set' or 'updateRecord'");

        }
    }

    public String buildStatementString() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("UPDATE ").append(theTable.getKeyspace()).append(".").append(theTable.getTableName()).append(" SET ");

        if (!theSetColumns.isEmpty()) {
            addColumns(builder, theSetColumns.toArray(new Column[theSetColumns.size()]));
        } else {
            addColumns(builder, theTable.getColumns());
        }

        if (theWhereCondition != null) {
            builder.append(" WHERE ").append(theWhereCondition.queryParam());
        }

        theAndConditions.stream().forEach(condition -> {
            builder.append(" AND ").append(condition.queryParam());
        });

        for (Column column : theTable.getColumns()) {
            if (column.getDataType() == Column.DataType.LOCK) {
                builder.append(" IF ").append(column.getName()).append(" = ?");
                theConsistencyLevel = ConsistencyLevel.LOCAL_SERIAL;
                break;
            }
        }

        builder.append(SEMI_COLON);
        return builder.toString();
    }

    private void addColumns(StringBuilder builder, Column[] columns) {
        boolean firstField = true;
        for (Column column : columns) {
            // cannot set primary keys
            if (matchesPrimaryKey(theTable.getPrimaryKeys(), column)) {
                continue;
            }
            if (!firstField) {
                builder.append(COMMA);
            }
            builder.append(SPACE).append(column.getName()).append(VALUE_PARAM);
            firstField = false;
        }
    }

    private boolean matchesPrimaryKey(Column[] primaryKeys, Column column) {
        for (Column col : primaryKeys) {
            if (col.getName().equals(column.getName())) {
                return true;
            }
        }
        return false;
    }

}
