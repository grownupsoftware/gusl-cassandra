package gusl.cassandra.builder;

import lombok.CustomLog;

/**
 * @author grant
 */
@CustomLog
public class Condition {

    public static enum Operand {
        EQUAL,
        NOT_EQUAL,
        GREATER_THAN,
        GREATER_THAN_EQUALS,
        LESS_THAN,
        LESS_THAN_EQUALS,
        LIKE,
        OR,
        IN

    }

    private Column column;

    private final Operand operand;
    private final String operandValue;
    private Object value;
    private boolean useApollo;
    private final boolean usesContains;

    public Condition(Column column, Operand operand) {
        this.column = column;
        useApollo = checkIfNeedApollo(operand);
        usesContains = checkIfNeedsContains(column);
        this.operandValue = get(operand);
        this.operand = operand;
        //logger.info("------------> column: {} Type: {} UsesContains: {}", column.getName(), column.getDataType(), usesContains);
    }

    public Condition(String operand, Object value) {
        this.operandValue = operand;
        this.value = value;
        useApollo = false;
        usesContains = false;
        this.operand = null;
    }

    private boolean checkIfNeedApollo(Operand operand) {
        switch (operand) {
            case NOT_EQUAL:
            case LIKE:
            case OR:
                return true;
            default:
                return false;
        }
    }

    private boolean checkIfNeedsContains(Column column) {
        switch (column.getDataType()) {
            case SET:
            case LIST:
            case MAP:
                // cannot use apollo for sets, lists etc
                useApollo = false;
                return true;
            default:
                return false;
        }
    }

    public boolean requiresApollo() {
        return useApollo;
    }

    public Column getColumn() {
        return column;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String queryParam() {
        return " " + column.getName() + operandValue;
    }

    private String get(Operand operand) {
        switch (operand) {
            case EQUAL:
                //logger.info("----GET--------> column: {} Type: {} UsesContains: {}", column.getName(), column.getDataType(), usesContains);
                return usesContains ? " contains ? " : " = ? ";
            case NOT_EQUAL:
                return " != ? ";
            case GREATER_THAN:
                return " > ? ";
            case GREATER_THAN_EQUALS:
                return " >= ? ";
            case LESS_THAN:
                return " < ? ";
            case LESS_THAN_EQUALS:
                return " <= ? ";
            case LIKE:
                return " LIKE ? ";
            case IN:
                return " IN ? ";
            default:
                throw new AssertionError(operand.name());
        }
    }

    public boolean isIn() {
        if (this.operand != null && this.operand == Operand.IN) {
            return true;
        }
        return false;
    }
}
