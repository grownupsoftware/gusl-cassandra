package gusl.cassandra.builder.codecs;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.exceptions.InvalidTypeException;

import java.nio.ByteBuffer;

import static com.datastax.driver.core.ParseUtils.*;
import static java.lang.Long.parseLong;

/**
 *
 * @author grant
 */
public class InstantCodec extends TypeCodec<java.time.Instant> {

    public static final InstantCodec instance = new InstantCodec();

    /**
     * A {@link java.time.format.DateTimeFormatter} that parses (most) of the
     * ISO formats accepted in CQL.
     */
    private static final java.time.format.DateTimeFormatter PARSER = new java.time.format.DateTimeFormatterBuilder()
            .parseCaseSensitive()
            .parseStrict()
            .append(java.time.format.DateTimeFormatter.ISO_LOCAL_DATE)
            .optionalStart()
            .appendLiteral('T')
            .appendValue(java.time.temporal.ChronoField.HOUR_OF_DAY, 2)
            .appendLiteral(':')
            .appendValue(java.time.temporal.ChronoField.MINUTE_OF_HOUR, 2)
            .optionalEnd()
            .optionalStart()
            .appendLiteral(':')
            .appendValue(java.time.temporal.ChronoField.SECOND_OF_MINUTE, 2)
            .optionalEnd()
            .optionalStart()
            .appendFraction(java.time.temporal.ChronoField.NANO_OF_SECOND, 0, 9, true)
            .optionalEnd()
            .optionalStart()
            .appendZoneId()
            .optionalEnd()
            .toFormatter()
            .withZone(java.time.ZoneOffset.UTC);

    private static final java.time.format.DateTimeFormatter FORMATTER = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxxx")
            .withZone(java.time.ZoneOffset.UTC);

    private InstantCodec() {
        super(DataType.timestamp(), java.time.Instant.class);
    }

    @Override
    public ByteBuffer serialize(java.time.Instant value, ProtocolVersion protocolVersion) {
        return value == null ? null : bigint().serializeNoBoxing(value.toEpochMilli(), protocolVersion);
    }

    @Override
    public java.time.Instant deserialize(ByteBuffer bytes, ProtocolVersion protocolVersion) {
        return bytes == null || bytes.remaining() == 0 ? null : java.time.Instant.ofEpochMilli(bigint().deserializeNoBoxing(bytes, protocolVersion));
    }

    @Override
    public String format(java.time.Instant value) {
        if (value == null) {
            return "NULL";
        }
        return quote(FORMATTER.format(value));
    }

    @Override
    public java.time.Instant parse(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("NULL")) {
            return null;
        }
        // strip enclosing single quotes, if any
        if (isQuoted(value)) {
            value = unquote(value);
        }
        if (isLongLiteral(value)) {
            try {
                return java.time.Instant.ofEpochMilli(parseLong(value));
            } catch (NumberFormatException e) {
                throw new InvalidTypeException(String.format("Cannot parse timestamp value from \"%s\"", value));
            }
        }
        try {
            return java.time.Instant.from(PARSER.parse(value));
        } catch (java.time.format.DateTimeParseException e) {
            throw new InvalidTypeException(String.format("Cannot parse timestamp value from \"%s\"", value));
        }
    }
}
