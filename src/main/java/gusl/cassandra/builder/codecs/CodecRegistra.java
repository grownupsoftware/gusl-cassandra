package gusl.cassandra.builder.codecs;

import com.datastax.driver.core.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 */
public class CodecRegistra {

    private final List<TypeCodec<?>> theCodecs;

    public CodecRegistra() {
        theCodecs = new ArrayList<>();
        // Lets add the default codecs
        theCodecs.add(InstantCodec.instance);
        theCodecs.add(LocalDateCodec.instance);
        theCodecs.add(LocalTimeCodec.instance);

    }

    public void registerCodec(TypeCodec<?> codec) {
        theCodecs.add(codec);
    }

    public void registerJsonCodec(Class<?> type) {
        for (TypeCodec<?> codec : theCodecs) {
            if (codec.accepts(type)) {
                // Already registered ..
                return;
            }
        }

        theCodecs.add(new JsonCodec<>(type));
    }

    public void registerWithCassandra(Cluster cluster, CodecRegistry registry) {
        for (TypeCodec<?> codec : theCodecs) {
            registry.register(codec);
        }

        TupleType tupleType = cluster.getMetadata()
                .newTupleType(DataType.timestamp(), DataType.varchar());

        registry.register(new ZonedDateTimeCodec(tupleType));
    }
}
