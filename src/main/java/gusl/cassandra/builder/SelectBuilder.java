package gusl.cassandra.builder;

import com.datastax.driver.core.ConsistencyLevel;
import gusl.core.exceptions.GUSLException;
import lombok.CustomLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author grant
 */
@CustomLog
public class SelectBuilder<T> implements OperationBuilder<SelectBuilder<T>> {

    private List<Column> theColumns;
    //private boolean allColumns = false;
    private Table theTable;
    private Condition theWhereCondition = null;
    private final List<Condition> theAndConditions = new ArrayList<>(3);
    private final CassandraContext theContext;
    private Class<T> theDataClass;
    private String theMetricName;
    private boolean allowFiltering;
    private boolean requiresApollo;
    private boolean ignoreApollo;
    private Column[] orderColumns;
    private int limit;
    private ConsistencyLevel theConsistencyLevel;

    public SelectBuilder(CassandraContext context, List<Column> fields) {
        theColumns = fields;
        theContext = context;
    }

    public SelectBuilder(CassandraContext context, Column... fields) {
        this(context, Arrays.asList(fields));
    }

    public SelectBuilder(CassandraContext context) {
        this(context, (List<Column>) null);
    }

    public SelectBuilder<T> from(Table table) {
        this.theTable = table;
        return this;
    }

    @Override
    public SelectBuilder<T> where(Condition condition) throws GUSLException {
        if (theTable.hasStaticPrimaryKey()) {
            theWhereCondition = theTable.getPrimaryKeys()[0].equal();
            theAndConditions.add(condition);
        } else {
            theWhereCondition = condition;
        }

        requiresApollo = condition.requiresApollo();
        return this;
    }

    @Override
    public SelectBuilder<T> and(Condition condition) {
        theAndConditions.add(condition);
        if (!requiresApollo) {
            requiresApollo = condition.requiresApollo();
        }
        return this;
    }

    public SelectBuilder<T> withPrimaryKeyClauses() {
        if (theTable.getPrimaryKeys().length > 0) {
            theWhereCondition = theTable.getPrimaryKeys()[0].equal();
        }
        for (int x = 1; x < theTable.getPrimaryKeys().length; x++) {
            theAndConditions.add(theTable.getPrimaryKeys()[x].equal());
        }
        return this;
    }

    public SelectBuilder<T> into(Class<T> destClass) {
        this.theDataClass = destClass;
        return this;
    }

    public SelectBuilder<T> withMetric(String metricName) {
        this.theMetricName = metricName;
        return this;
    }

    public SelectBuilder<T> allowFiltering() {
        this.allowFiltering = true;
        return this;
    }

    public SelectBuilder<T> useApollo() {
        this.requiresApollo = true;
        return this;
    }

    public SelectBuilder<T> ignoreApollo() {
        this.ignoreApollo = true;
        return this;
    }

    public SelectBuilder<T> orderBy(Column... orderColumns) {
        this.orderColumns = orderColumns;
        return this;
    }

    public SelectBuilder<T> limit(int limit) {
        this.limit = limit;
        return this;
    }

    public SelectBuilder<T> consistencyLevel(ConsistencyLevel consistencyLevel) {
        this.theConsistencyLevel = consistencyLevel;
        return this;
    }

    public LmSelectExecutor<T> prepare() throws GUSLException {
        if (theTable.hasStaticPrimaryKey() && theWhereCondition == null) {
            theWhereCondition = theTable.getPrimaryKeys()[0].equal();
        }

        if (theColumns == null) {
            theColumns = theTable.getColumnsAsList();
        }

        validate();
        String statement = buildStatementString();
        logger.debug("-------> sql statement: {} Use Apollo: {}", statement, requiresApollo);

        return new LmSelectExecutor<>(theContext, statement, theTable, theColumns, theDataClass, theWhereCondition,
                theAndConditions.toArray(new Condition[theAndConditions.size()]), theMetricName, requiresApollo, ignoreApollo, theConsistencyLevel);

    }

    private void validate() throws GUSLException {
        if (theTable == null) {
            throw new GUSLException("No table specified");
        }
        if (theAndConditions.size() > 0 && theWhereCondition == null) {
            throw new GUSLException("Cannot have 'and' conditions without a where - change one of the 'and' to a where");
        }
    }

    public String buildStatementString() {
        StringBuilder builder = new StringBuilder(400);
        builder.append("SELECT ");

        addFields(builder, theColumns.toArray(new Column[theColumns.size()]));

        builder.append(" FROM ").append(theTable.getKeyspace()).append(".").append(theTable.getTableName());

        if (theWhereCondition != null) {
            if (theWhereCondition.isIn()) {
                builder.append(" WHERE ").append(theWhereCondition.getColumn().getName()).append(" IN (?) ");

            } else {
                builder.append(" WHERE ").append(theWhereCondition.queryParam());
            }
        } else if (theTable.hasStaticPrimaryKey()) {
            builder.append(" WHERE ").append(theTable.getStaticPrimaryKey().getName()).append(" = ? ");
        }

        theAndConditions.stream().forEach(condition -> {
            builder.append(" AND ").append(condition.queryParam());
        });

        if (orderColumns != null) {
            builder.append(" ORDER BY ");
            addFields(builder, orderColumns);
        }

        if (limit > 0) {
            builder.append(" LIMIT ").append(limit);
        }

        if (allowFiltering) {
            builder.append(" ALLOW FILTERING");
        }

        builder.append(";");
        return builder.toString();
    }

    private void addFields(StringBuilder builder, Column[] columns) {
        final List<String> columnNames = Stream.of(columns)
                .map(Column::getName)
                .collect(Collectors.toList());
        builder.append(String.join(", ", columnNames));
    }
}
