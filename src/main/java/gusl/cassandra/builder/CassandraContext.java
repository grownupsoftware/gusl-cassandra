package gusl.cassandra.builder;

import com.datastax.driver.core.Session;
import gusl.cassandra.config.CassandraConnectionConfig;
import gusl.cassandra.utils.CassandraHelper;
import gusl.core.executors.NamedThreadFactory;
import gusl.core.utils.Platform;
import lombok.CustomLog;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author grant
 */
@CustomLog
public class CassandraContext {

    private static final int ASYNC_QUEUE_CAPACITY = 1000;

    private Session theSession;

    // keep a map of all statements we create
    private final Map<LmStatement, LmStatement> theStatementMap = new ConcurrentHashMap<>();

    private ExecutorService executor;

    private final Set<CassandraDAO> theDAOs = new HashSet<>();

    private boolean tracingEnabled = false;
    private boolean isSanityChecking = false;

    public void configure(CassandraConnectionConfig cassandraConnectionConfig) {

        int poolSize = Platform.getNumberOfCores();

        BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>(ASYNC_QUEUE_CAPACITY);

        executor = new ThreadPoolExecutor(poolSize, poolSize, 1, TimeUnit.MINUTES, queue,
                new NamedThreadFactory("cassandra-repository", true, Thread.NORM_PRIORITY),
                new ThreadPoolExecutor.CallerRunsPolicy());

        ((ThreadPoolExecutor) executor).allowCoreThreadTimeOut(false);

        theSession = CassandraHelper.getCassandraSession(cassandraConnectionConfig);
        if (isSantiyChecking()) {
            isSanityChecking = true;
            logger.warn("CassandraSanity enabled");
        }
    }

    public void addDAO(CassandraDAO cassandraDAO) throws Exception {
        try {
            cassandraDAO.setCassandraContext(this);
            cassandraDAO.prepare();

            if (isSanityChecking) {
                cassandraDAO.initialise();
            }

            theDAOs.add(cassandraDAO);
        } catch (Throwable ex) {
            logger.error("Failed to prepare DAO", ex);
            throw ex;
        }
    }

    public void close() {

        if (executor != null) {
            executor.shutdownNow();
        }
        if (theSession != null) {
            theSession.close();
        }
    }

    public boolean hasTracingEnabled() {
        return tracingEnabled;
    }

    public void setTracingEnabled(boolean value) {
        tracingEnabled = value;
    }

    public void register(CassandraDAO dao) {
        theDAOs.add(dao);
    }

    public <T> SelectBuilder<T> select(Column... fields) {
        return new SelectBuilder<>(this, fields);
    }

    public <T> SelectBuilder<T> select() {
        return new SelectBuilder<>(this);
    }

    public Session getSession() {
        return theSession;
    }

    public <T> InsertBuilder<T> insertInto(Table table) {
        return new InsertBuilder<>(this, table);
    }

    public <T> UpdateBuilder<T> update(Table table) {
        return new UpdateBuilder<>(this, table);
    }

    void add(LmStatement mbStatement) {
        theStatementMap.put(mbStatement, mbStatement);
    }

    void remove(LmStatement mbStatement) {
        theStatementMap.remove(mbStatement);
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public boolean isSantiyChecking() {
        return System.getenv("CassandraSanity") != null;
    }
}
