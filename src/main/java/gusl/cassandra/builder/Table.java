package gusl.cassandra.builder;

import gusl.cassandra.builder.codecs.CodecRegistra;
import gusl.core.exceptions.GUSLException;

import java.util.List;
import java.util.Map;

/**
 *
 * @author grant
 */
public interface Table {

    void performValidation() throws GUSLException;

    public Column[] getColumns();

    public String getTableName();

    // LmTableMeta getTableMeta();

    public String createSchema() throws GUSLException;

    public Column[] getPrimaryKeys();

    public boolean hasStaticPrimaryKey();

    public Column getStaticPrimaryKey();

    public String getKeyspace();

    public String getCommaSeparatedColumnNames();

    public void registerCodecs(CodecRegistra codecRegistra);

    Map<String, Column> getColumnsAsMap();

    List<Column> getColumnsAsList();
}
