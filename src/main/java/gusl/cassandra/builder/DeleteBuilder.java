package gusl.cassandra.builder;

import com.datastax.driver.core.ConsistencyLevel;
import java.util.ArrayList;
import java.util.List;
import gusl.core.exceptions.GUSLException;

/**
 *
 * @author grant
 * @param <T> the DO class
 */
public class DeleteBuilder<T> implements OperationBuilder<DeleteBuilder<T>> {

    private static final String SEMI_COLON = ";";

    private final CassandraContext theContext;
    private final Table theTable;
    private Condition theWhereCondition = null;
    private final List<Condition> theAndConditions = new ArrayList<>();
    private String theMetricName;
    private ConsistencyLevel theConsistencyLevel;

    public DeleteBuilder(CassandraContext context, Table table) {
        this.theContext = context;
        this.theTable = table;
    }

    @Override
    public DeleteBuilder<T> where(Condition condition) throws GUSLException {
        theWhereCondition = condition;
        return this;
    }

    /**
     *
     * @param condition
     * @return
     */
    @Override
    public DeleteBuilder<T> and(Condition condition) throws GUSLException {
        theAndConditions.add(condition);
        return this;
    }

    public DeleteBuilder<T> withPrimaryKeyClauses() throws GUSLException {
        if (theTable.getPrimaryKeys().length > 0) {
            where(theTable.getPrimaryKeys()[0].equal());
        }
        for (int x = 1; x < theTable.getPrimaryKeys().length; x++) {
            and(theTable.getPrimaryKeys()[x].equal());
        }
        return this;
    }

    public DeleteBuilder<T> withMetric(String metricName) {
        this.theMetricName = metricName;
        return this;
    }

    public LmDeleteExecutor<T> prepare() throws GUSLException {
        validate();
        String statement = buildStatementString();
        return new LmDeleteExecutor<>(theContext, statement, theTable, theWhereCondition, theAndConditions.toArray(new Condition[theAndConditions.size()]), theMetricName, theConsistencyLevel);
    }

    private void validate() throws GUSLException {
        if (theTable == null) {
            throw new GUSLException("No table specified");
        }
        if (theAndConditions.size() > 0 && theWhereCondition == null) {
            throw new GUSLException("Cannot have 'and' conditions without a where - change one of the 'and' to a where");
        }
        if (theWhereCondition == null) {
            throw new GUSLException("No 'where' clause specified");
        }
    }

    public String buildStatementString() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("DELETE FROM ").append(theTable.getKeyspace()).append(".").append(theTable.getTableName());

        if (theWhereCondition != null) {
            builder.append(" WHERE ").append(theWhereCondition.queryParam());
        }

        theAndConditions.stream().forEach(condition -> {
            builder.append(" AND ").append(condition.queryParam());
        });

        for (Column column : theTable.getColumns()) {
            if (column.getDataType() == Column.DataType.LOCK) {
                builder.append(" IF ").append(column.getName()).append(" = ?");
                theConsistencyLevel = ConsistencyLevel.LOCAL_SERIAL;
                break;
            }
        }
        builder.append(SEMI_COLON);
        return builder.toString();
    }

}
