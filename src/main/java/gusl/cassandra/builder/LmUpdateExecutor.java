package gusl.cassandra.builder;

import com.datastax.driver.core.*;
import gusl.core.exceptions.GUSLException;
import gusl.core.metrics.Latency;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author grant
 */
public class LmUpdateExecutor<T> extends AbstractBaseExecutor<T> {

    public static final String METRIC_PREFIX = "repository-update-";

    private final Column[] theSetColumns;
    private final Condition theWhereCondition;
    private final Condition[] theAndColumns;
    private final boolean useRecord;
    private final Optional<Column> theLockColumn;
    private final ConsistencyLevel theConsistencyLevel;

    LmUpdateExecutor(CassandraContext context, String statement, Table table, Column[] setColumns, Condition whereCondition, Condition[] andColumns, Class<T> dataClass, String metricName, ConsistencyLevel consistencyLevel) throws GUSLException {
        super(context, statement, table, dataClass, metricName, false);
        this.theSetColumns = setColumns;
        this.theWhereCondition = whereCondition;
        this.theAndColumns = andColumns;
        this.useRecord = dataClass != null;
        this.theConsistencyLevel = consistencyLevel;

        theLockColumn = getLockColumn(getTable().getColumns());
        if (useRecord) {
            loadGetterMethods();
        }
    }

    public Updater update() throws GUSLException {
        if (!isInit()) {
            throw new GUSLException("DAO has not been initialised");
        }
        return new Updater(getCassandraContext().getSession(), theSetColumns, theWhereCondition, theAndColumns, useRecord, getTable().getStaticPrimaryKey());
    }

    public class Updater {

        private final Session session;
        private final Object[] values;
        private final int numberSetColumns;
        private final int numberAndColumns;
        private final boolean useRecord;

        private int setIndex = 0;
        private int andIndex = 0;
        private int andCounter = 0;
        private boolean doneSets = false;
        private boolean doneWhere = false;
        private boolean doneAnd = false;
        private boolean doneRecord = false;
        private T theRecord;
        private final Column theStaticPrimaryKey;

        public Updater(Session session, Column[] setColumns, Condition whereCondition, Condition[] andColumns, boolean useRecord, Column staticPrimaryKey) {
            this.session = session;
            this.numberSetColumns = setColumns.length;
            this.numberAndColumns = andColumns.length;
            this.values = new Object[setColumns.length + (whereCondition != null ? 1 : 0) + ((whereCondition == null && staticPrimaryKey != null) ? 1 : 0) + andColumns.length];
            this.useRecord = useRecord;
            this.theStaticPrimaryKey = staticPrimaryKey;

        }

        public Updater where(Object whereValue) throws GUSLException {
            if (useRecord) {
                throw new GUSLException("Cannot specify 'where' with 'record' - it'll work out the where clauseTable["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (!doneSets) {
                throw new GUSLException("Cannot specify 'where' until all 'sets' have been added. [" + setIndex + "] [" + numberSetColumns + "] Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            doneWhere = true;
            if (theStaticPrimaryKey != null) {
                values[setIndex] = theStaticPrimaryKey.getStaticPartitionKeyValue();
                values[++setIndex] = whereValue;
            } else if (theWhereCondition.getColumn().getDataType() == Column.DataType.DATE) {
                values[0] = convertToNoTime((Date) whereValue);
            } else {
                values[setIndex] = whereValue;
            }
            if (setIndex == values.length - 1) {
                doneAnd = true;
            }

            return this;
        }

        public Updater set(Object value) throws GUSLException {
            if (useRecord) {
                throw new GUSLException("When 'updateRecord' is specified in prepare then there are no 'set' - instead use 'record' Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (setIndex > numberSetColumns) {
                throw new GUSLException("Trying to 'set' more columns thans was specified in 'prepare' Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            } else if (setIndex == numberSetColumns - 1) {
                doneSets = true;
            }
            if (theSetColumns[setIndex].isEnum()) {
                if (value != null) {
                    values[setIndex++] = ((Enum<?>) value).toString();
                } else {
                    values[setIndex++] = null;
                }

            } else {
                values[setIndex++] = value;
            }
            return this;
        }

        public Updater record(T record) {
            doneRecord = true;
            theRecord = record;
            return this;
        }

        public Updater and(Object value) throws GUSLException {
            if (!doneWhere) {
                throw new GUSLException("Cannot specify 'and' until 'where' has been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (andIndex > numberAndColumns) {
                throw new GUSLException("Trying to have more 'and' columns thans was specified in 'prepare' Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (theAndColumns[andCounter].getColumn().getDataType() == Column.DataType.DATE) {
                values[setIndex + 1 + andIndex++] = convertToNoTime((Date) value);
            } else {
                values[setIndex + 1 + andIndex++] = value;
            }
            andCounter++;
            doneAnd = true;
            return this;
        }

        private void validate() throws GUSLException {

            if (useRecord && !doneRecord) {
                logger.error("Error: Cannot 'execute' until a 'record' has been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until a 'record' has been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }

            if (!useRecord && !doneSets) {
                logger.error("Error: Cannot 'execute' until all 'sets' have been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until all 'sets' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (!useRecord && !doneWhere) {
                logger.error("Error: Cannot 'execute' until 'where' have been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until 'where' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (!useRecord && !doneAnd && numberAndColumns > 0) {
                logger.error("Error: Cannot 'execute' until all 'and' have been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until all 'and' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (!useRecord && (setIndex + andIndex + 1 != values.length)) {
                logger.error("Error: Cannot 'execute' until all 'and' have been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until all 'and' have been added[ " + (setIndex + andIndex) + "] [" + values.length + "] Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");

            }

        }

        public boolean execute() throws GUSLException {
            validate();
            try {
                try (Latency.Context ctx = getLatencyMetric().time()) {
                    BoundStatement boundStatement = getBoundStatement();
                    ResultSet resultSet = session.execute(getBoundStatement());
                    if (getCassandraContext().hasTracingEnabled()) {
                        logTrace(false, resultSet.getExecutionInfo());
                    }
                    Row row = resultSet.one();
                    return row == null || row.getBool("[applied]");
                }
            } catch (GUSLException ex) {
                logger.error("Sql: {} values: {}", theStatementString, values, ex);
                throw ex;
            } catch (Throwable t) {
                logger.error("Sql: {} values: {}", theStatementString, values, t);
                throw new GUSLException(t.getMessage(), t);
            }
        }

        public CompletableFuture<Boolean> executeAsAsync() throws GUSLException {
            validate();
            try {
                if (theLockColumn.isPresent()) {
                    throw new GUSLException("Cannot execute as async if the table has a lock column");
                }
                CompletableFuture<Boolean> future = new CompletableFuture<>();
                executeNoResultAsAsync(session, getBoundStatement(), future, getLatencyMetric().time());
                return future;
            } catch (GUSLException ex) {
                logger.error("Sql: {} values: {}", theStatementString, values);
                throw ex;
            } catch (Throwable t) {
                logger.error("Sql: {} values: {}", theStatementString, values);
                throw new GUSLException(t.getMessage(), t);
            }
        }

        private BoundStatement getBoundStatement() throws GUSLException {
            BoundStatement boundStatement;
            if (useRecord) {
                boundStatement = createBoundStatementFromRecord(theRecord);
            } else {
                boundStatement = createBoundStatement(theSetColumns, theWhereCondition, theAndColumns, theLockColumn, values);
            }
            logger.debug("Sql: {} - SQL: [{}] set: {} Where: {} And: {} Values: {} ", getStatementAsString(), theSetColumns, theWhereCondition, theAndColumns, values);
            if (theConsistencyLevel != null) {
                logger.debug("Sql: {} - Setting consitency level to: [{}] ", getStatementAsString(), theConsistencyLevel);
                if (theConsistencyLevel == ConsistencyLevel.LOCAL_SERIAL || theConsistencyLevel == ConsistencyLevel.SERIAL) {
                    boundStatement.setSerialConsistencyLevel(theConsistencyLevel);
                } else {
                    boundStatement.setConsistencyLevel(theConsistencyLevel);
                }
            }
            return boundStatement;
        }

        public BoundStatement createUpdateBoundStatement() throws GUSLException {
            if (useRecord) {
                return createBoundStatementFromRecord(theRecord);
            } else {
                return createBoundStatement(theSetColumns, theWhereCondition, theAndColumns, theLockColumn, values);
            }
        }

    }
}
