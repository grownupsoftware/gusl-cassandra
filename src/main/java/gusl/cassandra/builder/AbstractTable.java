package gusl.cassandra.builder;

import gusl.cassandra.builder.codecs.CodecRegistra;
import gusl.core.exceptions.GUSLException;
import gusl.core.tostring.ToString;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author grant
 */
public class AbstractTable implements Table {

    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String NL = "\n";
    private static final String SPACE = " ";
    private static final String COMMA = ",";
    private static final String SEMI_COLON = ";";
    private static final String TAB = "    ";

    private final String theKeyspace;
    private final String theTableName;
//    private LmTableMeta tableMeta;

    private Column[] theColumns;
    private Column[] theKeys;
    private final List<Index> theIndexes = new ArrayList<>(1);
    private boolean hasStaticPrimaryKey = false;
    private Column staticPrimaryKeyColumn;

    public AbstractTable(String keyspace, String tableName) {
        theTableName = tableName;
        theKeyspace = keyspace.toLowerCase();
    }

    public void setColumns(Column... columns) {
        theColumns = columns;
    }

    @Override
    public void registerCodecs(CodecRegistra codecRegistra) {
    }

    @Override
    public void performValidation() throws GUSLException {
        if (theColumns == null || theColumns.length == 0) {
            throw new GUSLException("No columns defined for this table");
        }
        for (Column column : theColumns) {
            if (column.getDataType() == Column.DataType.ENUM
                    && (column.getClasses() == null || column.getClasses().length != 1)) {
                throw new GUSLException("Table: " + theTableName + " Column: " + column.getName() + " ENUM can only have one enum class defined. Classes: " + (column.getClasses() == null ? "null" : column.getClasses()));
            } else if ((column.getDataType() == Column.DataType.SET || column.getDataType() == Column.DataType.LIST)
                    && (column.getClasses() == null || column.getClasses().length != 1)) {
                throw new GUSLException("Table: " + theTableName + " Column: " + column.getName() + " Type: " + column.getDataType() + " must have only ONE class specified. Classes: " + (column.getClasses() == null ? "null" : column.getClasses()));
            } else if (column.getDataType() == Column.DataType.MAP
                    && (column.getClasses() == null || column.getClasses().length != 2)) {
                throw new GUSLException("Table: " + theTableName + " Column: " + column.getName() + " Type: " + column.getDataType() + " must have TWO classes specified. Classes: " + (column.getClasses() == null ? "null" : column.getClasses()));
            } else if (column.getDataType() == Column.DataType.JSON
                    && (column.getClasses() == null || column.getClasses().length != 1)) {
                throw new GUSLException("Table: " + theTableName + " Column: " + column.getName() + " Type: " + column.getDataType() + " must have ONE classes specified. Classes: " + (column.getClasses() == null ? "null" : column.getClasses()));
            }
        }
    }

    public void setPrimaryKeys(Column... keys) {
        theKeys = keys;
        for (Column column : keys) {
            if (column.isStaticPartitionKey()) {
                hasStaticPrimaryKey = true;
                staticPrimaryKeyColumn = column;
                break;
            }
        }
    }

    @Override
    public Column getStaticPrimaryKey() {
        return this.staticPrimaryKeyColumn;
    }

    @Override
    public boolean hasStaticPrimaryKey() {
        return this.hasStaticPrimaryKey;
    }

    @Override
    public Column[] getPrimaryKeys() {
        return theKeys;
    }

    public void addIndex(String indexName, Column... indexes) {
        theIndexes.add(new Index(indexName, indexes));
    }

    @Override
    public Column[] getColumns() {
        return theColumns;
    }

    @Override
    public String getCommaSeparatedColumnNames() {
        return Arrays.stream(theColumns).map(Column::getName).collect(Collectors.joining(","));
    }

    @Override
    public String getTableName() {
        return theTableName;
    }

    @Override
    public String getKeyspace() {
        return theKeyspace;
    }

//    @Override
//    public LmTableMeta getTableMeta() {
//        return tableMeta;
//    }

    private class Index {

        private final Column[] indexes;
        private final String indexName;

        public Index(String indexName, Column... indexes) {
            this.indexName = indexName;
            this.indexes = indexes;
        }

        public Column[] getColumns() {
            return indexes;
        }

        public String getIndexName() {
            return indexName;
        }
    }

    @Override
    public String createSchema() throws GUSLException {

        StringBuilder builder = new StringBuilder();
        builder.append("DROP TABLE IF EXISTS ").append(theTableName).append(SEMI_COLON).append(NL);
        builder.append("CREATE TABLE ").append(theTableName).append(OPEN_BRACKET).append(NL);
        for (Column column : theColumns) {
            builder.append(TAB).append(column.getName()).append(SPACE).append(column.getCassandraType()).append(COMMA).append(NL);
        }

        if (theKeys != null) {
            builder.append(TAB).append("PRIMARY KEY ").append(OPEN_BRACKET);
            int idx = 0;
            for (Column column : theKeys) {
                if (idx > 0) {
                    builder.append(COMMA);
                }
                builder.append(column.getName());
                idx++;
            }
            builder.append(CLOSE_BRACKET).append(NL);
            builder.append(CLOSE_BRACKET).append(SEMI_COLON).append(NL);
        }

        theIndexes.stream().forEach(index -> {
            builder.append("DROP INDEX IF EXISTS ").append(index.getIndexName()).append(SEMI_COLON).append(NL);
            builder.append("CREATE INDEX ").append(index.getIndexName()).append(" ON ").append(theTableName).append(OPEN_BRACKET);

            int idx = 0;
            for (Column column : index.getColumns()) {
                if (idx > 0) {
                    builder.append(COMMA);
                }
                builder.append(column.getName());
                idx++;
            }
            builder.append(CLOSE_BRACKET).append(SEMI_COLON).append(NL);

        });
        return builder.toString();
    }

    @Override
    public Map<String, Column> getColumnsAsMap() {
        HashMap<String, Column> map = new HashMap(theColumns.length);
        for (int i = 0; i < theColumns.length; i++) {
            Column col = theColumns[i];
            map.put(col.getName(), col);
        }

        return map;
    }

    @Override
    public List<Column> getColumnsAsList() {
        List<Column> list = new ArrayList<>(theColumns.length);
        for (int i = 0; i < theColumns.length; i++) {
            list.add(theColumns[i]);
        }
        return list;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
