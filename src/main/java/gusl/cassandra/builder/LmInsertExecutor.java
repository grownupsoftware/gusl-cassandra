package gusl.cassandra.builder;

import com.codahale.metrics.MetricRegistry;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import gusl.core.exceptions.GUSLException;
import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;

import static gusl.core.utils.LambdaExceptionHelper.rethrowFunction;

/**
/**
 *
 * @author grant
 */
public class LmInsertExecutor<T> extends AbstractBaseExecutor<T> {

    public static final String METRIC_PREFIX = "repository-insert-";

    private final Latency bulkLatency;

    public LmInsertExecutor(CassandraContext context, String statement, Table table, Class<T> dataClass, String metricName) throws GUSLException {
        super(context, statement, table, dataClass, metricName, false);

        if (metricName != null) {
            bulkLatency = MetricsFactory.getLatency(MetricRegistry.name(METRIC_PREFIX + "-bulk-" + metricName));
        } else {
            bulkLatency = MetricsFactory.getLatency(MetricRegistry.name(METRIC_PREFIX + table.getTableName() + "-bulk-" + idCounter.getAndIncrement()));
        }

        loadGetterMethods();
    }

    public Inserter insert() throws GUSLException {
        if (!isInit()) {
            throw new GUSLException("DAO has not been initialised");
        }
        return new Inserter(getCassandraContext().getSession());
    }

    public class Inserter {

        private final Session session;
        private T value;
        private boolean addedValue = false;
        private boolean batchMode = false;
        private boolean batchedBounds = false;
        private List<T> batchList;
        private BoundStatement[] boundStatements;

        public Inserter(Session session) {
            this.session = session;
        }

        public Inserter values(T value) {
            this.value = value;
            addedValue = true;
            return this;
        }

        public Inserter batch(List<T> list) {
            addedValue = true;
            batchMode = true;
            batchList = list;
            return this;
        }

        public Inserter batch(BoundStatement... boundStatements) {
            this.boundStatements = boundStatements;
            batchedBounds = true;
            return this;
        }

        private void validate() throws GUSLException {
            if (!addedValue && !batchedBounds) {
                throw new GUSLException("No 'values' specified");
            }
            if (!batchedBounds && (batchMode && (batchList == null || batchList.isEmpty()))) {
                throw new GUSLException("'batch' values are empty");

            }
        }

        public boolean execute() throws GUSLException {
            validate();
            if (batchMode || batchedBounds) {
                try (Latency.Context ctx = bulkLatency.time()) {
                    ResultSet resultSet = session.execute(getBatchStatement());
                    if (getCassandraContext().hasTracingEnabled()) {
                        logTrace(false, resultSet.getExecutionInfo());
                    }
                    Row row = resultSet.one();
                    return row == null || row.getBool("[applied]");
                }
            }

            try (Latency.Context ctx = getLatencyMetric().time()) {
                try {
                    BoundStatement boundStatement = createInsertBoundStatementFromRecord(value);
                    ResultSet resultSet = session.execute(boundStatement);
                    if (getCassandraContext().hasTracingEnabled()) {
                        logTrace(false, resultSet.getExecutionInfo());
                    }

                    Row row = resultSet.one();
                    // applied is only present when IF EXISTS is used
                    return row == null || row.getBool("[applied]");

                } catch (Exception ex) {
                    logger.error("Statement: {} values: {}", getStatementAsString(), getBindValues(value), ex);
                    throw ex;
                }
            }

        }

        private BatchStatement getBatchStatement() {
            BatchStatement batch = new BatchStatement(BatchStatement.Type.LOGGED);

            if (batchedBounds) {
                batch.addAll(Arrays.asList(boundStatements));
            } else {
                List<BoundStatement> statements = batchList.stream().map(rethrowFunction(listItem -> {
                    try {
                        return createInsertBoundStatementFromRecord(listItem);
                    } catch (GUSLException ex) {
                        logger.error("Error creating bound statement", ex);
                        return null;
                    }
                })).collect(Collectors.toList());

                batch.addAll(statements);
            }

            return batch;

        }

        public CompletableFuture<Boolean> executeAsAsync() throws GUSLException {
            validate();
            CompletableFuture<Boolean> future = new CompletableFuture<>();

            if (batchMode || batchedBounds) {
                executeNoResultAsAsync(session, getBatchStatement(), future, bulkLatency.time());
            } else {
                executeNoResultAsAsync(session, createInsertBoundStatementFromRecord(value), future, getLatencyMetric().time());
            }
            return future;

        }

        public BoundStatement createBoundStatement() throws GUSLException {
            validate();
            try {
                return createInsertBoundStatementFromRecord(value);
            } catch (Exception ex) {
                logger.error("Statement: {} values: {}", getStatementAsString(), getBindValues(value), ex);
                throw ex;
            }
        }
    }
}
