package gusl.cassandra.builder;

import gusl.core.exceptions.GUSLException;
import gusl.cassandra.builder.Column.DataType;

/**
 *
 * @author grant
 * @param <T> The DO class
 */
public class InsertBuilder<T> {

    private static final String SPACE = " ";
    private static final String COMMA = ",";
    private static final String QUESTION_MARK = "?";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String SEMI_COLON = ";";

    private final CassandraContext theContext;
    private final Table theTable;
    private Class<T> theDataClass;
    private String theMetricName;
    private int timeToLive = -1;

    public InsertBuilder(CassandraContext context, Table table) {
        this.theContext = context;
        this.theTable = table;
    }

    public InsertBuilder<T> from(Class<T> dataClass) {
        this.theDataClass = dataClass;
        return this;
    }

    public InsertBuilder<T> ttl(int timeToLive) {
        this.timeToLive = timeToLive;
        return this;
    }

    public InsertBuilder<T> withMetric(String metricName) {
        this.theMetricName = metricName;
        return this;
    }

    public LmInsertExecutor<T> prepare() throws GUSLException {
        validate();
        String statement = buildStatementString();
        return new LmInsertExecutor<>(theContext, statement, theTable, theDataClass, theMetricName);
    }

    private void validate() throws GUSLException {
        if (this.theDataClass == null) {
            throw new GUSLException("A 'from' class must be specified");
        }
    }

    public String buildStatementString() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("INSERT INTO ").append(theTable.getKeyspace()).append(".").append(theTable.getTableName()).append(OPEN_BRACKET);

        String values = addColumns(builder, theTable.getColumns());

        builder.append(CLOSE_BRACKET).append(" VALUES ").append(OPEN_BRACKET).append(values).append(CLOSE_BRACKET);

        for (Column column : theTable.getColumns()) {
            if (column.getDataType() == DataType.LOCK) {
                builder.append(" IF NOT EXISTS ");
                break;
            }
        }
        if (timeToLive > 0) {
            builder.append("  USING TTL ").append(this.timeToLive).append(" ");
        }

        builder.append(SEMI_COLON);
        return builder.toString();
    }

    private String addColumns(StringBuilder builder, Column[] columns) {
        StringBuilder valuesBuilder = new StringBuilder(100);
        boolean firstField = true;
        for (Column column : columns) {
            if (!firstField) {
                builder.append(COMMA);
                valuesBuilder.append(COMMA);
            }
            builder.append(SPACE).append(column.getName());
            valuesBuilder.append(QUESTION_MARK);
            firstField = false;
        }

        return valuesBuilder.toString();
    }

}
