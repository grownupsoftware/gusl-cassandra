package gusl.cassandra.builder;

/**
 *
 * @author grant
 */
public class StatementBuilder<T> {

    private static CassandraContext theContext;

    public static void setContext(CassandraContext context) {
        theContext = context;
    }

    public static <T> SelectBuilder<T> select(Column... fields) {
        return new SelectBuilder<>(theContext, fields);
    }

    public static <T> SelectBuilder<T> select() {
        return new SelectBuilder<>(theContext);
    }

    public static <T> InsertBuilder<T> insertInto(Table table) {
        return new InsertBuilder<>(theContext, table);
    }

    public static <T> UpdateBuilder<T> update(Table table) {
        return new UpdateBuilder<>(theContext, table);
    }

    public static <T> DeleteBuilder<T> delete(Table table) {
        return new DeleteBuilder<>(theContext, table);
    }

}
