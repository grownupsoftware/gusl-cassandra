package gusl.cassandra.builder;

import com.codahale.metrics.MetricRegistry;
import com.datastax.driver.core.*;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PageResponse;
import gusl.annotations.form.page.PageResponseBuilder;
import gusl.annotations.form.page.PredicateCache;
import gusl.cassandra.apollo.Apollo;
import gusl.cassandra.apollo.ApolloConstruct;
import gusl.cassandra.apollo.ApolloParseException;
import gusl.cassandra.utils.CassandraUtils;
import gusl.cassandra.utils.RowMapper;
import gusl.core.exceptions.GUSLException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.metrics.Latency;
import gusl.core.metrics.MetricsFactory;
import gusl.core.utils.DateFormats;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * @author grant
 */

public abstract class AbstractBaseExecutor<T> extends CassandraBinder implements LmStatement {

    protected GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    private static final int FETCH_SIZE = 20;

    public static final String METRIC_PREFIX = "repository-";

    private final CassandraContext theContext;
    protected final String theStatementString;
    private PreparedStatement thePreparedStatement;
    private final Table theTable;
    private final Class<T> theDataClass;
    private final String theMetricName;
    private final Latency theLatency;
    protected final boolean requiresApollo;
    private Apollo theApollo;

    private final List<Column> theColumns;

    protected final AtomicLong idCounter = new AtomicLong(1);

    private final PredicateCache<T> thePredicateCache = new PredicateCache<T>();

    public AbstractBaseExecutor(CassandraContext context, String statementString, Table table, Class<T> dataClass, String metricName, boolean isSelect) {
        this(context, statementString, table, table.getColumnsAsList(), dataClass, metricName, isSelect, false, 0, false);
    }

    public AbstractBaseExecutor(CassandraContext context, String statementString, Table table, List<Column> columns, Class<T> dataClass, String metricName, boolean isSelect, boolean requiresApollo, int numParams, boolean ignoreApollo) {
        theContext = context;
        theStatementString = statementString;
        theTable = table;
        theMetricName = metricName;
        theDataClass = dataClass;
        theColumns = columns;

        if (theMetricName != null) {
            theLatency = MetricsFactory.getLatency(MetricRegistry.name(METRIC_PREFIX + theMetricName));
        } else {
            theLatency = MetricsFactory.getLatency(MetricRegistry.name(METRIC_PREFIX + theTable.getTableName() + "-" + idCounter.getAndIncrement()));
        }

        theContext.add(this);

        if (isSelect && !requiresApollo && !ignoreApollo) {
            // create a dummy construct - and check if we need apollo, throw the construct away afterwards
            try {
                theApollo = new Apollo(context.getSession());
                ApolloConstruct apolloConstruct = theApollo.parse(statementString);
                logger.debug("Pure CQL [{}] Parsing sql statement: {} ", apolloConstruct.isPureCQL(), statementString);
                requiresApollo = !apolloConstruct.isPureCQL();

            } catch (ApolloParseException ex) {
                logger.error("\nError parsing sql statement: {} \n{}", statementString, ex.getMessage());
                requiresApollo = true;
            }
        }

        this.requiresApollo = requiresApollo;

        if (!requiresApollo) {
            try {
                // standard CQL - we can prepare statement
                thePreparedStatement = theContext.getSession().prepare(statementString);
            } catch (Exception ex) {
                logger.error("failed to prepare statement: {}", statementString);
                throw ex;
            }

            theApollo = null;
        } else if (!ignoreApollo) {
            // we need apollo
            if (theApollo == null) {
                theApollo = new Apollo(context.getSession());
                logger.warn("Apollo required ................................................ for table {}", table.getTableName());
            }
        }
    }

    public void close() {
        getCassandraContext().remove(this);
    }

    public Latency getLatencyMetric() {
        return theLatency;
    }

    public CassandraContext getCassandraContext() {
        return theContext;
    }

    public Table getTable() {
        return theTable;
    }

    public String getStatementAsString() {
        return theStatementString;
    }

    public Class<T> getDataClass() {
        return theDataClass;
    }

    protected BoundStatement createBoundStatement(Condition whereCondition, Condition[] andColumns, Optional<Column> lockColumn, Object[] values) throws GUSLException {
        return createBoundStatement(null, whereCondition, andColumns, lockColumn, values);
    }

    protected BoundStatement createBoundStatement(Column[] setColumns, Condition whereCondition, Condition[] andColumns, Optional<Column> lockColumn, Object[] values) throws GUSLException {
        BoundStatement boundStatement = thePreparedStatement.bind();
        int index = 0;
        if (setColumns != null) {
            for (Column column : setColumns) {
                bindValue(boundStatement, index, column, values[index], false);
                index++;
            }
        }
        if (whereCondition != null) {
            bindValue(boundStatement, index, whereCondition.getColumn(), values[index], true);
            index++;
        }

        for (Condition condition : andColumns) {
            bindValue(boundStatement, index, condition.getColumn(), values[index], true);
            index++;
        }

        if (lockColumn.isPresent()) {
            Column column = lockColumn.get();
            bindValue(boundStatement, index, column, values[index], false);
            index++;
        }
        return boundStatement;
    }

    @SuppressWarnings("unchecked")

    private int createBaseBoundStatementFromRecord(T record, StringBuilder builder, BoundStatement boundStatement, boolean excludeKeys) throws GUSLException {

        int index = 0;
        for (Column column : theColumns) {
            if (excludeKeys && matchesPrimaryKey(getTable().getPrimaryKeys(), column)) {
                continue;
            }

            builder.append(" ").append(column.getName()).append("[");
            try {
                if (column.isStaticPartitionKey()) {
                    boundStatement.setString(index, column.getStaticPartitionKeyValue());
                    builder.append(column.getStaticPartitionKeyValue());
                } else {
                    Object value = column.getGetterMethod().invoke(record);
                    bindValue(boundStatement, index, column, value, true);
                    builder.append(value);
                }

                builder.append("]");

            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                logger.error("Error invoking getter Column: {} DataType: {} GetterMethod: {} Class: {}", column.getName(), column.getDataType(), column.getGetterMethod().getName(), record.getClass().getCanonicalName(), ex);
                throw new GUSLException("Error invoking getter", ex);
            }
            index++;
        }

        if (theContext.hasTracingEnabled()) {
            boundStatement.enableTracing();
        }

        return index;
    }

    public BoundStatement createBoundStatementFromRecord(T record) throws GUSLException {
        StringBuilder logBuilder = new StringBuilder(300);

        BoundStatement boundStatement = thePreparedStatement.bind();
        int index = createBaseBoundStatementFromRecord(record, logBuilder, boundStatement, true);

        logBuilder.append(" **WHERE** ");
        for (Column column : getTable().getPrimaryKeys()) {
            logBuilder.append(" ").append(column.getName()).append("[");
            try {
                if (column.isStaticPartitionKey()) {
                    boundStatement.setString(index, column.getStaticPartitionKeyValue());
                    logBuilder.append(column.getStaticPartitionKeyValue());
                } else {
                    Object value = column.getGetterMethod().invoke(record);
                    logBuilder.append(value);

                    if (column.getDataType() == Column.DataType.DATE) {
                        if (value != null) {
                            boundStatement.setTimestamp(index, convertToNoTime((Date) value));
                        } else {
                            boundStatement.setTimestamp(index, null);
                        }
                    } else {
                        if (value == null && column.hasNullValue()) {
                            // translate value
                            setValue(boundStatement, column, column.getNullValue(), index);
                        } else {
                            setValue(boundStatement, column, value, index);
                        }
                    }
                }
                index++;
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                logger.error("Error getting value for column: {} Method: {} Record: {}", column.getName(), column.getGetterMethod().getName(), record);
                throw new GUSLException("Error invoking getter", ex);
            }
        }

        // final pass - check for lock column
        for (Column column : theColumns) {
            if (column.getDataType() == Column.DataType.LOCK) {
                logBuilder.append(" **LOCK** ");
                logBuilder.append(" ").append(column.getName()).append("[");
                try {
                    Object value = column.getGetterMethod().invoke(record);
                    logBuilder.append(value);
                    if (value == null) {
                        boundStatement.setInt(index, 1);
                    } else {
                        boundStatement.setInt(index, (Integer) value);
                    }
                    logBuilder.append("]");

                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    logger.error("Error getting value for column: {} Method: {} Record: {}", column.getName(), column.getGetterMethod().getName(), record);
                    throw new GUSLException("Error invoking getter", ex);
                }

                index++;
                break;
            }
        }

        logger.debug("SQL : {} values: {} ", theStatementString, logBuilder.toString());
        return boundStatement;

    }

    public BoundStatement createInsertBoundStatementFromRecord(T value) throws GUSLException {
        StringBuilder builder = new StringBuilder(300);
        BoundStatement boundStatement = thePreparedStatement.bind();
        createBaseBoundStatementFromRecord(value, builder, boundStatement, false);
        logger.debug("SQL : {} values: {} record: {}", theStatementString, builder.toString(), value);
        return boundStatement;
    }

    @SuppressWarnings("unchecked")
    protected Object[] getBindValues(T doObject) throws GUSLException {
        Object[] bindValues = new Object[theColumns.size()];
        int index = 0;
        try {
            for (Column column : theColumns) {
                try {
                    if (column.isStaticPartitionKey()) {
                        bindValues[index] = column.getStaticPartitionKeyValue();
                    } else if (column.getDataType() == Column.DataType.ENUM) {
                        Object enumValue = column.getGetterMethod().invoke(doObject);
                        if (enumValue != null) {
                            bindValues[index] = enumValue.toString();
                        }
                    } else if (column.getDataType() == Column.DataType.MAP) {
                        Object value = column.getGetterMethod().invoke(doObject);
                        if (value != null) {
                            Map<String, Object> props = theObjectMapper.convertValue(value, Map.class);
                            bindValues[index] = props;

                        }
                    } else {
                        Object value = column.getGetterMethod().invoke(doObject);
                        if (value != null && column.getDataType() == Column.DataType.ENUM) {
                            bindValues[index] = value.toString();
                        } else if (value != null && column.getDataType() == Column.DataType.MAP) {
                            Map<String, Object> props = theObjectMapper.convertValue(value, Map.class);
                            bindValues[index] = props;
                        } else if (value == null && column.hasNullValue()) {
                            // translate value
                            bindValues[index] = column.getNullValue();
                        } else {
                            bindValues[index] = value;
                        }
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    logger.error("Error invoking getter", ex);
                    throw new Exception("Error invoking getter", ex);
                }
                index++;
            }
            logger.debug("BindValues: {} doObject: {}", bindValues, doObject);
            return bindValues;
        } catch (Throwable t) {
            logger.error("doObject: {} BindValues: {} Index: {} Error: {}", doObject, bindValues, index, t.getMessage());
            throw new GUSLException("Error binding values", t);
        }
    }

    private boolean matchesPrimaryKey(Column[] primaryKeys, Column column) {
        for (Column col : primaryKeys) {
            if (col.getName().equals(column.getName())) {
                return true;
            }
        }
        return false;
    }

    protected PageResponse<T> executeForList(Session session, Statement statement, PageRequest request) throws GUSLException {

        long total = -1;

        if (isNull(request)) {
            throw new GUSLException("No request params");
        }

        final List<Predicate<T>> predicates = thePredicateCache.getPredicates(request, theDataClass);

        logger.debug("statement: {} request: {} predicates: {}", statement, request, predicates);

        String pageState = null;

        if (nonNull(request.getPageStates())) {
            int offset = (request.getOffset() < 1) ? 0 : request.getOffset();
            // currentPageStates = request.getPageStates();
            if (offset < request.getPageStates().length) {
                // going backwards
                offset--;
                if (offset >= 0) {
                    pageState = request.getPageStates()[offset];
                }
            } else {
                // go forwards
                if (request.getPageStates().length > 0) {
                    pageState = request.getPageStates()[request.getPageStates().length - 1];
                }
            }
        }

        if (nonNull(pageState)) {
            logger.debug("using page state: {}", pageState);
            try {
                statement.setPagingState(PagingState.fromString(pageState));
            } catch (Throwable t) {
                logger.warn("Invalid page state - going back to first page");
                request.setPageStates(new String[0]);
                request.setOffset(0);
            }
        }

        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }
        try {
            if (total < 0 && predicates.isEmpty()) {
                final ResultSet countResultSet = session.execute("select count(*) as count from " + theTable.getKeyspace() + "." + theTable.getTableName());
                Row r = countResultSet.one();
                total = r.getLong("count");
            } else if (total < 0) {
                final ResultSet countResultSet = session.execute("select * from " + theTable.getKeyspace() + "." + theTable.getTableName());
                total = CassandraUtils.count(
                        countResultSet,
                        this,
                        theDataClass,
                        theColumns,
                        predicates
                );
            }

            statement.setFetchSize(request.getPerPage());
            ResultSet result = session.execute(statement);
            if (theContext.hasTracingEnabled()) {
                logTrace(true, result.getExecutionInfo());
            }

            final String[] newPageStates = copyArray(request.getPageStates(), 1);
            if (nonNull(result.getExecutionInfo().getPagingState())) {
                newPageStates[newPageStates.length - 1] = result.getExecutionInfo().getPagingState().toString();
            } else {
                newPageStates[newPageStates.length - 1] = "no_more";
            }
            final List<T> resultList = CassandraUtils.fetch(
                    result,
                    request.getPerPage(),
                    this,
                    theDataClass,
                    theColumns,
                    predicates
            );

            final PageResponseBuilder<T> responsePage = PageResponseBuilder.<T>builder()
                    .offset(request.getOffset())
                    .perPage(request.getPerPage())
                    .pageStates(newPageStates)
                    .total(total)
                    .orderParamTieBreaker(request.getOrderParamTieBreaker())
                    .orderDirection(request.getOrderDirection())
                    .orderParam(request.getOrderParam())
                    .content(resultList)
                    .queryConditions(request.getQueryConditions())
                    .after(request.getAfter())
                    .before(request.getBefore())
                    .convertTo(request.getConvertTo())
                    .indexClass(request.getIndexClass())
                    .last(request.getLast())
                    .build();
            logger.debug("Response: Statement: {} CassandraPage: {}", statement, responsePage);
            return responsePage;
        } catch (Throwable t) {
            logger.error("Error sql {}", theStatementString, t);
            throw new GUSLException(t.getMessage(), t);
        }

    }

    private String[] copyArray(String[] from, int plus) {
        if (isNull(from) || from.length == 0) {
            return new String[]{""};
        }
        String[] to = new String[from.length + 1];
        for (int x = 0; x < from.length; x++) {
            to[x] = from[x];
        }
        to[from.length] = new String("");
        return to;
    }

    protected List<T> executeForList(Session session, Statement statement) throws GUSLException {
        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }
        try {
            ResultSet result = session.execute(statement);
            statement.setFetchSize(FETCH_SIZE);
            if (theContext.hasTracingEnabled()) {
                logTrace(true, result.getExecutionInfo());
            }
            return CassandraUtils.fetchAll(result, this, theDataClass, theColumns);
        } catch (Throwable t) {
            logger.error("Error sql {}", theStatementString, t);
            throw new GUSLException(t.getMessage(), t);
        }
    }

    protected Stream<T> executeForStream(Session session, Statement statement) throws GUSLException {
        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }
        statement.setFetchSize(10);
        try {
            statement.setFetchSize(FETCH_SIZE);
            ResultSet result = session.execute(statement);
            if (theContext.hasTracingEnabled()) {
                logTrace(true, result.getExecutionInfo());
            }
            return CassandraUtils.stream(result, this, theDataClass, theColumns);
        } catch (Throwable t) {
            logger.error("Error sql {}", theStatementString, t);
            throw new GUSLException(t.getMessage(), t);
        }
    }

    protected void executeForListAsAsync(Session session, Statement statement, CompletableFuture<List<T>> future, Latency.Context latencyContext) {

        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }
        ResultSetFuture resultFuture = session.executeAsync(statement);
        resultFuture.addListener(() -> {
            try {
                ResultSet result = resultFuture.get();
                if (theContext.hasTracingEnabled()) {
                    logTrace(true, result.getExecutionInfo());
                }

                future.complete(CassandraUtils.fetchAll(result, this, theDataClass, theColumns));

            } catch (ExecutionException e) {
                future.completeExceptionally(new GUSLException(e.getCause().getMessage(), e.getCause()));
            } catch (Throwable t) {
                future.completeExceptionally(new GUSLException(t.getMessage(), t));
            } finally {
                latencyContext.close();
            }
        }, theContext.getExecutor());
    }

    protected Stream<T> streamUsingApollo(Session session, Object[] values) throws GUSLException {
        logger.debug("Asking Apollo to query: {} {} ", theStatementString, values);

        ApolloConstruct apolloConstruct = theApollo.parse(theStatementString);
        apolloConstruct.setArgs(values);

        return theApollo.stream(apolloConstruct, this, theDataClass, theColumns);
    }

    protected List<T> executeForListUsingApollo(Session session, Object[] values) throws GUSLException {
        logger.debug("Asking Apollo to query: {} {} ", theStatementString, values);

        ApolloConstruct apolloConstruct = theApollo.parse(theStatementString);
        apolloConstruct.setArgs(values);

        return theApollo.execute(apolloConstruct, this, theDataClass, theColumns);
    }

    protected void executeForListAsAsyncUsingApollo(Session session, Object[] values, CompletableFuture<List<T>> future, Latency.Context latencyContext) throws GUSLException {

        logger.debug("Asking Apollo to query ASYNC : {} {} ", theStatementString, values);

        final RowMapper mapper = this;

        ApolloConstruct apolloConstruct = theApollo.parse(theStatementString);
        apolloConstruct.setArgs(values);

        theContext.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    apolloConstruct.setArgs(values);
                    future.complete(theApollo.execute(apolloConstruct, mapper, theDataClass, theColumns));
                } catch (Throwable t) {
                    future.completeExceptionally(t);
                } finally {
                    latencyContext.close();
                }
            }
        });
    }

    protected void executeForObjectAsAsync(Session session, Statement statement, CompletableFuture<Optional<T>> future, Latency.Context latencyContext) {
        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }

        ResultSetFuture resultFuture = session.executeAsync(statement);
        resultFuture.addListener(() -> {
            try {
                ResultSet result = resultFuture.get();
                if (theContext.hasTracingEnabled()) {
                    logTrace(false, result.getExecutionInfo());
                }
                Row row = result.one();

                if (row == null) {
                    future.complete(Optional.empty());
                } else {
                    future.complete(Optional.of(mapRow(row, theDataClass, theColumns)));
                }
            } catch (ExecutionException e) {
                future.completeExceptionally(e.getCause());
            } catch (Throwable t) {
                future.completeExceptionally(t);
            } finally {
                latencyContext.close();
            }
        }, theContext.getExecutor());
    }

    protected Optional<T> executeForObject(Session session, Statement statement) throws GUSLException {
        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }

        ResultSet result = session.execute(statement);
        if (theContext.hasTracingEnabled()) {
            logTrace(false, result.getExecutionInfo());
        }

        Row row = result.one();
        if (row == null) {
            return Optional.empty();
        }

        try {
            return Optional.of(mapRow(row, theDataClass, theColumns));
        } catch (Throwable t) {
            throw new GUSLException(t.getMessage(), t);
        }
    }

    protected void executeForObjectAsAsyncUsingApollo(Session session, Object[] values, CompletableFuture<Optional<T>> future, Latency.Context latencyContext) throws GUSLException {

        ApolloConstruct apolloConstruct = theApollo.parse(theStatementString);
        apolloConstruct.setArgs(values);

        theContext.getExecutor().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Row> dataList = theApollo.execute(apolloConstruct, values);
                    if (dataList == null || dataList.isEmpty()) {
                        future.complete(Optional.empty());
                    } else {
                        future.complete(Optional.of(mapRow(dataList.get(0), theDataClass, theColumns)));
                    }
                } catch (Throwable t) {
                    future.completeExceptionally(t);
                } finally {
                    latencyContext.close();
                }
            }
        });
    }

    protected Optional<T> executeForObjectUsingApollo(Session session, Object[] values) throws GUSLException {

        ApolloConstruct apolloConstruct = theApollo.parse(theStatementString);
        apolloConstruct.setArgs(values);

        List<T> list = theApollo.execute(apolloConstruct, this, theDataClass, theColumns);

        if (list == null || list.isEmpty()) {
            return Optional.empty();
        }

        try {
            return Optional.of(list.get(0));
        } catch (Throwable t) {
            throw new GUSLException(t.getMessage(), t);
        }
    }

    protected void executeNoResultAsAsync(Session session, Statement statement, CompletableFuture<Boolean> future, Latency.Context latencyContext) {
        if (theContext.hasTracingEnabled()) {
            statement.enableTracing();
        }

        ResultSetFuture resultFuture = session.executeAsync(statement);
        resultFuture.addListener(() -> {
            try {
                ResultSet result = resultFuture.get();
                if (theContext.hasTracingEnabled()) {
                    logTrace(false, result.getExecutionInfo());
                }
                future.complete(Boolean.TRUE);
            } catch (ExecutionException e) {
                future.completeExceptionally(e.getCause());
            } catch (Throwable t) {
                future.completeExceptionally(t);
            } finally {
                latencyContext.close();
            }
        }, getCassandraContext().getExecutor());
    }

    protected void loadGetterMethods() throws GUSLException {
        for (Column column : getTable().getColumns()) {
            if (!column.isStaticPartitionKey()) {
                Method getterMethod;
                getterMethod = findGetterMethod(column);
                if (getterMethod == null) {
                    throw new GUSLException("No getter method found for column: " + column.getName() + " - looking for " + getMethodName("get or is", normaliseName(column.getName())) + " in class: " + theDataClass.getCanonicalName());
                }
                column.setGetterMethod(getterMethod);
            }
        }
    }

    private Method findGetterMethod(Column column) {
        String name = normaliseName(column.getName());

        Method getterMethod;
        try {
            getterMethod = theDataClass.getMethod(getMethodName("get", name));
        } catch (NoSuchMethodException ex) {
            try {
                getterMethod = theDataClass.getMethod(getMethodName("is", name));
            } catch (NoSuchMethodException e) {
                return null;
            }
        }
        if (getterMethod != null) {
            return getterMethod;
        }
        logger.warn("Failed to find getter " + getMethodName("get or is", name) + " in Class: " + theDataClass.getCanonicalName()
                + " ignoring and carrying on");

        return null;
    }

    protected Optional<Column> getLockColumn(Column[] columns) {
        for (Column column : columns) {
            if (column.getDataType() == Column.DataType.LOCK) {
                return Optional.of(column);
            }
        }
        return Optional.empty();
    }

    protected void logTrace(boolean isList, ExecutionInfo executionInfo) {
        StringBuilder builder = new StringBuilder(500);
        builder.append("\n=================================================================================================================================================================\n");
        builder.append(String.format("\n%s Statement: %s\n\n", isList ? "List" : "Singleton", theStatementString));
        builder.append(String.format("\tHost (queried): %s\n", executionInfo.getQueriedHost().toString()));
        executionInfo.getTriedHosts().stream().forEach((host) -> {
            builder.append(String.format("\tHost (tried): %s\n", host.toString()));
        });
        QueryTrace queryTrace = executionInfo.getQueryTrace();
        if (queryTrace != null) {
            builder.append(String.format("\tTrace id: %s\n\n", queryTrace.getTraceId()));
            builder.append(String.format("\t%-100s | %-12s | %-10s | %-12s\n", "activity", "timestamp", "source", "source_elapsed"));
            builder.append(String.format("\t-----------------------------------------------------------------------------------------------------+--------------+------------+---------------\n"));
            queryTrace.getEvents().stream()
                    .limit(20)
                    .forEach((event) -> {
                        builder.append(String.format("\t%-100s | %12s | %10s | %12s\n", event.getDescription(),
                                millis2Date(event.getTimestamp()),
                                event.getSource(), event.getSourceElapsedMicros()));
                    });
            if (queryTrace.getEvents().size() > 20) {
                builder.append("...output restricted to 20 entries\n");
            }
        }

        builder.append("=================================================================================================================================================================\n");
        logger.info(builder.toString());
    }

    private String millis2Date(long timestamp) {
        return DateFormats.getUTCFormatFor("HH:mm:ss.SSS").format(timestamp);
    }

    public boolean isInit() {
        return theContext != null;
    }
}
