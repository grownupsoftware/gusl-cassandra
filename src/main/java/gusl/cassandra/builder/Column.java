package gusl.cassandra.builder;

import gusl.core.exceptions.GUSLException;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * @author grant
 */
public class Column {

    public static enum DataType {
        LONG(Long.MIN_VALUE),
        STRING("8A085DFC3E5BEF71F611D372D8C0040E9A525F08B9B53DE9F0804946218E0FB8"),
        BOOLEAN(Boolean.FALSE),
        INTEGER(Integer.MAX_VALUE),
        BIGDECIMAL(BigDecimal.valueOf(Long.MIN_VALUE)),
        DATE(new Date(1)),
        TIMESTAMP(new Date(1)),
        DOUBLE(Double.MIN_VALUE),
        FLOAT(Float.MIN_VALUE),
        ENUM("8A085DFC3E5BEF71F611D372D8C0040E9A525F08B9B53DE9F0804946218E0FB8"), //not supported...,
        SET(Collections.emptySet()),
        LIST(Collections.emptyList()),
        MAP(Collections.emptyMap()),
        JSON("8A085DFC3E5BEF71F611D372D8C0040E9A525F08B9B53DE9F0804946218E0FB8"),
        LOCK(null),
        LOCAL_DATE(new Date(1)),
        LOCAL_TIME(new Date(1)),
        // nano timestamp
        TIMESTAMP_INSTANT(new Date(1)),
        PASSWORD("8A085DFC3E5BEF71F611D372D8C0040E9A525F08B9B53DE9F0804946218E0FB8"),
        PLONG(Long.MIN_VALUE),
        PINT(Integer.MIN_VALUE),
        PDOUBLE(Double.MIN_VALUE),
        OBJECT_MAP(Collections.emptyMap()),
        ZONED_DATE_TIME(new Date(1));

        private final Object emptyValue;
        private final BiFunction compareFunction = Objects::equals;

        DataType(Object emptyValue) {
            this.emptyValue = emptyValue;
        }
    }

    private final String name;
    private final DataType dataType;
    private final Class<?>[] classes;
    private Method setterMethod;
    private Method getterMethod;
    private final boolean partitionKey;
    private final String partitionKeyValue;
    private Object nullValue;

    private Column(String name, DataType dataType, Class<?>[] classes, boolean partitionKey, String partitionKeyValue, Object nullValue) {
        this.name = name;
        this.dataType = dataType;
        this.classes = classes;
        this.partitionKey = partitionKey;
        this.partitionKeyValue = partitionKeyValue;
        this.nullValue = nullValue;
    }

    public Column(String name, DataType dataType) {
        this.name = name;
        this.dataType = dataType;
        this.classes = null;
        this.partitionKey = false;
        this.partitionKeyValue = null;
    }

    /**
     * For primitives Cassandra does not have a null value, so we use nullValue
     * so that when there is a NULL we translate (identify) it with the
     * nullValue inbound and outbound
     *
     * @param name
     * @param dataType
     * @param nullValue
     */
    public Column(String name, DataType dataType, Object nullValue) {
        this.name = name;
        this.dataType = dataType;
        this.classes = null;
        this.partitionKey = false;
        this.partitionKeyValue = null;
        this.nullValue = nullValue;
    }

    public Column(String name, DataType dataType, Class<?> clazz) {
        this.name = name;
        this.dataType = dataType;
        this.partitionKey = false;
        this.partitionKeyValue = null;
        this.classes = new Class[]{clazz};
    }

    public Column(String name, String partitionKeyValue) {
        this.name = name;
        this.dataType = DataType.STRING;
        this.classes = null;
        this.partitionKey = true;
        this.partitionKeyValue = partitionKeyValue;
    }

    public Column(String name, DataType dataType, Class<?>... classes) {
        this.name = name;
        this.dataType = dataType;
        this.classes = classes;
        this.partitionKey = false;
        this.partitionKeyValue = null;
    }

    public boolean isStaticPartitionKey() {
        return this.partitionKey;
    }

    public String getStaticPartitionKeyValue() {
        return this.partitionKeyValue;
    }

    public String getName() {
        return name;
    }

    public DataType getDataType() {
        return dataType;
    }

    public Class<?>[] getClasses() {
        return this.classes;
    }

    @SuppressWarnings("unchecked")
    public Class<Enum<?>> getEnumClass() {
        if (this.classes != null && this.classes.length > 0) {
            return (Class<Enum<?>>) this.classes[0];
        }
        return null;
    }

    public boolean isEnum() {
        return dataType == DataType.ENUM;
    }

    public boolean isSet() {
        return dataType == DataType.SET;
    }

    public boolean isList() {
        return dataType == DataType.LIST;
    }

    public boolean isMap() {
        return dataType == DataType.MAP;
    }

    public boolean hasNullValue() {
        return nullValue != null;
    }

    public Condition equal() {
        return new Condition(this, Condition.Operand.EQUAL);
    }

    public Condition notEqual() {
        return new Condition(this, Condition.Operand.NOT_EQUAL);
    }

    public Condition like() {
        return new Condition(this, Condition.Operand.LIKE);
    }

    public Condition in() throws GUSLException {
        return new Condition(this, Condition.Operand.IN);
    }

    public Condition greaterThan() throws GUSLException {
        return new Condition(this, Condition.Operand.GREATER_THAN);
    }

    public Condition equalsOrGreaterThan() {
        return new Condition(this, Condition.Operand.GREATER_THAN_EQUALS);
    }

    public Condition lessThan() {
        return new Condition(this, Condition.Operand.LESS_THAN);
    }

    public Condition equalsOrLessThan() {
        return new Condition(this, Condition.Operand.LESS_THAN_EQUALS);
    }

    public Method getSetterMethod() {
        return setterMethod;
    }

    public void setSetterMethod(Method setterMethod) {
        this.setterMethod = setterMethod;
    }

    public Method getGetterMethod() {
        return getterMethod;
    }

    public void setGetterMethod(Method getterMethod) {
        this.getterMethod = getterMethod;
    }

    public Object getNullValue() {
        return nullValue;
    }

    @Override
    public String toString() {
        return "Column{" + "name=" + name + ", dataType=" + dataType + ", Classes=" + classes + ", setterMethod=" + setterMethod + ", getterMethod=" + getterMethod + '}';
    }

    public String getCassandraType() throws GUSLException {
        switch (dataType) {
            case PLONG:
            case LONG:
                return "bigint";
            case PASSWORD:
            case STRING:
                return "varchar";
            case BOOLEAN:
                return "boolean";
            case PINT:
            case INTEGER:
                return "int";
            case BIGDECIMAL:
                return "decimal";
            case DATE:
                return "timestamp";
            case TIMESTAMP:
                return "timestamp";
            case PDOUBLE:
            case DOUBLE:
                return "decimal";
            case FLOAT:
                return "decimal";
            case LOCK:
                return "int";
            case ENUM:
                break;
            case LOCAL_DATE:
                return "date";
            case LOCAL_TIME:
                return "time";
            case TIMESTAMP_INSTANT:
                return "timestamp";
            case OBJECT_MAP:
                return "varchar";
            case ZONED_DATE_TIME:
                return "timestamp";
            default:
                throw new GUSLException("No known Cassandra Type for Data Type: " + dataType);
        }
        return null;
    }

    public Class<?> getJavaClass() throws GUSLException {
        switch (dataType) {
            case PLONG:
                return long.class;
            case PINT:
                return int.class;
            case PDOUBLE:
                return double.class;
            case LONG:
                return Long.class;
            case PASSWORD:
            case STRING:
                return String.class;
            case BOOLEAN:
                return Boolean.class;
            case INTEGER:
                return Integer.class;
            case BIGDECIMAL:
                return BigDecimal.class;
            case DATE:
                return Date.class;
            case TIMESTAMP:
                return Date.class;
            case DOUBLE:
                return Double.class;
            case FLOAT:
                return Float.class;
            case SET:
                return Set.class;
            case LIST:
                return List.class;
            case MAP:
                return Map.class;
            case LOCK:
                return Integer.class;
            case ENUM:
                break;
            case LOCAL_DATE:
                return LocalDate.class;
            case LOCAL_TIME:
                return LocalTime.class;
            case TIMESTAMP_INSTANT:
                return Instant.class;
            case JSON:
                return classes[0];
            case OBJECT_MAP:
                return Map.class;
            case ZONED_DATE_TIME:
                return ZonedDateTime.class;
            default:
                throw new GUSLException("No known Java Type for Cassandra Data Type: " + dataType);
        }
        return null;
    }

    /**
     * Can't use the same getters and setters for Views.
     *
     * @return a copy of this
     */
    public Column copy() {
        return new Column(name, dataType, classes, partitionKey, partitionKeyValue, nullValue);
    }
}
