package gusl.cassandra.builder;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import gusl.core.exceptions.GUSLException;
import gusl.core.metrics.Latency;

/**
 *
 * @author grant
 */
public class LmDeleteExecutor<T> extends AbstractBaseExecutor<T> {

    public static final String METRIC_PREFIX = "repository-delete-";

    private final Condition theWhereCondition;
    private final Condition[] theAndColumns;
    private final String sqlStatement;
    private final ConsistencyLevel theConsistencyLevel;

    LmDeleteExecutor(CassandraContext context, String statement, Table table, Condition whereCondition, Condition[] andColumns, String metricName, ConsistencyLevel consistencyLevel) {
        super(context, statement, table, null, metricName, false);
        this.theWhereCondition = whereCondition;
        this.theAndColumns = andColumns;
        this.sqlStatement = statement;
        this.theConsistencyLevel = consistencyLevel;
    }

    public Deleter delete() throws GUSLException {
        if (!isInit()) {
            throw new GUSLException("DAO has not been initialised");
        }
        Optional<Column> lockColumn = getLockColumn(getTable().getColumns());
        return new Deleter(getCassandraContext().getSession(), theWhereCondition, theAndColumns, getTable().getStaticPrimaryKey(), lockColumn);
    }

    public class Deleter {

        private final Session session;
        private final Object[] values;
        private final int numberAndColumns;

        private int andIndex = 0;
        private boolean doneWhere = false;
        private boolean doneAnd = false;
        private final Column theStaticPrimaryKey;
        private final Condition theWhereCondition;
        private final Condition[] theAndColumns;
        private final Optional<Column> theLockColumn;

        public Deleter(Session session, Condition whereCondition, Condition[] andColumns, Column staticPrimaryKey, Optional<Column> lockColumn) {
            this.session = session;
            this.numberAndColumns = andColumns.length;
//            this.values = new Object[(whereCondition != null ? 1 : 0) + ((whereCondition == null && staticPrimaryKey != null) ? 1 : 0) + andColumns.length];
            this.values = new Object[(whereCondition != null ? 1 : 0) + ((whereCondition == null && staticPrimaryKey != null) ? 1 : 0) + andColumns.length + (lockColumn.isPresent() ? 1 : 0)];
            this.theStaticPrimaryKey = staticPrimaryKey;
            this.theWhereCondition = whereCondition;
            this.theAndColumns = andColumns;
            this.theLockColumn = lockColumn;
        }

        public Deleter where(Object whereValue) {
            doneWhere = true;
            if (theStaticPrimaryKey != null) {
                values[0] = theStaticPrimaryKey.getStaticPartitionKeyValue();
                values[1] = whereValue;
                andIndex++;
                if (andIndex >= numberAndColumns) {
                    doneAnd = true;
                }
            } else {
                values[0] = whereValue;
            }
            return this;
        }

        public Deleter and(Object value) throws GUSLException {
            if (!doneWhere) {
                throw new GUSLException("Cannot specify 'and' until 'where' has been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (andIndex > numberAndColumns) {
                throw new GUSLException("Trying to have more 'and' columns thans was specified in 'prepare' Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            values[++andIndex] = value;
            doneAnd = true;
            return this;
        }

        public Deleter lockValue(Object value) throws GUSLException {
            if (!doneWhere || !doneAnd) {
                throw new GUSLException("Cannot specify 'lockValue' until 'where' or any 'and' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            int index = andIndex + 1;
            values[index] = value;
            return this;
        }

        private void validate() throws GUSLException {
            if (!doneWhere) {
                logger.error("Error: Cannot 'execute' until 'where' have been added. Statement: {} {}", theStatementString, values);
                throw new GUSLException("Cannot 'execute' until 'where' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (!doneAnd && numberAndColumns > 0) {
                logger.error("Error: Cannot 'execute' until all 'and' have been added. [{}  {}] Statement: {} {}", doneAnd, numberAndColumns, theStatementString, values);
                throw new GUSLException("Cannot 'execute' until all 'and' have been added Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }
            if (andIndex + 1 + (theLockColumn.isPresent() ? 1 : 0) != values.length) {
                logger.error("Error: Cannot 'execute' until all 'and' have been added. [{} = {}] Statement: {} {}", (andIndex + 1), values.length, theStatementString, values);
                throw new GUSLException("Cannot 'execute' until all 'and' have been added[ " + (andIndex + 1) + "] [" + values.length + "] Table["+ getTable().getTableName()+"] statement["+getStatementAsString()+"]");
            }

        }

        public BoundStatement asBoundStatement() throws GUSLException {
            validate();
            return createBoundStatement(theWhereCondition, theAndColumns, theLockColumn, values);
        }

        public boolean execute() throws GUSLException {
            validate();
            try (Latency.Context ctx = getLatencyMetric().time()) {
                logger.debug("Delete: SQL: {} (pre-codec) values: {}", sqlStatement, values);
                BoundStatement boundStatement = createBoundStatement(theWhereCondition, theAndColumns, theLockColumn, values);
                if (theConsistencyLevel != null) {
                    logger.debug("Sql: {} - Setting consitency level to: [{}] ", getStatementAsString(), theConsistencyLevel);
                    if (theConsistencyLevel == ConsistencyLevel.LOCAL_SERIAL || theConsistencyLevel == ConsistencyLevel.SERIAL) {
                        boundStatement.setSerialConsistencyLevel(theConsistencyLevel);
                    } else {
                        boundStatement.setConsistencyLevel(theConsistencyLevel);
                    }
                }
                ResultSet resultSet = session.execute(boundStatement);

                if (getCassandraContext().hasTracingEnabled()) {
                    logTrace(false, resultSet.getExecutionInfo());
                }

                Row row = resultSet.one();
                if (row == null) {
                    return true;
                }
                if (row.getColumnDefinitions().asList().size() > 1) {
                    try {
                        logger.warn("Delete not applied. applied: {} lock: {}", row.getBool("[applied]"), row.getInt("lock"));
                    } catch (Exception ex) {
                        // ignore may not have a column called lock
                    }
                }
                return row.getBool("[applied]");
            }
        }

        public CompletableFuture<Boolean> executeAsAsync() throws GUSLException {
            if (theLockColumn.isPresent()) {
                throw new GUSLException("Cannot execute as async if the table has a lock column");
            }
            validate();
            CompletableFuture<Boolean> future = new CompletableFuture<>();
            BoundStatement boundStatement = createBoundStatement(theWhereCondition, theAndColumns, theLockColumn, values);
            if (theConsistencyLevel != null) {
                logger.debug("Sql: {} - Setting consitency level to: [{}] ", getStatementAsString(), theConsistencyLevel);
                if (theConsistencyLevel == ConsistencyLevel.LOCAL_SERIAL || theConsistencyLevel == ConsistencyLevel.SERIAL) {
                    boundStatement.setSerialConsistencyLevel(theConsistencyLevel);
                } else {
                    boundStatement.setConsistencyLevel(theConsistencyLevel);
                }
            }

            executeNoResultAsAsync(session, boundStatement, future, getLatencyMetric().time());
            return future;
        }

    }
}
