package gusl.cassandra.apollo;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.LocalDate;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Token;
import com.datastax.driver.core.TupleValue;
import com.datastax.driver.core.TypeCodec;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.exceptions.InvalidTypeException;
import com.google.common.reflect.TypeToken;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * This is used when we use Count or Sum
 *
 * @author dhudson
 */
public class ApolloFunctionRow implements Row {

    private final String theName;
    private final Number theValue;

    ApolloFunctionRow(String name, Number value) {
        theName = name;
        theValue = value;
    }

    /**
     * Return the function result.
     *
     * @return
     */
    public Number getFunctionResult() {
        return theValue;
    }

    @Override
    public ColumnDefinitions getColumnDefinitions() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public Token getToken(int i) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Token getToken(String string) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Token getPartitionKeyToken() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public boolean isNull(int i) {
        checkIndex(i);
        return (theValue == null);
    }

    @Override
    public boolean getBool(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public byte getByte(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public short getShort(int i) {
        checkIndex(i);
        return theValue.shortValue();
    }

    @Override
    public int getInt(int i) {
        checkIndex(i);
        return theValue.intValue();
    }

    @Override
    public long getLong(int i) {
        checkIndex(i);
        return theValue.longValue();
    }

    @Override
    public Date getTimestamp(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public LocalDate getDate(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public long getTime(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public float getFloat(int i) {
        checkIndex(i);
        return theValue.floatValue();
    }

    @Override
    public double getDouble(int i) {
        checkIndex(i);
        return theValue.doubleValue();
    }

    @Override
    public ByteBuffer getBytesUnsafe(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public ByteBuffer getBytes(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public String getString(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public BigInteger getVarint(int i) {
        checkIndex(i);
        return new BigInteger(Integer.toString(theValue.intValue()));
    }

    @Override
    public BigDecimal getDecimal(int i) {
        checkIndex(i);
        return new BigDecimal(Double.toString(theValue.doubleValue()));
    }

    @Override
    public UUID getUUID(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public InetAddress getInet(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> List<T> getList(int i, Class<T> type) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> List<T> getList(int i, TypeToken<T> tt) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> Set<T> getSet(int i, Class<T> type) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> Set<T> getSet(int i, TypeToken<T> tt) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <K, V> Map<K, V> getMap(int i, Class<K> type, Class<V> type1) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <K, V> Map<K, V> getMap(int i, TypeToken<K> tt, TypeToken<V> tt1) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public UDTValue getUDTValue(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public TupleValue getTupleValue(int i) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public Object getObject(int i) {
        if (i == 0) {
            return theValue;
        }

        throw new IndexOutOfBoundsException();
    }

    @Override
    public <T> T get(int i, Class<T> type) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public <T> T get(int i, TypeToken<T> tt) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public <T> T get(int i, TypeCodec<T> tc) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public boolean isNull(String string) {
        checkString(string);
        return theValue == null;
    }

    @Override
    public boolean getBool(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public byte getByte(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public short getShort(String string) {
        checkString(string);
        return theValue.shortValue();
    }

    @Override
    public int getInt(String string) {
        checkString(string);
        return theValue.intValue();
    }

    @Override
    public long getLong(String string) {
        checkString(string);
        return theValue.longValue();
    }

    @Override
    public Date getTimestamp(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public LocalDate getDate(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public long getTime(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public float getFloat(String string) {
        checkString(string);
        return theValue.floatValue();
    }

    @Override
    public double getDouble(String string) {
        checkString(string);
        return theValue.doubleValue();
    }

    @Override
    public ByteBuffer getBytesUnsafe(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public ByteBuffer getBytes(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public String getString(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public BigInteger getVarint(String string) {
        checkString(string);
        return new BigInteger(Integer.toString(theValue.intValue()));
    }

    @Override
    public BigDecimal getDecimal(String string) {
        checkString(string);
        return new BigDecimal(Double.toString(theValue.doubleValue()));
    }

    @Override
    public UUID getUUID(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public InetAddress getInet(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> List<T> getList(String string, Class<T> type) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> List<T> getList(String string, TypeToken<T> tt) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> Set<T> getSet(String string, Class<T> type) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <T> Set<T> getSet(String string, TypeToken<T> tt) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <K, V> Map<K, V> getMap(String string, Class<K> type, Class<V> type1) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public <K, V> Map<K, V> getMap(String string, TypeToken<K> tt, TypeToken<V> tt1) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public UDTValue getUDTValue(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public TupleValue getTupleValue(String string) {
        throw new InvalidTypeException("Expected Number");
    }

    @Override
    public Object getObject(String string) {
        checkString(string);
        return theValue;
    }

    @Override
    public <T> T get(String string, Class<T> type) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public <T> T get(String string, TypeToken<T> tt) {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public <T> T get(String string, TypeCodec<T> tc) {
        throw new UnsupportedOperationException("Not supported.");
    }

    private void checkString(String name) {
        if (!theName.equals(name)) {
            throw new IllegalArgumentException();
        }
    }

    private void checkIndex(int index) {
        if (index != 0) {
            throw new IndexOutOfBoundsException();
        }
    }
}
