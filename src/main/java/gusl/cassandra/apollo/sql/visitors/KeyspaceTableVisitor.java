package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitorAdapter;

/**
 * Get the keyspace and table name from the sql.
 *
 * @author dhudson
 */
public class KeyspaceTableVisitor extends FromItemVisitorAdapter {

    private final ApolloConstruct theConstruct;

    public KeyspaceTableVisitor(ApolloConstruct construct) {
        theConstruct = construct;
    }

    public void parse(FromItem fromItem) {
        fromItem.accept(this);
    }

    @Override
    public void visit(Table table) {
        theConstruct.setKeyspaceName(table.getSchemaName());
        theConstruct.setTableName(table.getName());
    }

}
