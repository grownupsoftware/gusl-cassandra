package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.schema.Column;

/**
 * Order By visitor.
 *
 * @author dhudson
 */
public class OrderByVisitor extends ExpressionVisitorAdapter {

    private final ApolloConstruct theConstruct;

    public OrderByVisitor(ApolloConstruct construct) {
        theConstruct = construct;
    }

    @Override
    public void visit(Column column) {
        theConstruct.addOrderByColumn(column.getColumnName());
    }
}
