package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import lombok.CustomLog;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.schema.Column;

/**
 * Select Visitor.
 *
 * @author dhudson
 */
@CustomLog
public class SelectedItemVisitor extends ExpressionVisitorAdapter {

    private final ApolloConstruct theConstruct;

    public SelectedItemVisitor(ApolloConstruct construct) {
        theConstruct = construct;
    }

    @Override
    public void visit(Function function) {
        logger.debug("Function {}", function);
        theConstruct.addSelectItem("*");
        theConstruct.setCountFunction();
    }

    @Override
    public void visit(Column column) {
        theConstruct.addSelectItem(column.getColumnName());
    }
}
