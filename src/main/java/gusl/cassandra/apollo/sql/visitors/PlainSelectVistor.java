package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import lombok.CustomLog;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.statement.select.*;

import java.util.List;

/**
 * Main starting point for visitors.
 *
 * @author dhudson
 */
@CustomLog
public class PlainSelectVistor extends SelectVisitorAdapter {

    private final KeyspaceTableVisitor theTableVisitor;
    private final ExpressionColumnVisitor theColumnVisitor;
    private final ExpressionVisitor theExpressionVisitor;
    private final SelectedItemVisitor theSelectedItemVisitor;
    private final OrderByVisitor theOrderByVisitor;

    private final ApolloConstruct theContruct;

    public PlainSelectVistor(ApolloConstruct construct) {
        theContruct = construct;
        theTableVisitor = new KeyspaceTableVisitor(construct);
        theColumnVisitor = new ExpressionColumnVisitor(construct);
        theExpressionVisitor = new ExpressionVisitor(construct);
        theSelectedItemVisitor = new SelectedItemVisitor(construct);
        theOrderByVisitor = new OrderByVisitor(construct);
    }

    @Override
    public void visit(PlainSelect plainSelect) {
        logger.debug("visit: PlainSelect {}", plainSelect);

        List<SelectItem> selectedItems = plainSelect.getSelectItems();

        for (SelectItem selectItem : selectedItems) {
            selectItem.accept(new SelectItemVisitorAdapter() {
                @Override
                public void visit(SelectExpressionItem item) {
                    item.getExpression().accept(theColumnVisitor);
                    item.getExpression().accept(theSelectedItemVisitor);
                }

                @Override
                public void visit(AllColumns columns) {
                    theContruct.addSelectItem("*");
                }

            });
        }

        if (plainSelect.getLimit() != null) {
            logger.warn("******** check the code - this has changed when updating version");
            //theContruct.setLimit(plainSelect.getLimit().getRowCount());
        }

        plainSelect.getFromItem().accept(theTableVisitor);
        List<OrderByElement> orderByCols = plainSelect.getOrderByElements();

        if (orderByCols != null) {
            for (OrderByElement orderBy : orderByCols) {
                if (orderBy.getExpression() != null) {
                    orderBy.getExpression().accept(theColumnVisitor);
                    orderBy.getExpression().accept(theOrderByVisitor);
                }
            }
        }

        Expression expression = plainSelect.getWhere();
        if (expression != null) {
            expression.accept(theColumnVisitor);
            expression.accept(theExpressionVisitor);
        }

    }

}
