package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.schema.Column;

/**
 * Column Visitor.
 *
 * @author dhudson
 */
public class ExpressionColumnVisitor extends ExpressionVisitorAdapter {

    private final ApolloConstruct theConstruct;

    public ExpressionColumnVisitor(ApolloConstruct construct) {
        theConstruct = construct;
    }

    @Override
    public void visit(Column column) {
        theConstruct.addColumn(column.getColumnName());
    }

}
