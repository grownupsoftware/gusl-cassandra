package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import gusl.cassandra.apollo.expression.*;
import gusl.cassandra.apollo.expression.side.*;
import lombok.CustomLog;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;

/**
 * Expression Visitor.
 * <p>
 * This visitor does most of the work.
 *
 * @author dhudson
 */
@CustomLog
public class ExpressionVisitor extends ExpressionVisitorAdapter {

    private ApolloSimpleExpression theExpression;
    private final ApolloConstruct theConstruct;

    public ExpressionVisitor(ApolloConstruct construct) {
        theConstruct = construct;
    }

    @Override
    public void visit(AnalyticExpression expr) {
        super.visit(expr);
        logger.info("I have an analytic expression of {} ", expr);
    }

    @Override
    protected void visitBinaryExpression(BinaryExpression expr) {
        logger.debug("BinaryExpression {}:{}", expr, expr.getClass());
        if (expr instanceof MinorThan) {
            processSimpleExpression(new ApolloLTExpression(), expr);
        }

        if (expr instanceof GreaterThanEquals) {
            processSimpleExpression(new ApolloGreaterThanEqualsExpression(), expr);
        }

        if (expr instanceof MinorThanEquals) {
            processSimpleExpression(new ApolloLessThanEqualsExpression(), expr);
        }

        if (expr instanceof AndExpression) {
            processConditionalExpression(new ApolloAndExpression(), expr);
        }

        if (expr instanceof OrExpression) {
            processConditionalExpression(new ApolloOrExpression(), expr);
        }

    }

    private void processConditionalExpression(ApolloConditionalExpression expression, BinaryExpression expr) {
        theConstruct.addExpression(expression);
        expr.getLeftExpression().accept(this);
        expr.getRightExpression().accept(this);
        theConstruct.popExpression();
    }

    @Override
    public void visit(InExpression expr) {
        logger.debug("In Expr {}", expr);
        ApolloInExpression expression = new ApolloInExpression();
        theExpression = expression;
        // Col or constant
        expr.getLeftExpression().accept(this);
        InItemsListVisitor visitor = new InItemsListVisitor(expression, theConstruct);
        expr.getRightItemsList().accept(visitor);
        theConstruct.addExpression(expression);
    }

    @Override
    public void visit(Between expr) {

    }

    @Override
    public void visit(NotEqualsTo expr) {
        logger.debug("NoEqualsTo {}", expr);
        processSimpleExpression(new ApolloNotEqualsExpression(), expr);
    }

    @Override
    public void visit(EqualsTo expr) {
        logger.debug("EqualsTo {}", expr);
        processSimpleExpression(new ApolloEqualsExpression(), expr);
    }

    @Override
    public void visit(GreaterThan expr) {
        logger.debug("GreaterThan {}", expr);
        processSimpleExpression(new ApolloGTExpression(), expr);
    }

    @Override
    public void visit(LikeExpression expr) {
        logger.debug("Like {}", expr);
        processSimpleExpression(new ApolloLikeExpression(), expr);
    }

    private void processSimpleExpression(ApolloSimpleExpression expression, BinaryExpression expr) {
        theExpression = expression;
        if (expr.isNot()) {
            theExpression.setIsNot();
        }
        expr.getLeftExpression().accept(this);
        expr.getRightExpression().accept(this);
        theConstruct.addExpression(expression);
    }

    @Override
    public void visit(JdbcParameter parameter) {
        logger.debug("JdbcParameter {}", parameter);
        theExpression.addSide(new ParamSide(theConstruct));
    }

    @Override
    public void visit(LongValue value) {
        logger.debug("LongValue {}", value);
        theExpression.addSide(new LongSide(value.getValue()));
    }

    @Override
    public void visit(DoubleValue value) {
        logger.debug("DoubleValue {}", value);
        theExpression.addSide(new DoubleSide(value.getValue()));
    }

    @Override
    public void visit(Column column) {
        logger.debug("Column {}", column);
        theExpression.addSide(new ColumnSide(column.getColumnName()));
    }

    @Override
    public void visit(StringValue value) {
        logger.debug("StringValue {}", value);
        theExpression.addSide(new StringSide(value.getValue()));
    }

    @Override
    public void visit(TimestampValue value) {
        logger.debug("TimestampValue {}", value.getValue());
        theExpression.addSide(new TimestampSide(value.getValue()));
    }

    @Override
    public void visit(TimeValue value) {
        logger.debug("TimeValue {}", value);
        theExpression.addSide(new TimeSide(value.getValue()));
    }

    @Override
    public void visit(DateValue value) {
        logger.debug("DateValue {}", value);
        theExpression.addSide(new DateSide(value.getValue()));
    }

    /*--*/
    @Override
    public void visit(SelectExpressionItem selectExpressionItem) {
        logger.debug("SelectExpressionItem {}", selectExpressionItem);
    }

    @Override
    public void visit(AllTableColumns allTableColumns) {
        logger.debug("AllTableColumns {}", allTableColumns);
    }

    @Override
    public void visit(AllColumns allColumns) {
        logger.debug("AllColumns {}", allColumns);
    }

    @Override
    public void visit(Parenthesis parenthesis) {
        logger.debug("Parenthesis {}", parenthesis);
    }

}
