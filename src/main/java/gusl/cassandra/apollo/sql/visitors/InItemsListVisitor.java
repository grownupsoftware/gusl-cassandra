package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import gusl.cassandra.apollo.expression.ApolloInExpression;
import gusl.cassandra.apollo.expression.side.ListSide;
import lombok.CustomLog;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitorAdapter;
import net.sf.jsqlparser.expression.operators.relational.MultiExpressionList;
import net.sf.jsqlparser.statement.select.SubSelect;

/**
 * Get the List of Items.
 *
 * @author dhudson
 */
@CustomLog
public class InItemsListVisitor extends ItemsListVisitorAdapter {

    private final ListSide theRightSide;
    private final ApolloConstruct theConstruct;

    public InItemsListVisitor(ApolloInExpression expr, ApolloConstruct construct) {
        theConstruct = construct;
        theRightSide = new ListSide(construct);
        expr.addSide(theRightSide);
    }

    @Override
    public void visit(MultiExpressionList multiExprList) {
        logger.debug("MultiExpressionList {}", multiExprList);
    }

    @Override
    public void visit(ExpressionList expressionList) {
        logger.debug("ExpressionList {}", expressionList);
        for (Expression expression : expressionList.getExpressions()) {
            expression.accept(new ExpressionVisitorAdapter() {
                @Override
                public void visit(StringValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(TimestampValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(TimeValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(DateValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(LongValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(DoubleValue value) {
                    theRightSide.addConstant(value.getValue());
                }

                @Override
                public void visit(JdbcParameter parameter) {
                    logger.debug("In ExpressionList JDBC {}", parameter);
                    theRightSide.addJDBCParam();
                }

            });
        }
    }

    @Override
    public void visit(SubSelect subSelect) {
        logger.debug("SubSelect {}", subSelect);
    }
}
