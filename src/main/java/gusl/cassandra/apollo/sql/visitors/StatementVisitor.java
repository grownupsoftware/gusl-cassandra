package gusl.cassandra.apollo.sql.visitors;

import gusl.cassandra.apollo.ApolloConstruct;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitorAdapter;
import net.sf.jsqlparser.statement.select.Select;

/**
 * Statement Visitor.
 *
 * @author dhudson
 */
public class StatementVisitor extends StatementVisitorAdapter {

    private final PlainSelectVistor theSelectVistor;

    public StatementVisitor(ApolloConstruct construct) {
        theSelectVistor = new PlainSelectVistor(construct);
    }

    public void parse(Statement statement) {
        statement.accept(this);
    }

    @Override
    public void visit(Select select) {
        select.getSelectBody().accept(theSelectVistor);
    }
}
