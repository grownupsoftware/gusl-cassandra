package gusl.cassandra.apollo;

import com.datastax.driver.core.Row;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Different sent of rules required for Count(*)
 *
 * @author dhudson
 */
public class CountConstructProcessor extends AbstractConstructProcessor {

    private int theRowCount;

    CountConstructProcessor(ApolloConstruct construct) {
        super(construct);
    }

    @Override
    public boolean process(Row t) {
        if (theRowCount == 0) {
            if (hasException()) {
                // Stop processing
                return false;
            }

            if (getConstruct().requiresColNames()) {
                Set<String> resultingCols = new LinkedHashSet<>(1);
                resultingCols.add("count");
                getConstruct().setResultingColName(resultingCols);
            }
        }

        theRowCount++;
        return true;
    }

    @Override
    public List<Row> getRows() {
        ArrayList<Row> rows = new ArrayList<>(1);
        rows.add(new ApolloFunctionRow("count", theRowCount));
        return rows;
    }
}
