package gusl.cassandra.apollo;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import lombok.CustomLog;

import java.util.HashMap;
import java.util.Map;

/**
 * Cache Prepared statements for re-use.
 *
 * @author dhudson
 */
@CustomLog
public class ApolloStatementCache {

    private static final Map<Session, Map<Integer, PreparedStatement>> theSessionMap = new HashMap<>(2);

    private ApolloStatementCache() {
    }

    public static PreparedStatement getPreparedStatement(Session session, ApolloConstruct construct) {
        Map<Integer, PreparedStatement> preparedStatementMap = theSessionMap.get(session);
        if (preparedStatementMap == null) {
            preparedStatementMap = new HashMap<>();
            theSessionMap.put(session, preparedStatementMap);
        }

        String cql = construct.getCQL();
        PreparedStatement statement = preparedStatementMap.get(cql.hashCode());
        if (statement != null) {
            logger.debug("(N) Had {} returned {} ", cql, statement);
            return statement;
        }

        statement = session.prepare(cql);
        preparedStatementMap.put(cql.hashCode(), statement);
        logger.debug("(C) Had {} returned {} ", cql, statement);
        return statement;
    }
}
