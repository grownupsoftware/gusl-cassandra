package gusl.cassandra.apollo;

/**
 * Parse Exception.
 *
 * This is thrown during the parse cycle.
 *
 * @author dhudson
 */
public class ApolloParseException extends ApolloException {

    private static final long serialVersionUID = 953350524371473476L;

    static final String INVALID_TABLE = "Invalid table";
    static final String INVALID_KEYSPACE = "Invalid Keyspace";
    static final String NO_SESSION_SET = "No session set";
    static final String SESSION_CLOSED = "Session Closed";
    static final String UNABLE_TO_PARSE = "Unable to parse";
    static final String INVALID_COLUMN = "Invalid column";
    static final String NOT_SELECT_STATEMENT = "Not a select statement";

    public ApolloParseException(String reason) {
        super(reason);
    }

    public ApolloParseException(String reason, Throwable cause) {
        super(reason, cause);
    }

}
