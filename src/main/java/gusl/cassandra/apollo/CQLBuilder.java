package gusl.cassandra.apollo;

import com.datastax.driver.core.ColumnMetadata;
import gusl.cassandra.apollo.expression.ApolloConditionalExpression;
import gusl.cassandra.apollo.expression.ApolloExpression;
import gusl.cassandra.apollo.expression.ApolloSimpleExpression;
import gusl.cassandra.apollo.expression.side.ColumnSide;
import gusl.core.utils.StringUtils;
import lombok.CustomLog;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Build the CQL from the Construct
 * <p>
 * The idea here is to look for indexable columns and make sure that they get
 * passed to the CQL so only the minimum amount of data is returned from
 * Cassandra
 *
 * @author dhudson
 */
@CustomLog
public class CQLBuilder {

    private final ApolloConstruct theConstruct;
    private StringBuilder theBuilder;
    private boolean firstExpression = true;
    private final Set<String> theIndexable;
    private boolean isPureCQL;
    private ArrayList<Integer> theCQLArgs;

    public CQLBuilder(ApolloConstruct construct) {
        theConstruct = construct;
        theIndexable = construct.getIndexableCols();
    }

    // This is re-usable
    void build() throws ApolloExpressionException {
        theBuilder = new StringBuilder(1000);
        isPureCQL = true;
        theCQLArgs = new ArrayList<>(2);

        theBuilder.append("SELECT ");
        theBuilder.append(theConstruct.getSelectedItems().stream().collect(Collectors.joining(",")));
        theBuilder.append(" FROM ");

        if (StringUtils.isNotBlank(theConstruct.getKeyspaceName())) {
            theBuilder.append(theConstruct.getKeyspaceName());
            theBuilder.append(".");
        }
        theBuilder.append(theConstruct.getTableName());

        // Lets walk the expression and see if we can add the expression to CQL
        if (theConstruct.getRootExpression() != null) {
            walk(theConstruct.getRootExpression());
        }

        if (theConstruct.hasLimit()) {
            theBuilder.append(" LIMIT ");
            theBuilder.append(theConstruct.getLimit());
        }

        if (theConstruct.isAllowFiltering()) {
            theBuilder.append(" ALLOW FILTERING");
        }
    }

    private void walk(ApolloExpression expression) throws ApolloExpressionException {
        if (expression instanceof ApolloSimpleExpression) {
            ApolloSimpleExpression expr = (ApolloSimpleExpression) expression;
            for (ColumnSide colSide : expr.getColumnSides()) {

                // Set the data type of the 
                ColumnMetadata colMD = theConstruct.getTableMetadata().getColumn(colSide.getColumnName());
                expr.setDataTypeName(colMD.getType().getName());

                if (theIndexable.contains(colSide.getColumnName())) {
                    if (expr.isCQLExpression()) {
                        int index = expr.setParamIsIndexable();
                        // Can be constant values
                        if (index != -1) {
                            theCQLArgs.add(index);
                        } else {
                            // Its indexable and a constant, so we don't really need the expression
                            expr.setDummyExpression(true);
                        }
                        if (firstExpression) {
                            theBuilder.append(" WHERE ");
                            firstExpression = false;
                        } else {
                            ApolloConditionalExpression parent = (ApolloConditionalExpression) expr.getParent();
                            theBuilder.append(" ");
                            theBuilder.append(parent.getConditionalKeyword());
                            theBuilder.append(" ");
                        }
                        theBuilder.append(expr.getSQL());
                    } else {
                        isPureCQL = false;
                        expr.setDummyExpression(false);
                    }
                } else {
                    // Its an expression that is not indexed
                    isPureCQL = false;
                    expr.setDummyExpression(false);
                }
            }
        } else {
            // Its a conditional expression
            ApolloConditionalExpression expr = (ApolloConditionalExpression) expression;
            walk(expr.getLeftSide());
            walk(expr.getRightSide());
        }
    }

    boolean isPureCQL() {
        return isPureCQL;
    }

    String getCQL() {
        return theBuilder.toString();
    }

    Object[] getCQLArgs() {
        Object[] args = new Object[theCQLArgs.size()];
        for (int i = 0; i < theCQLArgs.size(); i++) {
            args[i] = theConstruct.getArgAt(theCQLArgs.get(i));
        }
        return args;
    }
}
