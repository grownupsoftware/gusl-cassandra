package gusl.cassandra.apollo;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.LocalDate;
import com.datastax.driver.core.Row;
import java.util.Comparator;

/**
 * Handle Order By.
 *
 * This row comparator is nested, if this compares equals 0 then the next (if
 * exists) comparator is used.
 *
 * @author dhudson
 */
public class RowComparator implements Comparator<Row> {

    private final DataType.Name theType;
    private final String theColumnName;
    private RowComparator theNextComparator;

    public RowComparator(String columnName, DataType.Name type) {
        theColumnName = columnName;
        theType = type;
    }

    void setNextComparator(RowComparator comparator) {
        theNextComparator = comparator;
    }

    @Override
    public int compare(Row row1, Row row2) {
        int comp;
        switch (theType) {
            case DOUBLE:
                comp = Double.compare(row1.getDouble(theColumnName), row2.getDouble(theColumnName));
                break;
            case BIGINT:
                comp = Long.compare(row1.getLong(theColumnName), row2.getLong(theColumnName));
                break;
            case DECIMAL:
                comp = compareValues(row1.getDecimal(theColumnName), row2.getDecimal(theColumnName));
                break;

            case VARCHAR:
            case TEXT:
                comp = row1.getString(theColumnName).compareTo(row2.getString(theColumnName));
                break;
            case DATE:
                LocalDate dt1 = row1.getDate(theColumnName);
                LocalDate dt2 = row2.getDate(theColumnName);
                comp = compareValues(dt1 == null ? null : dt1.getMillisSinceEpoch(), dt2 == null ? null : dt2.getMillisSinceEpoch());

                break;
            case TIMESTAMP:
                comp = compareValues(row1.getTimestamp(theColumnName), row2.getTimestamp(theColumnName));
                break;

            default:
                comp = 0;
        }

        if (comp == 0 && theNextComparator != null) {
            return theNextComparator.compare(row1, row2);
        }
        return comp;
    }

    private <E extends Comparable<E>> int compareValues(E val1, E val2) {
        if (val1 == null && val2 == null) {
            return 0;
        } else if (val1 != null) {
            return val1.compareTo(val2);
        } else {
            return 1;
        }
    }
}
