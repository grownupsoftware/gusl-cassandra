package gusl.cassandra.apollo;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import gusl.cassandra.apollo.expression.ApolloConditionalExpression;
import gusl.cassandra.apollo.expression.ApolloExpression;

/**
 * The Construct.
 *
 * Contains all the information to run the CQL and filter the results.
 *
 * Need to run construct(), to get the CQL and isPure
 *
 * @author dhudson
 */
public class ApolloConstruct {

    private static final int DEFAULT_FETCH_SIZE = 6000;
    private static final int DEFAULT_FETCH_REFRESH = 1000;

    private String theKeyspaceName;
    private String theTableName;
    private ApolloExpression theRootExpression;

    private TableMetadata theTableMetadata;
    private final Set<String> theColumns;
    private final List<String> theSelectItems;
    private final String theSQL;
    private Object[] theArgs;
    private int theArgIndex;
    private int theJDBCParamCount;
    private long theLimit;
    private final List<String> theOrderByCols;
    private final HashSet<String> theIndexable;
    private boolean isAllowFiltering;

    private final Stack<ApolloExpression> theExpressionStack;

    private int theFetchSize;
    private int theFetchRefresh;

    private final CQLBuilder theCQLBuilder;

    private Set<String> resultingColNames;

    private boolean requiresColNames;
    private long theLastRunTime;

    private int theResultLimit;

    private ConsistencyLevel theConsistencyLevel;
    private boolean isCount;

    // Used to flag that an exception has occurred during paring.
    private ApolloExpressionException theExpressionException;

    public ApolloConstruct(String sql, Object[] args) {
        theSQL = sql;
        theArgs = args;
        theColumns = new HashSet<>();
        theSelectItems = new ArrayList<>();
        theExpressionStack = new Stack<>();
        theOrderByCols = new ArrayList<>(2);
        theIndexable = new HashSet<>();
        theFetchSize = DEFAULT_FETCH_SIZE;
        theFetchRefresh = DEFAULT_FETCH_REFRESH;
        theCQLBuilder = new CQLBuilder(this);
    }

    public String getKeyspaceName() {
        return theKeyspaceName;
    }

    public void setKeyspaceName(String keyspaceName) {
        theKeyspaceName = keyspaceName;
    }

    public String getTableName() {
        return theTableName;
    }

    public void setTableName(String tableName) {
        theTableName = tableName;
    }

    public TableMetadata getTableMetadata() {
        return theTableMetadata;
    }

    public void setTableMetadata(TableMetadata tableMetadata) {
        theTableMetadata = tableMetadata;
    }

    public void addExpression(ApolloExpression expression) {
        if (theRootExpression == null) {
            theRootExpression = expression;
        } else {
            theExpressionStack.peek().addExpression(expression);
        }

        if (expression instanceof ApolloConditionalExpression) {
            theExpressionStack.push(expression);
        }

    }

    public void popExpression() {
        theExpressionStack.pop();
    }

    public ApolloExpression getExpression() {
        return theRootExpression;
    }

    public void addColumn(String column) {
        theColumns.add(column);
    }

    public Set<String> columns() {
        return theColumns;
    }

    public List<String> getSelectedItems() {
        return theSelectItems;
    }

    public void addSelectItem(String item) {
        theSelectItems.add(item);
    }

    public void setArgs(Object[] args) {
        theArgs = args;
    }

    public Object[] getArgs() {
        return theArgs;
    }

    public int getNextArgIndex() {
        return theArgIndex++;
    }

    public Object getArgAt(int index) {
        return theArgs[index];
    }

    public void incrementJdbcParamCount() {
        theJDBCParamCount++;
    }

    public int getJdbcParamCount() {
        return theJDBCParamCount;
    }

    public boolean hasLimit() {
        return theLimit > 0;
    }

    public long getLimit() {
        return theLimit;
    }

    public void setLimit(long limit) {
        theLimit = limit;
    }

    void construct() throws ApolloExpressionException {
        theCQLBuilder.build();
    }

    public String getCQL() {
        return theCQLBuilder.getCQL();
    }

    public boolean isPureCQL() {
        // We are not using Cassnadra order by, so don't pass it
        if (hasOrderBy()) {
            return false;
        }
        return theCQLBuilder.isPureCQL();
    }

    public boolean hasOrderBy() {
        return !theOrderByCols.isEmpty();
    }

    public List<String> getOrderByCols() {
        return theOrderByCols;
    }

    public void addOrderByColumn(String name) {
        theOrderByCols.add(name);
    }

    public void addIndexable(String name) {
        theIndexable.add(name);
    }

    public Set<String> getIndexableCols() {
        return theIndexable;
    }

    public String getSQL() {
        return theSQL;
    }

    public ApolloExpression getRootExpression() {
        return theRootExpression;
    }

    void setAllowFiltering() throws ApolloExpressionException {
        isAllowFiltering = true;
    }

    public boolean isAllowFiltering() {
        return isAllowFiltering;
    }

    public ApolloConstruct setFetchSize(int size) {
        theFetchSize = size;
        return this;
    }

    public ApolloConstruct setFetchRefreshSize(int size) {
        theFetchRefresh = size;
        return this;
    }

    public int getFetchSize() {
        return theFetchSize;
    }

    public int getFetchRefreshSize() {
        return theFetchRefresh;
    }

    public boolean hasCQLArgs() {
        return theCQLBuilder.getCQLArgs().length > 0;
    }

    public BoundStatement getBoundStatement(Session session) {
        BoundStatement bound = ApolloStatementCache.getPreparedStatement(session, this).bind(theCQLBuilder.getCQLArgs());
        bound.setFetchSize(getFetchSize());
        if (hasConsistencyLevel()) {
            bound.setConsistencyLevel(getConsistencyLevel());
        }

        return bound;
    }

    void setResultingColName(Set<String> colNames) {
        resultingColNames = colNames;
    }

    public Set<String> getResultingColNames() {
        return resultingColNames;
    }

    /**
     * Set before execute
     */
    public void setRequireColNames() {
        requiresColNames = true;
    }

    public boolean requiresColNames() {
        return requiresColNames;
    }

    void setRunTime(long time) {
        theLastRunTime = time;
    }

    public long getLastRunTime() {
        return theLastRunTime;
    }

    public int getResultLimit() {
        return theResultLimit;
    }

    /**
     * Set before execute
     *
     * @param limit
     */
    public void setResultLimit(int limit) {
        theResultLimit = limit;
    }

    /**
     * Set before execute
     *
     * @param level
     */
    public void setConsistencyLevel(ConsistencyLevel level) {
        theConsistencyLevel = level;
    }

    public boolean hasConsistencyLevel() {
        return (theConsistencyLevel != null);
    }

    public ConsistencyLevel getConsistencyLevel() {
        return theConsistencyLevel;
    }

    public void setCountFunction() {
        isCount = true;
    }

    public boolean hasFunction() {
        return isCount;
    }

    public ApolloExpressionException getExpressionException() {
        return theExpressionException;
    }

    public void setExpressionException(ApolloExpressionException expressionException) {
        theExpressionException = expressionException;
    }

}
