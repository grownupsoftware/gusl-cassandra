package gusl.cassandra.apollo;

import com.datastax.driver.core.*;
import gusl.cassandra.apollo.sql.visitors.StatementVisitor;
import gusl.core.utils.StringUtils;
import lombok.CustomLog;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;

/**
 * Apollo Parser.
 * <p>
 * Parse the SQL, validate params and against Cassandra schema.
 * <p>
 * Build the Construct along the way.
 *
 * @author dhudson
 */
@CustomLog
public class ApolloParser {

    private final Session theSession;
    private final String theSQL;
    private final Object[] theArgs;

    ApolloParser(Session session, String sql, Object[] args) {
        theSession = session;
        theSQL = sql;
        theArgs = args;
    }

    ApolloConstruct parse() throws ApolloParseException {
        try {

            if (!StringUtils.startsWithIgnoreCase(theSQL, "select")) {
                throw new ApolloParseException(ApolloParseException.NOT_SELECT_STATEMENT);
            }

            String sql = theSQL.replaceAll("(?i)ALLOW FILTERING", "");

            Statement statement = CCJSqlParserUtil.parse(sql);
            ApolloConstruct construct = new ApolloConstruct(sql, theArgs);
            construct.setAllowFiltering();

            StatementVisitor visitor = new StatementVisitor(construct);
            visitor.parse(statement);

            if (construct.getExpressionException() != null) {
                throw construct.getExpressionException();
            }

            validateMetadata(construct);

            construct.construct();
            return construct;
        } catch (JSQLParserException ex) {
            String cause = ex.getMessage();
            if (cause == null) {
                cause = ex.getCause().getMessage();
            }

            String reason = ApolloParseException.UNABLE_TO_PARSE + " " + cause;
            logger.warn("Unable to parse {} ", cause, ex);
            throw new ApolloParseException(reason, ex);
        } catch (ApolloExpressionException ex) {
            throw new ApolloParseException(ex.getMessage());
        }
    }

    void validateParams() throws ApolloExpressionException {
        int params = theSQL.split("\\?", -1).length - 1;
        int supplied = (theArgs == null) ? 0 : theArgs.length;

        if (params > supplied) {
            throw new ApolloExpressionException(ApolloExpressionException.TOO_FEW_PARAMS);
        }

        if (params < supplied) {
            throw new ApolloExpressionException(ApolloExpressionException.TOO_MANY_PARAMS);
        }
        // all good
    }

    void validateParams(int supplied) throws ApolloExpressionException {
        int params = theSQL.split("\\?", -1).length - 1;
        if (params > supplied) {
            throw new ApolloExpressionException(ApolloExpressionException.TOO_FEW_PARAMS);
        }

        if (params < supplied) {
            throw new ApolloExpressionException(ApolloExpressionException.TOO_MANY_PARAMS);
        }
        // all good
    }

    private void validateMetadata(ApolloConstruct construct) throws ApolloParseException {
        Metadata cassandraMetadata = theSession.getCluster().getMetadata();

        String keyspace;
        if (StringUtils.isNotBlank(construct.getKeyspaceName())) {
            keyspace = construct.getKeyspaceName();
        } else {
            keyspace = theSession.getLoggedKeyspace();
        }

        KeyspaceMetadata keyspaceMD = cassandraMetadata.getKeyspace(keyspace);

        if (keyspaceMD == null) {
            throw new ApolloParseException(ApolloParseException.INVALID_KEYSPACE + " " + keyspace);
        }

        String table = construct.getTableName();

        TableMetadata tableMD = keyspaceMD.getTable(table);

        if (tableMD == null) {
            throw new ApolloParseException(ApolloParseException.INVALID_TABLE + " " + table);
        }

        for (String column : construct.columns()) {
            if (tableMD.getColumn(column) == null) {
                throw new ApolloParseException(ApolloParseException.INVALID_COLUMN + " " + column);
            }
        }

        construct.setTableMetadata(tableMD);

        // Lets build the indexable columns
        // Add index columns
        for (IndexMetadata index : tableMD.getIndexes()) {
            construct.addIndexable(index.getTarget());
        }
        // Add key columns
        for (ColumnMetadata colMD : tableMD.getPrimaryKey()) {
            construct.addIndexable(colMD.getName());
        }
    }

}
