package gusl.cassandra.apollo;

/**
 * Expression Exception.
 *
 * This is thrown during the execute cycle.s
 *
 * @author dhudson
 */
public class ApolloExpressionException extends ApolloException {

    private static final long serialVersionUID = 4918933690203169362L;

    static final String TOO_FEW_PARAMS = "Too few params supplied";
    static final String TOO_MANY_PARAMS = "Too many params supplied";

    public ApolloExpressionException(String reason) {
        super(reason);
    }

    public ApolloExpressionException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
