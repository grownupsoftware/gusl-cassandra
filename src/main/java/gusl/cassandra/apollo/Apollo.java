package gusl.cassandra.apollo;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import gusl.cassandra.apollo.expression.ApolloConditionalExpression;
import gusl.cassandra.apollo.expression.ApolloExpression;
import gusl.cassandra.apollo.expression.ApolloSimpleExpression;
import gusl.cassandra.builder.Column;
import gusl.cassandra.utils.CassandraUtils;
import gusl.cassandra.utils.RowMapper;
import gusl.core.annotations.FixMe;
import lombok.CustomLog;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Stream;

/**
 * Apollo is an SQL interface into Cassandra.
 * <p>
 * Spit in the eye of Cassandra! OK, so it wasn't the eye, but he was Cassandras
 * downfall.
 * <p>
 * It is <b>not</b> to be used for :-
 * <ul>
 * <li>Big Data of any kind, Its not partition aware so its going to grab all of
 * the data from the table (other than indexable columns)</li>
 * <li>Enhancing ones CV. This is no way as near as cool as something like
 * SparkSQL</li>
 * </ul>
 *
 * @author dhudson
 */
@CustomLog
public class Apollo {

    private Session theSession;
    private ApolloParser theParser;

    public Apollo() {
    }

    public Apollo(Session session) {
        theSession = session;
    }

    public ApolloConstruct parse(String sql, Object... args) throws ApolloParseException {
        if (theSession == null) {
            throw new ApolloParseException(ApolloParseException.NO_SESSION_SET);
        }

        if (theSession.isClosed()) {
            throw new ApolloParseException(ApolloParseException.SESSION_CLOSED);
        }

        theParser = new ApolloParser(theSession, sql.trim(), args);
        return theParser.parse();
    }

    public List<Row> execute(String sql, Object... args) throws ApolloParseException, ApolloExpressionException {
        return execute(parse(sql, args));
    }

    public CompletableFuture<List<Row>> execute(Executor executor, ApolloConstruct construct, Object... args) {
        final CompletableFuture<List<Row>> future = new CompletableFuture<>();

        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    future.complete(execute(construct, args));
                } catch (Throwable t) {
                    future.completeExceptionally(t);
                }
            }
        });

        return future;
    }

    /**
     * Helper method for settings args and running the context
     *
     * @param construct
     * @param args
     * @return
     * @throws ApolloExpressionException
     */
    public List<Row> execute(ApolloConstruct construct, Object... args) throws ApolloExpressionException {
        construct.setArgs(args);
        return execute(construct);
    }

    /**
     * Execute the construct returning a complete list of rows.
     *
     * @param construct
     * @return
     * @throws ApolloExpressionException
     */
    public List<Row> execute(ApolloConstruct construct) throws ApolloExpressionException {
        theParser.validateParams(construct.getArgs().length);
        resetExpressions(construct);
        try {
            ResultSet resultSet = theSession.execute(construct.getBoundStatement(theSession));
            return processResultSet(construct, resultSet);
        } catch (Throwable t) {
            throw new ApolloExpressionException(t.getMessage(), t.getCause());
        }
    }

    /**
     * Return rows from the SQL expression, which have been mapped in type T
     *
     * @param <T>
     * @param construct
     * @param mapper
     * @return
     * @throws ApolloExpressionException
     */
    public <T> List<T> execute(ApolloConstruct construct, RowMapper mapper, Class<T> dataClass, List<Column> theColumns) throws ApolloExpressionException {
        return CassandraUtils.mapRows(execute(construct), mapper, dataClass, theColumns);
    }

    @FixMe(createdBy = "dhudson", description = "Apollo was written before streams, would make sense to convert all to streams")
    public <T> Stream<T> stream(ApolloConstruct construct, RowMapper mapper, Class<T> dataClass, List<Column> columns) throws ApolloExpressionException {
        return execute(construct, mapper, dataClass, columns).stream();
    }

    private List<Row> processResultSet(ApolloConstruct construct, ResultSet resultSet) throws ApolloExpressionException {

        AbstractConstructProcessor processor;

        if (construct.hasFunction()) {
            processor = new CountConstructProcessor(construct);
        } else {
            processor = new SelectConstructProcessor(construct);
        }

        long startTime = System.currentTimeMillis();

        CassandraUtils.processAll(resultSet, processor, processor);

        if (processor.hasException()) {
            throw processor.getException();
        }

        List<Row> resultingRows = processor.getRows();

        if (!construct.hasFunction()) {
            logger.debug("Fetched {} rows, reduced to {} rows", ((SelectConstructProcessor) processor).getReturnedCount(), resultingRows.size());

            if (construct.hasOrderBy()) {
                logger.debug("Sorting ..");
                RowComparator parent = null;
                RowComparator root = null;
                for (String colName : construct.getOrderByCols()) {
                    RowComparator comp = new RowComparator(colName, construct.getTableMetadata().getColumn(colName).getType().getName());
                    if (parent == null) {
                        root = comp;
                    } else {
                        parent.setNextComparator(comp);
                    }
                    parent = comp;
                }

                Collections.sort(resultingRows, root);
            }
        }

        construct.setRunTime(System.currentTimeMillis() - startTime);

        logger.debug("All done");
        return resultingRows;
    }

    public void setSession(Session session) {
        theSession = session;
    }

    private void resetExpressions(ApolloConstruct construct) {
        if (construct.getRootExpression() != null) {
            walk(construct.getRootExpression());
        }
    }

    private void walk(ApolloExpression expression) {
        if (expression instanceof ApolloSimpleExpression) {
            ApolloSimpleExpression expr = (ApolloSimpleExpression) expression;
            expr.reset();
        } else {
            // Its a conditional expression
            ApolloConditionalExpression expr = (ApolloConditionalExpression) expression;
            expr.reset();
            walk(expr.getLeftSide());
            walk(expr.getRightSide());
        }
    }
}
