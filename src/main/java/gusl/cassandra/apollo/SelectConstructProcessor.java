package gusl.cassandra.apollo;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.Row;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Process the Result Set from Cassandra.
 *
 * @author dhudson
 */
public class SelectConstructProcessor extends AbstractConstructProcessor {

    private final List<Row> theResultingRows = new ArrayList<>();
    private int theReturnedCount = 0;

    public SelectConstructProcessor(ApolloConstruct construct) {
        super(construct);
    }

    @Override
    public boolean test(Row t) {
        theReturnedCount++;
        if (theReturnedCount == 1 && getConstruct().requiresColNames()) {
            // Lets grab some details of the result
            List<ColumnDefinitions.Definition> cols = t.getColumnDefinitions().asList();
            Set<String> resultingCols = new LinkedHashSet<>(cols.size());
            for (ColumnDefinitions.Definition def : cols) {
                resultingCols.add(def.getName());
            }
            getConstruct().setResultingColName(resultingCols);
        }

        return super.test(t);
    }

    @Override
    public boolean process(Row t) {
        // Stop if there was an exception
        if (hasException()) {
            return false;
        }

        theResultingRows.add(t);

        if (getConstruct().getResultLimit() != 0) {
            if (theResultingRows.size() >= getConstruct().getResultLimit()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public List<Row> getRows() {
        return theResultingRows;
    }

    public int getReturnedCount() {
        return theReturnedCount;
    }
}
