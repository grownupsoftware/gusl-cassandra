package gusl.cassandra.apollo.expression;

import com.datastax.driver.core.DataType;
import com.datastax.driver.core.Row;
import gusl.cassandra.apollo.ApolloExpressionException;
import gusl.cassandra.apollo.expression.side.ColumnSide;
import gusl.cassandra.apollo.expression.side.ParamSide;
import gusl.cassandra.apollo.expression.side.Side;
import lombok.CustomLog;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Simple left and right expression.
 *
 * @author dhudson
 */
@CustomLog
public abstract class ApolloSimpleExpression extends AbstractApolloExpression {

    private Side theLeft;
    private Side theRight;
    private DataType.Name theDataTypeName;
    private final String theKeyword;
    private boolean isNot = false;
    // The expression is redundant,as its taken care of by the CQL
    private boolean isDummyExpression;

    public ApolloSimpleExpression(String keyword) {
        theKeyword = keyword;
    }

    public void addSide(Side side) {
        if (theLeft == null) {
            theLeft = side;
        } else {
            theRight = side;
        }
    }

    public Side getLeftSide() {
        return theLeft;
    }

    public Side getRightSide() {
        return theRight;
    }

    public void setDataTypeName(DataType.Name name) {
        theDataTypeName = name;
    }

    public void setDummyExpression(boolean value) {
        isDummyExpression = value;
    }

    @Override
    public final boolean execute(Row row) throws ApolloExpressionException {
        if (isDummyExpression) {
            return true;
        }

        switch (theDataTypeName) {
            case DOUBLE:
                return checkIsNot(execute(getLeftSide().getValueAsDouble(row), getRightSide().getValueAsDouble(row)));
            case INT:
                return checkIsNot(execute(getLeftSide().getValueAsInt(row), getRightSide().getValueAsInt(row)));
            case BIGINT:
                return checkIsNot(execute(getLeftSide().getValueAsLong(row), getRightSide().getValueAsLong(row)));
            case DECIMAL:
                BigDecimal decimalLeft = getLeftSide().getValueAsDecimal(row);
                BigDecimal decimalRight = getRightSide().getValueAsDecimal(row);
                if (decimalLeft == null || decimalRight == null) {
                    return false;
                }
                return checkIsNot(execute(decimalLeft, decimalRight));
            case TIMESTAMP:
                Date timeLeft = getLeftSide().getValueAsTimestamp(row);
                Date timeRight = getRightSide().getValueAsTimestamp(row);
                if (timeLeft == null || timeRight == null) {
                    return false;
                }
                return checkIsNot(execute(timeLeft, timeRight));
            case DATE:
                Date dateLeft = getLeftSide().getValueAsDate(row);
                Date dateRight = getRightSide().getValueAsDate(row);
                if (dateLeft == null || dateRight == null) {
                    return false;
                }
                return checkIsNot(execute(dateLeft, dateRight));
            case TEXT:
            case ASCII:
            case VARCHAR:
                String leftString = getLeftSide().getValueAsString(row);
                String rightString = getRightSide().getValueAsString(row);
                if (leftString == null || rightString == null) {
                    return false;
                }
                return checkIsNot(execute(leftString, rightString));
            default:
                logger.warn("I don't know how to handle data type {} {}", theDataTypeName, getClass().getName());
                return false;
        }
    }

    private boolean checkIsNot(boolean result) {
        if (isNot) {
            return !result;
        }

        return result;
    }

    public List<ColumnSide> getColumnSides() {
        ArrayList<ColumnSide> sides = new ArrayList<>(1);
        if (!getLeftSide().isValue()) {
            sides.add((ColumnSide) getLeftSide());
        }

        if (!getRightSide().isValue()) {
            sides.add((ColumnSide) getRightSide());
        }

        return sides;
    }

    abstract boolean execute(double left, double right) throws ApolloExpressionException;

    abstract boolean execute(long left, long right) throws ApolloExpressionException;

    abstract boolean execute(BigDecimal left, BigDecimal right) throws ApolloExpressionException;

    abstract boolean execute(String left, String right) throws ApolloExpressionException;

    abstract boolean execute(Date left, Date right) throws ApolloExpressionException;

    abstract boolean execute(int left, int right) throws ApolloExpressionException;

    @Override
    public void addExpression(ApolloExpression expression) {
        throw new UnsupportedOperationException("Not supported by simple expressions.");
    }

    public String getSQL() {
        StringBuilder builder = new StringBuilder(50);
        builder.append(theLeft.getSQL());
        builder.append(" ");
        builder.append(theKeyword);
        builder.append(" ");
        builder.append(theRight.getSQL());
        return builder.toString();
    }

    public boolean isCQLExpression() {
        return true;
    }

    @Override
    public String toString() {
        return "SimpleApolloExpression{" + getSQL() + "}";
    }

    public void setIsNot() {
        isNot = true;
    }

    /**
     * If there is a JDBC param as part of this expression, and the column is
     * indexable then flag the param
     *
     * @return the arg index or -1, if no params
     */
    public int setParamIsIndexable() {
        if (getLeftSide() instanceof ParamSide) {
            return ((ParamSide) getLeftSide()).setIndexable();
        }

        if (getRightSide() instanceof ParamSide) {
            return ((ParamSide) getRightSide()).setIndexable();
        }

        return -1;
    }

}
