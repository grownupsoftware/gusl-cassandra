package gusl.cassandra.apollo.expression;

import gusl.cassandra.apollo.ApolloExpressionException;
import gusl.cassandra.apollo.expression.side.ValueSide;
import lombok.CustomLog;

import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Like Expression.
 *
 * <p>
 * Ask any teenager, its like, the expression, like</p>
 *
 * <p>
 * The right hand side of this expression must be a string</p>
 *
 * @author dhudson
 */
@CustomLog
public class ApolloLikeExpression extends ApolloSimpleExpression {

    private Pattern thePattern;

    public ApolloLikeExpression() {
        super("LIKE");
    }

    @Override
    public boolean isCQLExpression() {
        return false;
    }

    @Override
    boolean execute(double left, double right) throws ApolloExpressionException {
        return matches(Double.toString(left));
    }

    @Override
    boolean execute(long left, long right) throws ApolloExpressionException {
        return matches(Long.toString(left));
    }

    @Override
    boolean execute(BigDecimal left, BigDecimal right) throws ApolloExpressionException {
        return matches(left.toPlainString());
    }

    @Override
    boolean execute(String left, String right) throws ApolloExpressionException {
        return matches(left);
    }

    @Override
    boolean execute(Date left, Date right) throws ApolloExpressionException {
        return matches(left.toString());
    }

    @Override
    boolean execute(int left, int right) throws ApolloExpressionException {
        return matches(Double.toString(left));
    }

    private boolean matches(String left) {
        return getPattern().matcher(left).matches();
    }

    private Pattern getPattern() {
        if (thePattern == null) {
            String like = ((ValueSide) getRightSide()).getValueAsString();
            thePattern = generateRegex(like);
        }

        return thePattern;
    }

    @Override
    public void reset() {
        thePattern = null;
    }

    private Pattern generateRegex(String like) {
        String tmp = like.replace("\\", "\\\\")
                .replace(".", "\\.")
                .replace("{", "\\{")
                .replace("}", "\\}")
                .replace("[", "\\[")
                .replace("]", "\\]")
                .replace("+", "\\+")
                .replace("$", "\\$")
                .replace(" ", "\\s")
                .replace("#", "[0-9]")
                .replace("_", ".")
                .replace("*", "\\w*")
                .replace("%", ".*");

        logger.debug("Like {} to Regex ^{}$", like, tmp);

        return Pattern.compile("^" + tmp + "$", Pattern.CASE_INSENSITIVE);
    }

}
