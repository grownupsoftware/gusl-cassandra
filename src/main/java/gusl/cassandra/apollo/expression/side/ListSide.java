package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import gusl.cassandra.apollo.ApolloConstruct;
import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * List Values.
 *
 * We can have in ('a','b','c') or .. in (?) where ? is a Collection
 *
 * @author dhudson
 */
public class ListSide extends ValueSide {

    // So its either a list or a JDBC 
    private final Set<Object> theHashSet;
    private ParamSide theParamSide;

    private final ApolloConstruct theConstruct;

    public ListSide(ApolloConstruct construct) {
        theConstruct = construct;
        theHashSet = new HashSet<>(3);
    }

    @Override
    public double getValueAsDouble() throws ApolloExpressionException {
        return 0D;
    }

    @Override
    public long getValueAsLong() throws ApolloExpressionException {
        return 1L;
    }

    @Override
    public String getValueAsString() {
        return "";
    }

    @Override
    public BigDecimal getValueAsDecimal() throws ApolloExpressionException {
        return BigDecimal.ZERO;
    }

    @Override
    public Date getValueAsTimestamp() throws ApolloExpressionException {
        return new Date();
    }

    @Override
    public Date getValueAsDate() throws ApolloExpressionException {
        return new Date();
    }

    public void addConstant(Object value) {
        if (!isJDBCParam()) {
            theHashSet.add(value);
        } else {
            setException();
        }
    }

    private void setException() {
        theConstruct.setExpressionException(new ApolloExpressionException("In can either be a list of constants or a single JDBC param"));
    }

    public ParamSide getParamSide() {
        return theParamSide;
    }

    public boolean isJDBCParam() {
        return (theParamSide != null);
    }

    public boolean contains(Object value) {
        if (isJDBCParam()) {
            Collection collection = (Collection) theParamSide.getValue();
            return collection.contains(value);
        } else {
            return theHashSet.contains(value);
        }
    }

    @Override
    public String getSQL() {
        StringBuilder builder = new StringBuilder(30);
        if (isJDBCParam()) {
            builder.append(" ? ");
        } else {
            builder.append("(");
            int count = 0;
            for (Object value : theHashSet) {
                count++;
                if (value instanceof String) {
                    builder.append("'");
                    builder.append(value);
                    builder.append("'");
                } else {
                    builder.append(value);
                }

                if (count < theHashSet.size()) {
                    builder.append(", ");
                }

            }
            builder.append(")");
        }

        return builder.toString();
    }

    @Override
    public Object getValue() {
        return null;
    }

    public void addJDBCParam() {
        if (theParamSide == null) {
            if (theHashSet.isEmpty()) {
                theParamSide = new ParamSide(theConstruct);
            } else {
                setException();
            }
        } else {
            setException();
        }
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        return 0;
    }
}
