package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class LongSide extends ValueSide {

    private final long theValue;

    public LongSide(long value) {
        theValue = value;
    }

    @Override
    public double getValueAsDouble() {
        return (double) theValue;
    }

    @Override
    public long getValueAsLong() {
        return theValue;
    }

    @Override
    public String getValueAsString() {
        return Long.toString(theValue);
    }

    @Override
    public BigDecimal getValueAsDecimal() {
        return new BigDecimal(theValue);
    }

    @Override
    public Date getValueAsTimestamp() {
        return new Date(theValue);
    }

    @Override
    public Date getValueAsDate() {
        return new Date(theValue);
    }

    @Override
    public Object getValue() {
        return theValue;
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        return (int) theValue;
    }
}
