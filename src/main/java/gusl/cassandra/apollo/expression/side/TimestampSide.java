package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class TimestampSide extends ValueSide {

    private final Timestamp theValue;

    public TimestampSide(Timestamp timestamp) {
        theValue = timestamp;
    }

    @Override
    public double getValueAsDouble() throws ApolloExpressionException {
        return (double) theValue.getTime();
    }

    @Override
    public long getValueAsLong() throws ApolloExpressionException {
        return theValue.getTime();
    }

    @Override
    public String getValueAsString() {
        return theValue.toString();
    }

    @Override
    public BigDecimal getValueAsDecimal() throws ApolloExpressionException {
        return new BigDecimal(theValue.getTime());
    }

    @Override
    public Date getValueAsTimestamp() throws ApolloExpressionException {
        return new Date(theValue.getTime());
    }

    @Override
    public Date getValueAsDate() throws ApolloExpressionException {
        return getValueAsTimestamp();
    }

    @Override
    public Object getValue() {
        return theValue;
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        throw new ApolloExpressionException("Can't convert Timestamp to int");
    }

}
