package gusl.cassandra.apollo.expression.side;

import com.datastax.driver.core.Row;
import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public abstract class ValueSide extends Side {

    @Override
    public boolean isValue() {
        return true;
    }

    @Override
    public double getValueAsDouble(Row row) throws ApolloExpressionException {
        return getValueAsDouble();
    }

    @Override
    public long getValueAsLong(Row row) throws ApolloExpressionException {
        return getValueAsLong();
    }

    @Override
    public String getValueAsString(Row row) {
        return getValueAsString();
    }

    @Override
    public Date getValueAsTimestamp(Row row) throws ApolloExpressionException {
        return getValueAsTimestamp();
    }

    @Override
    public Date getValueAsDate(Row row) throws ApolloExpressionException {
        return getValueAsDate();
    }

    @Override
    public BigDecimal getValueAsDecimal(Row row) throws ApolloExpressionException {
        return getValueAsDecimal();
    }

    @Override
    public int getValueAsInt(Row row) throws ApolloExpressionException {
        return getValueAsInt();
    }

    @Override
    public String getSQL() {
        return getValueAsString();
    }

    public abstract double getValueAsDouble() throws ApolloExpressionException;

    public abstract long getValueAsLong() throws ApolloExpressionException;

    public abstract String getValueAsString();

    public abstract BigDecimal getValueAsDecimal() throws ApolloExpressionException;

    public abstract Date getValueAsTimestamp() throws ApolloExpressionException;

    public abstract Date getValueAsDate() throws ApolloExpressionException;

    public abstract int getValueAsInt() throws ApolloExpressionException;

    public abstract Object getValue();
}
