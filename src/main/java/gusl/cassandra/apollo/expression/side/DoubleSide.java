package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class DoubleSide extends ValueSide {

    private final double theValue;

    public DoubleSide(double value) {
        theValue = value;
    }

    @Override
    public double getValueAsDouble() {
        return theValue;
    }

    @Override
    public long getValueAsLong() {
        return (long) theValue;
    }

    @Override
    public String getValueAsString() {
        return Double.toString(theValue);
    }

    @Override
    public BigDecimal getValueAsDecimal() {
        return new BigDecimal(Double.toString(theValue));
    }

    @Override
    public Date getValueAsTimestamp() {
        return new Date((long) theValue);
    }

    @Override
    public Date getValueAsDate() {
        return new Date((long) theValue);
    }

    @Override
    public Object getValue() {
        return theValue;
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        return (int) theValue;
    }
}
