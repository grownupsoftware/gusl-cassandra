package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class StringSide extends ValueSide {

    private final String theValue;

    public StringSide(String value) {
        theValue = value;
    }

    @Override
    public double getValueAsDouble() {
        return Double.parseDouble(theValue);
    }

    @Override
    public long getValueAsLong() {
        return Long.parseLong(theValue);
    }

    @Override
    public String getValueAsString() {
        return theValue;
    }

    @Override
    public BigDecimal getValueAsDecimal() {
        return new BigDecimal(theValue);
    }

    @Override
    public Date getValueAsTimestamp() throws ApolloExpressionException {
        return parseDate(theValue);
    }

    @Override
    public Date getValueAsDate() throws ApolloExpressionException {
        return parseDate(theValue);
    }

    @Override
    public Object getValue() {
        return theValue;
    }

    @Override
    public String getSQL() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("'");
        builder.append(theValue);
        builder.append("'");
        return builder.toString();
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        return Integer.parseInt(theValue);
    }

}
