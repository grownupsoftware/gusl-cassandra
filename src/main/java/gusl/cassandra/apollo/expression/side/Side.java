package gusl.cassandra.apollo.expression.side;

import com.datastax.driver.core.Row;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * Either left or right.
 *
 * Can be either a column or value or a JDBC parameter
 *
 * @author dhudson
 */
public abstract class Side {

    private static final DateFormat[] DATE_FORMATS = {
        new SimpleDateFormat("yyyy-mm-dd"),
        new SimpleDateFormat("yyyy-mm-dd HH:mm"),
        new SimpleDateFormat("yyyy-mm-dd HH:mm:ss"),
        new SimpleDateFormat("yyyy-mm-dd HH:mmZ"),
        new SimpleDateFormat("yyyy-mm-dd HH:mm:ssZ"),
        new SimpleDateFormat("yyyy-mm-dd'T'HH:mm"),
        new SimpleDateFormat("yyyy-mm-dd'T'HH:mmZ"),
        new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ss"),
        new SimpleDateFormat("yyyy-mm-dd'T'HH:mm:ssZ"),
        new SimpleDateFormat("yyyy-mm-ddZ")
    };

    static {
        for (DateFormat format : DATE_FORMATS) {
            format.setLenient(false);
        }
    }

    public Side() {
    }

    public abstract boolean isValue();

    public abstract String getSQL();

    public abstract double getValueAsDouble(Row row) throws ApolloExpressionException;

    public abstract long getValueAsLong(Row row) throws ApolloExpressionException;

    public abstract String getValueAsString(Row row);

    public abstract Date getValueAsTimestamp(Row row) throws ApolloExpressionException;

    public abstract Date getValueAsDate(Row row) throws ApolloExpressionException;

    public abstract BigDecimal getValueAsDecimal(Row row) throws ApolloExpressionException;

    public abstract int getValueAsInt(Row row) throws ApolloExpressionException;
    
    public static Date parseDate(String s) throws ApolloExpressionException {
        for (DateFormat currentFormat : DATE_FORMATS) {
            try {
                return currentFormat.parse(s);
            } catch (ParseException ignore) {
                // continue
            }
        }

        throw new ApolloExpressionException("Don't know how to handle date " + s);
    }
}
