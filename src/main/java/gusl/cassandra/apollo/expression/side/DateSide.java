package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class DateSide extends ValueSide {

    private final Date theValue;

    public DateSide(Date date) {
        theValue = date;
    }

    @Override
    public double getValueAsDouble() throws ApolloExpressionException {
        return (double) theValue.getTime();
    }

    @Override
    public long getValueAsLong() throws ApolloExpressionException {
        return theValue.getTime();
    }

    @Override
    public String getValueAsString() {
        return theValue.toString();
    }

    @Override
    public BigDecimal getValueAsDecimal() throws ApolloExpressionException {
        return new BigDecimal(theValue.getTime());
    }

    @Override
    public Date getValueAsTimestamp() throws ApolloExpressionException {
        return theValue;
    }

    @Override
    public Date getValueAsDate() throws ApolloExpressionException {
        return theValue;
    }

    @Override
    public Object getValue() {
        return theValue;
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        throw new ApolloExpressionException("Can't convert date to int");
    }
}
