package gusl.cassandra.apollo.expression.side;

import com.datastax.driver.core.Row;
import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * Represents a column in a table.
 *
 * @author dhudson
 */
public class ColumnSide extends Side {

    private final String theColumnName;

    public ColumnSide(String columnName) {
        theColumnName = columnName;
    }

    @Override
    public double getValueAsDouble(Row row) {
        return row.getDouble(theColumnName);
    }

    @Override
    public long getValueAsLong(Row row) {
        return row.getLong(theColumnName);
    }

    @Override
    public String getValueAsString(Row row) {
        return row.getString(theColumnName);
    }

    @Override
    public Date getValueAsTimestamp(Row row) {
        return row.getTimestamp(theColumnName);
    }

    @Override
    public Date getValueAsDate(Row row) {
        return new Date(row.getDate(theColumnName).getMillisSinceEpoch());
    }

    @Override
    public BigDecimal getValueAsDecimal(Row row) {
        return row.getDecimal(theColumnName);
    }

    @Override
    public int getValueAsInt(Row row) throws ApolloExpressionException {
        return row.getInt(theColumnName);
    }

    public String getColumnName() {
        return theColumnName;
    }

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public String getSQL() {
        return theColumnName;
    }

}
