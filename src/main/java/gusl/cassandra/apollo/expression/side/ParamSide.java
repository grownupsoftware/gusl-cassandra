package gusl.cassandra.apollo.expression.side;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloConstruct;
import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * JDBC Parameter.
 *
 * @author dhudson
 */
public class ParamSide extends ValueSide {

    private final ApolloConstruct theConstruct;
    private final int theParamIndex;
    private boolean isIndexable;

    public ParamSide(ApolloConstruct construct) {
        theConstruct = construct;
        theParamIndex = construct.getNextArgIndex();
    }

    @Override
    public double getValueAsDouble() throws ApolloExpressionException {
        Object value = theConstruct.getArgAt(theParamIndex);
        if (value instanceof Integer) {
            return (double) (int) value;
        }
        return (double) value;
    }

    @Override
    public long getValueAsLong() throws ApolloExpressionException {
        return (long) theConstruct.getArgAt(theParamIndex);
    }

    @Override
    public String getValueAsString() {
        Object value = theConstruct.getArgAt(theParamIndex);
        return value.toString();
    }

    @Override
    public BigDecimal getValueAsDecimal() throws ApolloExpressionException {
        Object value = theConstruct.getArgAt(theParamIndex);
        if (value instanceof Integer) {
            return new BigDecimal((int) value);
        }

        if (value instanceof Long) {
            return new BigDecimal((long) value);
        }

        if (value instanceof Double) {
            return new BigDecimal(Double.toString(((Double) value)));
        }

        return (BigDecimal) theConstruct.getArgAt(theParamIndex);
    }

    @Override
    public Date getValueAsTimestamp() throws ApolloExpressionException {
        return (Date) theConstruct.getArgAt(theParamIndex);
    }

    @Override
    public Date getValueAsDate() throws ApolloExpressionException {
        return (Date) theConstruct.getArgAt(theParamIndex);
    }

    @Override
    public String getSQL() {
        if (isIndexable) {
            return "?";
        }
        return getValueAsString();
    }

    public int setIndexable() {
        isIndexable = true;
        return theParamIndex;
    }

    @Override
    public Object getValue() {
        return theConstruct.getArgAt(theParamIndex);
    }

    @Override
    public int getValueAsInt() throws ApolloExpressionException {
        return (int) theConstruct.getArgAt(theParamIndex);
    }
}
