package gusl.cassandra.apollo.expression;

/**
 * Base Apollo Expression.
 *
 * @author dhudson
 */
public abstract class AbstractApolloExpression implements ApolloExpression {

    private ApolloExpression theParent;

    public AbstractApolloExpression() {
    }

    @Override
    public void setParent(ApolloExpression expression) {
        theParent = expression;
    }

    @Override
    public ApolloExpression getParent() {
        return theParent;
    }

    @Override
    public void reset() {
    }

}
