package gusl.cassandra.apollo.expression;

import java.math.BigDecimal;
import java.util.Date;
import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * Simple Equals Expression.
 *
 * @author dhudson
 */
public class ApolloEqualsExpression extends ApolloSimpleExpression {

    public ApolloEqualsExpression() {
        super("=");
    }

    @Override
    boolean execute(double left, double right) throws ApolloExpressionException {
        return left == right;
    }

    @Override
    boolean execute(long left, long right) throws ApolloExpressionException {
        return left == right;
    }

    @Override
    boolean execute(BigDecimal left, BigDecimal right) throws ApolloExpressionException {
        return (left.compareTo(right) == 0);
    }

    @Override
    boolean execute(String left, String right) throws ApolloExpressionException {
        return left.equals(right);
    }

    @Override
    boolean execute(Date left, Date right) throws ApolloExpressionException {
        return left.equals(right);
    }

    @Override
    boolean execute(int left, int right) throws ApolloExpressionException {
        return left == right;
    }

}
