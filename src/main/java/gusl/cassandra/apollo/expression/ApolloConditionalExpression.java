package gusl.cassandra.apollo.expression;

/**
 *
 * @author dhudson
 */
public abstract class ApolloConditionalExpression extends AbstractApolloExpression {

    private ApolloExpression theLeftSide;
    private ApolloExpression theRightSide;

    public ApolloConditionalExpression() {
    }

    @Override
    public void addExpression(ApolloExpression expression) {
        expression.setParent(this);
        if (theLeftSide == null) {
            theLeftSide = expression;
        } else {
            theRightSide = expression;
        }
    }

    public ApolloExpression getLeftSide() {
        return theLeftSide;
    }

    public void setLeftSide(ApolloExpression leftSide) {
        theLeftSide = leftSide;
    }

    public ApolloExpression getRightSide() {
        return theRightSide;
    }

    public void setRightSide(ApolloExpression rightSide) {
        theRightSide = rightSide;
    }

    @Override
    public String toString() {
        return "BinaryExpression{" + theLeftSide + getConditionalKeyword() + theRightSide + '}';
    }

    public abstract String getConditionalKeyword();

}
