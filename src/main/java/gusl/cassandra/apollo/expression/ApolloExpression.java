package gusl.cassandra.apollo.expression;

import com.datastax.driver.core.Row;
import java.util.function.Predicate;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * Apollo Expression.
 *
 *
 * @author dhudson
 */
public interface ApolloExpression extends Predicate<Row> {

    // Basic tree type interface
    public void setParent(ApolloExpression expression);

    public ApolloExpression getParent();

    public void addExpression(ApolloExpression expression);

    @Override
    public default boolean test(Row t) {
        try {
            return execute(t);
        } catch (ApolloExpressionException ex) {
            return false;
        }
    }

    // Process the bad boy
    public boolean execute(Row row) throws ApolloExpressionException;

    /**
     * Reset an expression between execution cycles if required.
     */
    public void reset();
}
