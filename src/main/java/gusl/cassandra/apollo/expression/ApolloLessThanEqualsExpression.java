package gusl.cassandra.apollo.expression;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;

/**
 * Less than Expression.
 *
 * @author dhudson
 */
public class ApolloLessThanEqualsExpression extends ApolloSimpleExpression {

    public ApolloLessThanEqualsExpression() {
        super("<=");
    }

    @Override
    boolean execute(double left, double right) throws ApolloExpressionException {
        return left <= right;
    }

    @Override
    boolean execute(long left, long right) throws ApolloExpressionException {
        return left <= right;
    }

    @Override
    boolean execute(BigDecimal left, BigDecimal right) throws ApolloExpressionException {
        return left.compareTo(right) <= 0;
    }

    @Override
    boolean execute(String left, String right) throws ApolloExpressionException {
        return left.compareTo(right) <= 0;
    }

    @Override
    boolean execute(Date left, Date right) throws ApolloExpressionException {
        return left.compareTo(right) <= 0;
    }

    @Override
    boolean execute(int left, int right) throws ApolloExpressionException {
        return left <= right;
    }
}
