package gusl.cassandra.apollo.expression;

import com.datastax.driver.core.Row;
import gusl.cassandra.apollo.ApolloExpressionException;

/**
 *
 * @author dhudson
 */
public class ApolloAndExpression extends ApolloConditionalExpression {

    @Override
    public boolean execute(Row row) throws ApolloExpressionException {
        return getLeftSide().execute(row) && getRightSide().execute(row);
    }

    @Override
    public String getConditionalKeyword() {
        return "AND";
    }
}
