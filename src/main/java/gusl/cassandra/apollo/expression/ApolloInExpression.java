package gusl.cassandra.apollo.expression;

import java.math.BigDecimal;
import java.util.Date;

import gusl.cassandra.apollo.ApolloExpressionException;
import gusl.cassandra.apollo.expression.side.ListSide;

/**
 *
 * @author dhudson
 */
public class ApolloInExpression extends ApolloSimpleExpression {

    public ApolloInExpression() {
        super("IN");
    }

    @Override
    boolean execute(double left, double right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    boolean execute(long left, long right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    boolean execute(BigDecimal left, BigDecimal right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    boolean execute(String left, String right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    boolean execute(Date left, Date right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    boolean execute(int left, int right) throws ApolloExpressionException {
        ListSide listSide = (ListSide) getRightSide();
        return listSide.contains(left);
    }

    @Override
    public boolean isCQLExpression() {
        // It would be good to make this work with CQL, but there are several issues with it.
        return false;
    }

    @Override
    public int setParamIsIndexable() {
        ListSide listSide = (ListSide) getRightSide();
        if (listSide.isJDBCParam()) {
            return listSide.getParamSide().setIndexable();
        }

        return -1;
    }

}
