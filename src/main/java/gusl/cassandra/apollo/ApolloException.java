package gusl.cassandra.apollo;

import gusl.core.exceptions.GUSLException;

/**
 * Super class of Apollo Exceptions.
 *
 * @author dhudson
 */
public class ApolloException extends GUSLException {

    private static final long serialVersionUID = 3262373791703187246L;

    public ApolloException(String reason) {
        super(reason);
    }

    public ApolloException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
