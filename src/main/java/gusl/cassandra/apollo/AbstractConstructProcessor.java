package gusl.cassandra.apollo;

import com.datastax.driver.core.Row;
import java.util.List;
import java.util.function.Predicate;
import gusl.cassandra.apollo.expression.ApolloExpression;
import gusl.cassandra.utils.RowProcessor;

/**
 * Base Apollo Processor.
 *
 * @author dhudson
 */
public abstract class AbstractConstructProcessor implements Predicate<Row>, RowProcessor<Row> {

    private final ApolloConstruct theConstruct;
    private final ApolloExpression theExpression;
    private ApolloExpressionException theException;

    public AbstractConstructProcessor(ApolloConstruct construct) {
        theConstruct = construct;
        theExpression = construct.getRootExpression();
    }

    /**
     * Test the expression, if there is one.
     *
     * @param t
     * @return
     */
    @Override
    public boolean test(Row t) {
        if (theExpression == null) {
            return true;
        }

        try {
            return theExpression.execute(t);
        } catch (ApolloExpressionException ex) {
            theException = ex;
            return false;
        }
    }

    public boolean hasException() {
        return (theException != null);
    }

    public ApolloExpressionException getException() {
        return theException;
    }

    public ApolloConstruct getConstruct() {
        return theConstruct;
    }

    public abstract List<Row> getRows();
}
