package gusl.cassandra.repository.dao;

import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.WriteTimeoutException;
import com.google.common.base.CaseFormat;
import com.google.common.collect.Lists;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PageResponse;
import gusl.cassandra.builder.*;
import gusl.cassandra.errors.CassandraErrors;
import gusl.cassandra.utils.AbstractBaseDAOCassImpl;
import gusl.core.annotations.FixMe;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.core.utils.ClassUtils;
import gusl.core.utils.Utils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static gusl.core.utils.LambdaExceptionHelper.rethrowConsumer;

/**
 * @param <T> the DO class
 * @author grant
 */
public abstract class AbstractDAOCassImpl<T> extends AbstractBaseDAOCassImpl implements CassandraDAO {

    protected GUSLLogger logger = GUSLLogManager.getLogger(getClass());

    private static final int BATCH_SIZE = 10;
    // Used to set the date to null
    public static final Date UNKNOWN_DATE = new Date(1);

    protected LmSelectExecutor<T> idSelector;
    protected LmSelectExecutor<T> allSelector;
    protected LmInsertExecutor<T> recordInserter;
    protected LmUpdateExecutor<T> recordUpdater;
    protected LmDeleteExecutor<T> recordDeleter;

    private final Table theTable;
    private final Column theKeyColumn;
    private final Class<T> theDOClass;

    private volatile boolean isInitialied;

    private final MergeHelper<T> theMergeHelper;

    private final Map<Class<?>, LmSelectExecutor> theViewMap = new HashMap<>();

    public AbstractDAOCassImpl(Table table, Column keyColumn, Class<T> doClass) {
        this.theTable = table;
        this.theKeyColumn = keyColumn;
        this.theDOClass = doClass;
        theMergeHelper = new MergeHelper<>();
    }

    @Override
    public void prepare() throws Exception {
    }

    @Override
    public synchronized void initialise() throws GUSLException {
        if (isInitialied) {
            return;
        }

        try {
            idSelector = this.<T>select()
                    .from(theTable)
                    .into(theDOClass)
                    .withMetric(theTable.getTableName() + "-id")
                    .withPrimaryKeyClauses()
                    .prepare();

            allSelector = this.<T>select()
                    .from(theTable)
                    .into(theDOClass)
                    .withMetric(theTable.getTableName() + "-all")
                    .prepare();

            recordInserter = this.<T>insertInto(theTable)
                    .from(theDOClass)
                    .withMetric(theTable.getTableName())
                    .prepare();

            recordUpdater = this.<T>update(theTable)
                    .updateRecord(theDOClass)
                    .withMetric(theTable.getTableName())
                    .withPrimaryKeyClauses()
                    .prepare();

            recordDeleter = this.<T>delete(theTable)
                    .withPrimaryKeyClauses()
                    .withMetric(theTable.getTableName())
                    .prepare();

            theMergeHelper.init(theTable);
            isInitialied = true;
        } catch (Exception ex) {
            logger.error("Failed to create prepared statements", ex);
            throw ex;
        }
    }

    protected void checkInit() {
        if (!isInitialied) {
            try {
                initialise();
            } catch (Exception ex) {
                // There is nothing that we can do ..
                throw new RuntimeException(ex);
            }
        }
    }

    public void insert(T entity) throws GUSLErrorException {
        try {
            checkInit();
            recordInserter.insert().values(entity).execute();
        } catch (GUSLException ex) {
            throw new GUSLErrorException(CassandraErrors.INSERT_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public Long count() throws GUSLErrorException {
        try {
            Session session = getCassandraContext().getSession();// (2)
            ResultSet rs = session.execute("select count(*) from " + theTable.getKeyspace() + "." + theTable.getTableName());
            Row row = rs.one();
            return row.getLong(0);
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.INSERT_DAO_ERROR.getError(theTable.getTableName(), ex.getMessage()), ex);
        }
    }

    public void insertBatch(List<T> list) throws GUSLErrorException {
        checkInit();
        // Should this use Batch?
        Utils.safeStream(list).forEach(rethrowConsumer(entity -> {
            try {
                recordInserter.insert().values(entity).execute();
            } catch (GUSLException ex) {
                throw new GUSLErrorException(CassandraErrors.INSERT_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
            }

        }));
    }

    public void updateBatch(List<T> list) throws GUSLErrorException {
        checkInit();
        // Should this use Batch?
        Utils.safeStream(list).forEach(rethrowConsumer(entity -> {
            try {
                recordUpdater.update().record(entity).execute();
            } catch (GUSLException ex) {
                throw new GUSLErrorException(CassandraErrors.INSERT_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
            }

        }));
    }

    public BoundStatement createInsertBoundStatement(T entity) throws GUSLErrorException {
        checkInit();
        try {
            return recordInserter.insert().values(entity).createBoundStatement();
        } catch (GUSLException ex) {
            throw new GUSLErrorException(CassandraErrors.INSERT_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    /**
     * Note: You do not really want to use this! Rather create a bespoke
     * LmUpdateExecutor with specific fields and update them, rather than doing
     * a compare
     */
    public BoundStatement createUpdateBoundStatement(T entity) throws GUSLErrorException {
        checkInit();
        Optional<T> optional = findRecord(entity);
        if (!optional.isPresent()) {
            throw new GUSLErrorException(CassandraErrors.ID_NOT_FOUND.getError(new String[]{theTable.getTableName(), String.valueOf(entity)}));
        }

        T currentEntity = optional.get();

        boolean requiresUpdate = mergeUpdateFields(currentEntity, entity);

        try {
            return recordUpdater.update()
                    .record(currentEntity)
                    .createUpdateBoundStatement();

        } catch (Exception ex) {
            logger.warn("Table: {} - failed to create bound update record {} ", theTable.getTableName(), entity, ex);
            throw new GUSLErrorException(CassandraErrors.UPDATE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public T findById(String id) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(id).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(id)}), ex);
        }
    }

    public T findById(Long id) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(id).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(id)}), ex);
        }
    }

    public CompletableFuture<Optional<T>> findByIdAsAsync(Long id) {
        checkInit();
        try {
            return idSelector.select().where(id).asSingleton().executeAsAsync();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find record for id: {} ", theTable.getTableName(), id, ex);
            CompletableFuture<Optional<T>> future = new CompletableFuture<>();
            future.completeExceptionally(new Exception("Failure in executing query", ex));
            return future;
        }
    }

    public T findById(Long partitionId, Long id) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_2_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(String partitionId, String id) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_2_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(Long partitionId, String id) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors
                    .ID_2_NOT_FOUND.getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(Long partitionId, String id, String recId) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).and(recId).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_3_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(String partitionId, String id, Long recId) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).and(recId).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_3_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(String partitionId, String id, Integer recId) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).and(recId).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_3_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public T findById(String partitionId, String id, String recId) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).and(recId).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_3_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId), String.valueOf(id)}), ex);
        }
    }

    public CompletableFuture<Optional<T>> findByIdAsAsync(Long partitionId, Long id) {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(id).asSingleton().executeAsAsync();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find record for partitionId: {} id: {} ", theTable.getTableName(), partitionId, id, ex);
            CompletableFuture<Optional<T>> future = new CompletableFuture<>();
            future.completeExceptionally(new Exception("Failure in executing query", ex));
            return future;
        }
    }

    public T findById(Long partitionId, Long secondaryId, Long recordId) throws GUSLErrorException {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(secondaryId).and(recordId).asSingleton().execute().get();
        } catch (Exception ex) {
            throw new GUSLErrorException(CassandraErrors.ID_3_NOT_FOUND
                    .getError(true, new String[]{theTable.getTableName(), String.valueOf(partitionId),
                            String.valueOf(secondaryId),
                            String.valueOf(recordId)}), ex);
        }
    }

    public CompletableFuture<Optional<T>> findByIdAsAsync(Long partitionId, Long secondaryId, Long recordId) {
        checkInit();
        try {
            return idSelector.select().where(partitionId).and(secondaryId).and(recordId).asSingleton().executeAsAsync();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find record for id: {} {} {} ", theTable.getTableName(), partitionId, secondaryId, recordId, ex);
            CompletableFuture<Optional<T>> future = new CompletableFuture<>();
            future.completeExceptionally(new Exception("Failure in executing query", ex));
            return future;
        }
    }

    public Optional<T> findRecord(T entity) {
        checkInit();
        try {
            return idSelector.select().using(entity).asSingleton().execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find record for using: {} ", theTable.getTableName(), entity, ex);
            return Optional.empty();
        }
    }

    public List<T> getAll() throws GUSLErrorException {
        checkInit();
        try {
            return allSelector.select().asList().execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find all records", theTable.getTableName(), ex);
            throw new GUSLErrorException(CassandraErrors.DAO_GET_ALL_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public PageResponse<T> getAllPaged(PageRequest request) throws GUSLErrorException {
        checkInit();
        try {
            return allSelector.select().asList().executePaged(request);
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find all records", theTable.getTableName(), ex);
            throw new GUSLErrorException(CassandraErrors.DAO_GET_ALL_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public CompletableFuture<List<T>> getAllAsync() {
        checkInit();
        try {
            return allSelector.select().asList().executeAsAsync();
        } catch (GUSLException ex) {
            logger.warn("Table: {} - failed to find all records (async) ", theTable.getTableName(), ex);
            CompletableFuture<List<T>> future = new CompletableFuture<>();
            future.completeExceptionally(new Exception("Failure in executing query", ex));
            return future;
        }
    }

    public Stream<T> stream() throws GUSLErrorException {
        checkInit();
        try {
            return allSelector.select().asList().stream();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to find all records (stream) ", theTable.getTableName(), ex);
            throw new GUSLErrorException(CassandraErrors.DAO_GET_ALL_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public boolean mergeUpdateFields(T currentEntity, T entity) {
        return theMergeHelper.mergeUpdateFields(currentEntity, entity);
    }

    public T update(T entity) throws GUSLErrorException {
        checkInit();
        Optional<T> optional = findRecord(entity);
        if (!optional.isPresent()) {
            throw new GUSLErrorException(CassandraErrors.ID_NOT_FOUND.getError(new String[]{theTable.getTableName(), String.valueOf(entity)}));
        }

        T currentEntity = optional.get();

        boolean requiresUpdate = mergeUpdateFields(currentEntity, entity);

        if (!requiresUpdate) {
            return currentEntity;
        }

        try {
            recordUpdater.update()
                    .record(currentEntity)
                    .execute();

            return currentEntity;
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to update record {} ", theTable.getTableName(), currentEntity, ex);
            throw new GUSLErrorException(CassandraErrors.UPDATE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }

    }

    public void delete(Long id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement createDeleteBoundStatement(Long id) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(id).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(String id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(Long partitionId, Long id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(String partitionId, String id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(Long partitionId, String id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(Date partitionId, Long id) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(id).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(String partitionId, String id, Long recId) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(id).and(recId).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(Long partitionId, Long secondaryId, Long recordId) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(secondaryId).and(recordId).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), recordId, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public void delete(String partitionId, String secondaryId, String recordId) throws GUSLErrorException {
        try {
            checkInit();
            recordDeleter.delete().where(partitionId).and(secondaryId).and(recordId).execute();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), recordId, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(Long id) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(id).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(String id) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(id).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(Long partitionId, Long id) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(partitionId).and(id).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(String partitionId, String id) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(partitionId).and(id).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(String partitionId, String id, Long recId) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(partitionId).and(id).and(recId).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), id, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public BoundStatement deleteAsBoundStatement(Long partitionId, Long secondaryId, Long recordId) throws GUSLErrorException {
        try {
            checkInit();
            return recordDeleter.delete().where(partitionId).and(secondaryId).and(recordId).asBoundStatement();
        } catch (Exception ex) {
            logger.warn("Table: {} - failed to delete record with id: {} ", theTable.getTableName(), recordId, ex);
            throw new GUSLErrorException(CassandraErrors.DELETE_DAO_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }
    }

    public Table getTable() {
        return theTable;
    }

    public boolean execute(List<BoundStatement> statements) throws GUSLErrorException {
        List<List<BoundStatement>> batches = Lists.partition(statements, BATCH_SIZE);
        boolean result = false;
        for (List<BoundStatement> batch : batches) {
            BatchStatement batchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
            batchStatement.addAll(batch);
            @FixMe(createdBy = "dhudson", description = "This is a fudge and should be looked at in the future")
            int retryCount = 0;
            boolean complete = false;
            while (!complete) {
                try {
                    ResultSet resultSet = getCassandraContext().getSession().execute(batchStatement);
                    result = resultSet.wasApplied();
                    complete = true;
                } catch (WriteTimeoutException ex) {
                    retryCount++;
                    logger.warn("LMSE100: Unable to complete batch in time, retry count {}", retryCount);
                    if (retryCount == 3) {
                        // Just throw the exception like the old days.
                        throw ex;
                    }
                }
            }
        }

        return result;
    }

    public CompletableFuture<Boolean> executeAsAsync(List<BoundStatement> statements) throws GUSLErrorException {

        CompletableFuture<Boolean> future = new CompletableFuture<>();

        BatchStatement batchStatement = new BatchStatement(BatchStatement.Type.LOGGED);
        batchStatement.addAll(statements);

        ResultSetFuture resultFuture = getCassandraContext().getSession().executeAsync(batchStatement);
        resultFuture.addListener(() -> {
            try {
                ResultSet result = resultFuture.get();
                future.complete(Boolean.TRUE);
            } catch (ExecutionException e) {
                future.completeExceptionally(e.getCause());
            } catch (Throwable t) {
                future.completeExceptionally(t);
            }
        }, getCassandraContext().getExecutor());

        return future;
    }

    public List<T> getAll(List<Predicate<T>> predicates) throws GUSLErrorException {
        checkInit();
        try {
            // Need to fix s/be using stream but it did not work so streaming list
            //Stream<T> stream = allSelector.select().asList().stream();
            Stream<T> stream = allSelector.select().asList().execute().stream();
            if (stream == null) {
                return new ArrayList<>(0);
            }
            return stream.filter(row -> Utils.safeStream(predicates).noneMatch((predicate) -> (!predicate.test(row))))
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            logger.warn("Table: {} - error getting all records", theTable.getTableName(), ex);
            throw new GUSLErrorException(CassandraErrors.DAO_GET_ALL_ERROR.getError(new String[]{theTable.getTableName(), ex.getMessage()}), ex);
        }

    }

    /**
     * The kids like optional.
     *
     * @param id
     * @return
     */
    public Optional<T> getOne(Long id) {
        try {
            return Optional.ofNullable(findById(id));
        } catch (GUSLErrorException notFound) {
            return Optional.empty();
        }
    }

    public Optional<T> getOne(Long partitionId, Long id) {
        try {
            return Optional.ofNullable(findById(partitionId, id));
        } catch (GUSLErrorException notFound) {
            return Optional.empty();
        }
    }

    // -- Views -- //
    public <VIEW> Stream<VIEW> stream(Class<VIEW> clazz) throws GUSLErrorException {
        checkInit();
        try {
            LmSelectExecutor<VIEW> viewLmSelectExecutor;
            if (theViewMap.containsKey(clazz)) {
                viewLmSelectExecutor = theViewMap.get(clazz);
                logger.debug("Found view in cache {}", clazz.getName());
            } else {

                // Need to map the fields in the class to columns
                Set<Field> fields = ClassUtils.getFieldsFor(clazz, false);
                Map<String, Column> columnMap = theTable.getColumnsAsMap();
                List<Column> viewColumns = new ArrayList<>(fields.size());

                for (Field field : fields) {
                    String name = field.getName();
                    if (name.startsWith("$") || name.equals("serialVersionUID")) {
                        continue;
                    }

                    String fieldName = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, field.getName());
                    if (columnMap.containsKey(fieldName)) {
                        // Getter and setters are different, so make a copy of the column
                        viewColumns.add(columnMap.get(fieldName).copy());
                    }
                }

                viewLmSelectExecutor =
                        new SelectBuilder<VIEW>(getCassandraContext(), viewColumns)
                                .from(theTable)
                                .into(clazz)
                                .withMetric(clazz.getName() + "-view")
                                .prepare();
                theViewMap.put(clazz, viewLmSelectExecutor);
            }
            return viewLmSelectExecutor.select().asList().stream();
        } catch (GUSLException ex) {
            logger.warn("View Error ...  ", ex);
            throw new GUSLErrorException();
        }
    }

    public <VIEW> VIEW findBy(Class<VIEW> clazz, Object key) throws GUSLErrorException {
        throw new UnsupportedOperationException();
    }

    public <VIEW> VIEW findBy(Class<VIEW> clazz, Object key, Object partition) throws GUSLErrorException {
        throw new UnsupportedOperationException();
    }

    public <VIEW> VIEW findBy(Class<VIEW> clazz, Object key, Object partition, Object secondary) throws GUSLErrorException {
        throw new UnsupportedOperationException();
    }

}
