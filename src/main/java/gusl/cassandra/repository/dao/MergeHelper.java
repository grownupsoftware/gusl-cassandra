package gusl.cassandra.repository.dao;

import gusl.cassandra.builder.Column;
import gusl.cassandra.builder.Table;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static gusl.cassandra.repository.dao.AbstractDAOCassImpl.UNKNOWN_DATE;

/**
 * @param <T>
 * @author dhudson
 */
@CustomLog
public class MergeHelper<T> {

    private final List<Column> updatableCols;
    private static final Void NULL = null;

    public MergeHelper() {
        updatableCols = new ArrayList<>();
    }

    void init(Table table) {
        Column[] columns = table.getColumns();
        Column[] keys = table.getPrimaryKeys();

        for (Column column : columns) {
            if (!isPrimaryKey(column, keys)) {
                updatableCols.add(column);
            }
        }
    }

    private boolean isPrimaryKey(Column column, Column[] keys) {
        if (column.isStaticPartitionKey()) {
            return true;
        }
        for (Column key : keys) {
            if (key.getName().equals(column.getName())) {
                return true;
            }
        }

        return false;
    }

    boolean mergeUpdateFields(T currentEntity, T entity) {
        boolean updated = false;

        for (Column column : updatableCols) {
            try {
                if (column.getGetterMethod().invoke(entity) != null) {
                    if (isDate(column)) {
                        if (UNKNOWN_DATE.equals(column.getGetterMethod().invoke(entity))) {
                            column.getSetterMethod().invoke(currentEntity, NULL);
                            continue;
                        }
                    }

                    column.getSetterMethod().invoke(currentEntity, column.getGetterMethod().invoke(entity));
                    updated = true;
                }
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                logger.warn("Unable to update field {} for {}", column.getName(), entity, ex);
            }
        }

        return updated;
    }

    private boolean isDate(Column column) {

        switch (column.getDataType()) {
            case DATE:
            case TIMESTAMP:
            case TIMESTAMP_INSTANT:
            case ZONED_DATE_TIME:
                return true;

            default:
                return false;
        }
    }
}
