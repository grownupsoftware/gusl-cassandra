package gusl.cassandra.repository.dao;

import com.datastax.driver.core.BoundStatement;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PageResponse;
import gusl.cassandra.builder.Table;
import gusl.core.exceptions.GUSLErrorException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Stream;

public interface DAOInterface<T> {

    T findById(Long id) throws GUSLErrorException;

    T findById(Long partitionId, Long id) throws GUSLErrorException;

    T findById(String partitionId, String id) throws GUSLErrorException;

    T findById(Long partitionId, String id) throws GUSLErrorException;

    T findById(String partitionId, String id, Long recordId) throws GUSLErrorException;

    T findById(String partitionId, String id, Integer recordId) throws GUSLErrorException;

    T findById(Long partitionId, Long secondaryId, Long recordId) throws GUSLErrorException;

    T findById(String partitionId, String secondaryId, String recordId) throws GUSLErrorException;

    T findById(String id) throws GUSLErrorException;

    List<T> getAll() throws GUSLErrorException;

    Stream<T> stream() throws GUSLErrorException;

    void insert(T entity) throws GUSLErrorException;

    T update(T entity) throws GUSLErrorException;

    void delete(Long id) throws GUSLErrorException;

    void delete(String id) throws GUSLErrorException;

    void delete(Long partitionId, Long id) throws GUSLErrorException;

    void delete(String partitionId, String id) throws GUSLErrorException;

    void delete(Long partitionId, String id) throws GUSLErrorException;

    void delete(String partitionId, String id, Long recId) throws GUSLErrorException;

    void delete(Long partitionId, Long secondaryId, Long recordId) throws GUSLErrorException;

    void delete(String partitionId, String secondaryId, String recordId) throws GUSLErrorException;

    void insertBatch(List<T> list) throws GUSLErrorException;

    void updateBatch(List<T> list) throws GUSLErrorException;

    BoundStatement createInsertBoundStatement(T entity) throws GUSLErrorException;

    BoundStatement createUpdateBoundStatement(T entity) throws GUSLErrorException;

    BoundStatement createDeleteBoundStatement(Long id) throws GUSLErrorException;

    boolean execute(List<BoundStatement> statements) throws GUSLErrorException;

    Table getTable();

    Optional<T> getOne(Long id);

    Optional<T> getOne(Long partitionId, Long id);

    Optional<T> findRecord(T entity);

    List<T> getAll(List<Predicate<T>> predicates) throws GUSLErrorException;

    CompletableFuture<List<T>> getAllAsync();

    PageResponse<T> getAllPaged(PageRequest request) throws GUSLErrorException;

    // -- Views -- //

    <VIEW> Stream<VIEW> stream(Class<VIEW> clazz) throws GUSLErrorException;

    /**
     <VIEW> VIEW findBy(Class<VIEW> clazz, Object key) throws GUSLErrorException;

     <VIEW> VIEW findBy(Class<VIEW> clazz, Object key, Object partition) throws GUSLErrorException;

     <VIEW> VIEW findBy(Class<VIEW> clazz, Object key, Object partition, Object secondary) throws GUSLErrorException;
     */

}
