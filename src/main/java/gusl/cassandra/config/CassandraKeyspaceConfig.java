/* Copyright lottomart */
package gusl.cassandra.config;

import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;

/**
 *
 * @author grant
 */
@DocClass(description = "Cassandra Keyspace Configuration")
public class CassandraKeyspaceConfig extends CassandraConnectionConfig {

    @DocField(description = "When set to false, data written to the keyspace bypasses the commit log")
    private Boolean durableWrites;

    @DocField(description = "Replica strategy class")
    private String strategyClass;

    @DocField(description = "Strategy class specific replication options")
    private String strategyOptions;

    public Boolean getDurableWrites() {
        return durableWrites;
    }

    public void setDurableWrites(Boolean durableWrites) {
        this.durableWrites = durableWrites;
    }

    public String getStrategyClass() {
        return strategyClass;
    }

    public void setStrategyClass(String strategyClass) {
        this.strategyClass = strategyClass;
    }

    public String getStrategyOptions() {
        return strategyOptions;
    }

    public void setStrategyOptions(String strategyOptions) {
        this.strategyOptions = strategyOptions;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
