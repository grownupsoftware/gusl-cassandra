/* Copyright lottomart */
package gusl.cassandra.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import gusl.core.annotations.DocClass;
import gusl.core.annotations.DocField;
import gusl.core.tostring.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @author grant
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@DocClass(description = "Cassandra evolve / migrate configurations")
public class CassandraMigrateConfig { // extends NodeConfig {

    @DocField(description = "List of keyspace configurations")
    List<CassandraKeyspaceConfig> cassandraKeyspaceConfigs;

    public List<CassandraKeyspaceConfig> getCassandraKeyspaceConfigs() {
        return cassandraKeyspaceConfigs;
    }

    public void setCassandraKeyspaceConfigs(List<CassandraKeyspaceConfig> cassandraKeyspaceConfigs) {
        this.cassandraKeyspaceConfigs = cassandraKeyspaceConfigs;
    }

    public CassandraKeyspaceConfig getMigrateConfigFor(String keyspace) {
        for (CassandraKeyspaceConfig config : cassandraKeyspaceConfigs) {
            if (config.getKeyspace().equalsIgnoreCase(keyspace.toLowerCase())) {
                return config;
            }
        }

        return null;
    }

    public CassandraConnectionConfig getConfigFor(String keyspace) {
        for (CassandraConnectionConfig config : getCassandraKeyspaceConfigs()) {
            if (config.getKeyspace().equalsIgnoreCase(keyspace.toLowerCase())) {
                return config;
            }
        }

        return null;
    }

    public List<CassandraConnectionConfig> getConnectionConfigs() {
        List<CassandraConnectionConfig> list = new ArrayList<>(3);
        for (CassandraKeyspaceConfig cassandraConfig : getCassandraKeyspaceConfigs()) {
            list.add(cassandraConfig);
        }
        return list;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
