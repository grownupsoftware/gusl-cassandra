/* Copyright lottomart */
package gusl.cassandra.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import gusl.core.annotations.DocField;
import gusl.core.config.ObfuscationDeserializer;
import gusl.core.config.SystemProperyDeserializer;
import gusl.core.tostring.ToString;
import gusl.core.tostring.ToStringMask;

/**
 *
 * @author grant
 */
public class CassandraConnectionConfig {

    @DocField(description = "Cassandra key space")
    private String keyspace;

    @DocField(description = "Cassandra username")
    private String username;

    @DocField(description = "Cassandra password")
    @JsonDeserialize(using = ObfuscationDeserializer.class)
    @ToStringMask
    private String password;

    @DocField(description = "Cassandra connection points, | (pipe) delimited string")
    @JsonDeserialize(using = SystemProperyDeserializer.class)
    private String connectionPoints;

    @DocField(description = "Number of retry reads")
    private int retryRead;

    @DocField(description = "Number of retry writes")
    private int retryWrite;

    @DocField(description = "Number of retry no host available")
    private int retryHostUnavailable;

    @DocField(description = "Degrade consistency on retry")
    private boolean degradeConsistency;

    @DocField(description = "Consistency level")
    private String consistencyLevel;

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnectionPoints() {
        return connectionPoints;
    }

    public void setConnectionPoints(String connectionPoints) {
        this.connectionPoints = connectionPoints;
    }

    public int getRetryRead() {
        return retryRead;
    }

    public void setRetryRead(int retryRead) {
        this.retryRead = retryRead;
    }

    public int getRetryWrite() {
        return retryWrite;
    }

    public void setRetryWrite(int retryWrite) {
        this.retryWrite = retryWrite;
    }

    public int getRetryHostUnavailable() {
        return retryHostUnavailable;
    }

    public void setRetryHostUnavailable(int retryHostUnavailable) {
        this.retryHostUnavailable = retryHostUnavailable;
    }

    public boolean isDegradeConsistency() {
        return degradeConsistency;
    }

    public void setDegradeConsistency(boolean degradeConsistency) {
        this.degradeConsistency = degradeConsistency;
    }

    public String getConsistencyLevel() {
        return consistencyLevel;
    }

    public void setConsistencyLevel(String consistencyLevel) {
        this.consistencyLevel = consistencyLevel;
    }

    public String[] getArrayOfConnectionPoints() {
        return connectionPoints.split("\\|");
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
