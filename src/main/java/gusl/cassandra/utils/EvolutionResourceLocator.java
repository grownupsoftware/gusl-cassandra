package gusl.cassandra.utils;

import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

/**
 * Find migrate or mode scripts.
 * <p>
 * Might be an idea to move this to IOUtils.
 *
 * @author dhudson
 */
@CustomLog
public class EvolutionResourceLocator {

    private EvolutionResourceLocator() {
    }

    static Set<String> findMigrateScripts(String location) throws CassandraEvolutionException {
        Set<String> files = findResourceFiles(location);
        return files.stream().filter(t -> t.startsWith("V") && t.endsWith(".cql")).collect(Collectors.toSet());
    }

    static Set<String> findModeScripts(String location, String keyspace) throws CassandraEvolutionException {
        Set<String> files = findResourceFiles(location);
        return files.stream().filter(t -> t.startsWith(keyspace) && t.endsWith(".cql")).collect(Collectors.toSet());
    }

    private static Set<String> findResourceFiles(String location) throws CassandraEvolutionException {
        Set<String> files;
        URL url;

        try {
            url = IOUtils.getResourceAsURL(location, EvolutionResourceLocator.class.getClassLoader());
        } catch (IOException ex) {
            logger.warn("Unable to locate scripts folder {}", location);
            return Collections.<String>emptySet();
        }

        if (url.getProtocol().equals("jar")) {
            try {
                // Nested in a jar of war
                files = findJarResourceNames(location, url);
            } catch (IOException ex) {
                logger.warn("Unable to extract scripts from jar {}", url);
                throw new CassandraEvolutionException("Unable to locate scripts folder " + location, ex);
            }
        } else {
            // Its files
            files = findFileResourceNames(url, location);
        }

        return files;
    }

    private static Set<String> findJarResourceNames(String location, URL locationUrl) throws IOException {
        try (JarFile jarFile = getJarFromUrl(locationUrl)) {
            // For Tomcat and non-expanded WARs.
            String prefix = jarFile.getName().toLowerCase().endsWith(".war") ? "WEB-INF/classes/" : "";
            return findResourceNamesFromJarFile(jarFile, prefix, location);
        }
    }

    /**
     * Retrieves the Jar file represented by this URL.
     *
     * @param locationUrl The URL of the jar.
     * @return The jar file.
     * @throws IOException when the jar could not be resolved.
     */
    private static JarFile getJarFromUrl(URL locationUrl) throws IOException {
        URLConnection con = locationUrl.openConnection();
        if (con instanceof JarURLConnection) {
            // Should usually be the case for traditional JAR files.
            JarURLConnection jarCon = (JarURLConnection) con;
            jarCon.setUseCaches(false);
            return jarCon.getJarFile();
        }

        // No JarURLConnection -> need to resort to URL file parsing.
        // We'll assume URLs of the format "jar:path!/entry", with the protocol
        // being arbitrary as long as following the entry format.
        // We'll also handle paths with and without leading "file:" prefix.
        String urlFile = locationUrl.getFile();

        int separatorIndex = urlFile.indexOf("!/");
        if (separatorIndex != -1) {
            String jarFileUrl = urlFile.substring(0, separatorIndex);
            if (jarFileUrl.startsWith("file:")) {
                try {
                    return new JarFile(new URL(jarFileUrl).toURI().getSchemeSpecificPart());
                } catch (URISyntaxException ex) {
                    // Fallback for URLs that are not valid URIs (should hardly ever happen).
                    return new JarFile(jarFileUrl.substring("file:".length()));
                }
            }
            return new JarFile(jarFileUrl);
        }

        return new JarFile(urlFile);
    }

    private static Set<String> findFileResourceNames(URL url, String resourceFolder) {
        Set<String> resourceNames = new TreeSet<>();

        File folder;
        try {
            folder = new File(url.toURI());
        } catch (URISyntaxException ex) {
            logger.info("Unable to find resource {}", resourceFolder);
            return resourceNames;
        }

        if (!folder.isDirectory()) {
            logger.info("Resource {} is not a directory", resourceFolder);
            return resourceNames;
        }

        String[] scripts = folder.list();

        if (scripts.length == 0) {
            // Nothing to do
            logger.info("No scripts found in {}", folder.getAbsolutePath());
            return resourceNames;
        }

        for (String script : scripts) {
            resourceNames.add(script);
        }

        return resourceNames;
    }

    /**
     * Finds all the resource names contained in this directory within this jar
     * file.
     *
     * @param jarFile  The jar file.
     * @param prefix   The prefix to ignore within the jar file.
     * @param location The location to look under.
     * @return The resource names.
     * @throws IOException when reading the jar file failed.
     */
    private static Set<String> findResourceNamesFromJarFile(JarFile jarFile, String prefix, String location) throws IOException {
        String toScan = prefix + location + (location.endsWith("/") ? "" : "/");
        Set<String> resourceNames = new TreeSet<>();

        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            String entryName = entries.nextElement().getName();
            if (entryName.startsWith(toScan)) {
                if (entryName.equals(toScan)) {
                    // Its the folder not interested
                    continue;
                }
                resourceNames.add(entryName.substring(entryName.lastIndexOf("/") + 1));
            }
        }

        return resourceNames;
    }
}
