package gusl.cassandra.utils;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.WriteType;
import com.datastax.driver.core.exceptions.DriverException;
import com.datastax.driver.core.policies.ReconnectionPolicy;
import com.datastax.driver.core.policies.RetryPolicy;
import lombok.CustomLog;

/**
 * @author grant
 */
@CustomLog
public class CassandraRetryPolicy implements RetryPolicy, ReconnectionPolicy {

    private int readAttempts;
    private int writeAttempts;
    private int unavailableAttempts;
    private boolean degradeConsistency;
    private long delayMs;
    private int reconnectAttempts;

    private boolean hasInited;
    private boolean hasClosed;

    private CassandraTimeoutListener timeoutListener;

    public CassandraRetryPolicy() {
    }

    public void configure(int readAttempts, int writeAttempts, int unavailableAttempts, boolean degradeConsistency, long reconnectDelay, int reconnectAttempts) {
        this.readAttempts = readAttempts;
        this.writeAttempts = writeAttempts;
        this.unavailableAttempts = unavailableAttempts;
        this.degradeConsistency = degradeConsistency;
        this.delayMs = reconnectDelay;
        this.reconnectAttempts = reconnectAttempts;
    }

    public CassandraTimeoutListener getTimeoutListener() {
        return timeoutListener;
    }

    public void setTimeoutListener(CassandraTimeoutListener timeoutListener) {
        this.timeoutListener = timeoutListener;
    }

    @Override
    public RetryDecision onReadTimeout(Statement statement, ConsistencyLevel consistencyLevel, int requiredNumHosts, int numHostsAlive, boolean dataReceived, int retriedCount) {
        logger.warn("Read Timeout: statement: [{}] consistencyLevel: [{}] requiredNumHosts:[{}] numHostsAlive: [{}] dataReceived: [{}] retriedCount: [{}] of [{}] attempts", statement, consistencyLevel, requiredNumHosts, numHostsAlive, dataReceived, retriedCount, readAttempts);
        // dont use if (dataReceived)
//        if (dataReceived) {
//            return RetryDecision.ignore();
//        } else 
        if (retriedCount < readAttempts) {
            return RetryDecision.retry(consistencyLevel);
        } else {
            if (timeoutListener != null) {
                timeoutListener.handleReadTimeout();
            }

            return RetryDecision.rethrow();
        }
    }

    @Override
    public RetryDecision onWriteTimeout(Statement statement, ConsistencyLevel consistencyLevel, WriteType writeType, int requiredNumHosts, int numHostsAlive, int retriedCount) {
        logger.warn("Write Timeout: statement: [{}] consistencyLevel: [{}] writeType: [{}] requiredNumHosts:[{}] numHostsAlive: [{}] retriedCount: [{}] of [{}] attempts", statement, consistencyLevel, writeType, requiredNumHosts, numHostsAlive, retriedCount, writeAttempts);
        if (retriedCount < writeAttempts) {
            return RetryDecision.retry(consistencyLevel);
        }

        if (timeoutListener != null) {
            timeoutListener.handleWriteTimeout();
        }

        return RetryDecision.rethrow();
    }

    @Override
    public RetryDecision onUnavailable(Statement statement, ConsistencyLevel consistencyLevel, int requiredNumHosts, int numHostsAlive, int retriedCount) {
        if (retriedCount < unavailableAttempts) {
            if (degradeConsistency) {
                logger.warn("Hosts Unavailable: statement: [{}] consistencyLevel: [{}] requiredNumHosts:[{}] numHostsAlive: [{}] retriedCount: [{}] of [{}] attempts - downgrading to ConsistencyLevel.ONE", statement, consistencyLevel, requiredNumHosts, numHostsAlive, retriedCount, unavailableAttempts);
                return RetryDecision.retry(ConsistencyLevel.ONE);
                //return RetryDecision.tryNextHost(consistencyLevel);
            } else {
                logger.warn("Hosts Unavailable: statement: [{}] consistencyLevel: [{}] requiredNumHosts:[{}] numHostsAlive: [{}] retriedCount: [{}] of [{}] attempts - retrying", statement, consistencyLevel, requiredNumHosts, numHostsAlive, retriedCount, unavailableAttempts);
                return RetryDecision.retry(consistencyLevel);
            }
        }

        if (timeoutListener != null) {
            timeoutListener.handleUnavailable();
        }

        return RetryDecision.rethrow();
    }

    @Override
    public RetryDecision onRequestError(Statement statement, ConsistencyLevel consistencyLevel, DriverException driverException, int retriedCount) {
        logger.warn("Request Error: statement: [{}] consistencyLevel: [{}] DriverException:[{}]  retriedCount: [{}]", statement, consistencyLevel, driverException, retriedCount);
        //TODO: decide what best action is
        return RetryDecision.rethrow();
    }

    @Override
    public void init(Cluster clstr) {
        // Its called twice as the same class is used for retry and connection
        if (!hasInited) {
            logger.info("-- initialising Cassandra Retry Policy - Retry Read [{}] Retry Write [{}] Retry Host Unavailable [{}]", readAttempts, writeAttempts, unavailableAttempts);
            hasInited = true;
        }
    }

    @Override
    public void close() {
        if (!hasClosed) {
            logger.info("closing Cassandra Retry Policy");
            hasClosed = true;
        }
    }

    @Override
    public ReconnectionSchedule newSchedule() {
        logger.info("new reconnection call");
        return new ConstantSchedule();
    }

    public static interface CassandraTimeoutListener {

        void handleReadTimeout();

        void handleWriteTimeout();

        void handleUnavailable();
    }

    private class ConstantSchedule implements ReconnectionSchedule {

        private int reconnectAttemptsCount;
        private boolean notified = false;

        public ConstantSchedule() {
            super();
        }

        @Override
        public long nextDelayMs() {
            //logger.info("..........................we have no cassandra {} : {}", reconnectAttemptsCount, reconnectAttempts);
            reconnectAttemptsCount++;

            if (reconnectAttemptsCount > reconnectAttempts) {
                if (timeoutListener != null && !notified) {
                    logger.info("notifying listeners");
                    timeoutListener.handleUnavailable();
                    notified = true;
                }
            }

            return delayMs;
        }
    }
}
