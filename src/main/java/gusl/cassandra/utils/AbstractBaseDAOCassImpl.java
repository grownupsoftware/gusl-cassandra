package gusl.cassandra.utils;

import gusl.cassandra.builder.*;
import gusl.core.exceptions.GUSLException;
import lombok.CustomLog;

/**
 * @author grant
 */
@CustomLog
public class AbstractBaseDAOCassImpl {

    private CassandraContext theCassandraContext;

    public CassandraContext getCassandraContext() {
        return theCassandraContext;
    }

    public void setCassandraContext(CassandraContext theCassandraContext) {
        this.theCassandraContext = theCassandraContext;
    }

    public <T> SelectBuilder<T> select(Column... fields) throws GUSLException {
        checkInitialised();
        return new SelectBuilder<>(theCassandraContext, fields);
    }

    public <T> SelectBuilder<T> select() throws GUSLException {
        checkInitialised();
        return new SelectBuilder<>(theCassandraContext);
    }

    public <T> InsertBuilder<T> insertInto(Table table) throws GUSLException {
        checkInitialised();
        return new InsertBuilder<>(theCassandraContext, table);
    }

    public <T> UpdateBuilder<T> update(Table table) throws GUSLException {
        checkInitialised();
        return new UpdateBuilder<>(theCassandraContext, table);
    }

    public <T> DeleteBuilder<T> delete(Table table) throws GUSLException {
        checkInitialised();
        return new DeleteBuilder<>(theCassandraContext, table);
    }

    public boolean isInit() {
        return theCassandraContext != null;
    }

    public void checkInitialised() throws GUSLException {
        if (!isInit()) {
            throw new GUSLException("DAO has not been initialised with a Cassandra Context");
        }

    }
}
