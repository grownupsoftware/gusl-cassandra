package gusl.cassandra.utils;

/**
 * In line execution fetched Cassandra Rows
 *
 * @author dhudson
 * @param <T> Could be Row or a Mapped Class
 */
@FunctionalInterface
public interface RowProcessor<T> {

    /**
     * Row processor
     *
     * @param t
     * @return false to stop processing.
     */
    public boolean process(T t);

}
