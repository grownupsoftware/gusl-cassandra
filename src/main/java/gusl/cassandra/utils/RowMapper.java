package gusl.cassandra.utils;

import com.datastax.driver.core.Row;
import gusl.cassandra.builder.Column;

import java.util.List;

/**
 * Row Mapper Interface.
 * 
 * Maps rows to a type.
 * 
 * @author dhudson
 */
@FunctionalInterface
public interface RowMapper {
    
    /**
     * Create a T from the row.
     * 
     * @param row
     * @return a T
     */
    public <T> T mapRow(Row row, Class<T> retClazz, List<Column> columns);
    
}
