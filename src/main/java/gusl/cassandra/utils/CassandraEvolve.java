package gusl.cassandra.utils;

import com.datastax.driver.core.Session;

/**
 * Interface for the Exec statement of a CQL file.
 *
 * @author dhudson
 */
public interface CassandraEvolve {

    /**
     * This will be called if there is an EXEC statement in the CQL file.
     *
     * @param session
     */
    public void evolve(Session session);

}
