package gusl.cassandra.utils;

import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.core.policies.WhiteListPolicy;
import gusl.cassandra.config.CassandraKeyspaceConfig;
import gusl.core.utils.IOUtils;
import gusl.core.utils.StringUtils;
import lombok.CustomLog;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;

/**
 * This is a take on flyway.
 * <p>
 * Use this to look for Cassandra scripts and execute them in order.
 * <p>
 * Concept stolen from ..
 * <p>
 * https://gist.github.com/brianwawok/01db9d8a1efcff5d2691
 *
 * @author dhudson
 */
@CustomLog
public class CassandraEvolution {

    public static final String MODE_BASE_DIRECTORY = "cassandra/mode/";
    public static final String INSTANCE_BASE_DIRECTORY = "cassandra/instance/";
    public static final String EVOLUTION_BASE_DIRECTORY = "cassandra/evolution/";
    public static final String STATIC_PATTERN = "_static_";

    private CassandraKeyspaceConfig theCassandraKeyspaceConfig;

    private static final String EVOLUTION_TABLE_CQL = "CREATE TABLE IF NOT EXISTS evolutions(name text, applied_date timestamp, crc int, PRIMARY KEY (name));";
    private static final String EVOLUTION_SELECT_CQL = "SELECT name, applied_date, crc from evolutions;";
    private static final String EVOLUTION_INSERT_CQL = "INSERT INTO evolutions (name, applied_date,crc) values (?,?,?);";

    private final HashMap<String, List<DiskMigrateScript>> theMigrateScriptCache;
    private final HashMap<String, Session> theKeyspaceSession;

    // We have one cluster, maybe more than one session
    // Connections are to a single node as we are modifing schema's
    private Cluster theCluster;
    private static final int DEFAULT_PORT = 9042;

    public CassandraEvolution() {
        theMigrateScriptCache = new HashMap<>(5);
        theKeyspaceSession = new HashMap<>(5);
    }

    public void validateKeyspaceExists() {
        try (Session session = getCluster().connect()) {

            KeyspaceMetadata keyspaceMD = session.getCluster().getMetadata().getKeyspace(getKeyspace());

            if (keyspaceMD == null) {
                logger.warn("Keyspace '{}' does not exists - will create", getKeyspace());
                session.execute("CREATE KEYSPACE " + getKeyspace() + " WITH REPLICATION = {'class': '" + theCassandraKeyspaceConfig.getStrategyClass() + "', " + theCassandraKeyspaceConfig.getStrategyOptions() + "} AND DURABLE_WRITES = " + theCassandraKeyspaceConfig.getDurableWrites() + ";");
                logger.info("Keyspace: {} created with durable writes: {} strategy class: {} strategy Options: {}", getKeyspace(), theCassandraKeyspaceConfig.getDurableWrites(), theCassandraKeyspaceConfig.getStrategyClass(), theCassandraKeyspaceConfig.getStrategyOptions());
            }
        }
    }

    /**
     * Clean.
     * <p>
     * Removes the metadata table (evolutions) from the keyspace</p>
     *
     * @throws CassandraEvolutionException
     */
    public void clean() throws CassandraEvolutionException {

        logger.info("Cleaning {} ", getKeyspace());

        Session session = getSession();

        logger.info("    Dropping keyspace {}", getKeyspace());
        SimpleStatement drop = new SimpleStatement("DROP KEYSPACE IF EXISTS " + getKeyspace());
        // Five minutes as it does a snapshot
        drop.setReadTimeoutMillis(5 * 1000 * 60 * 60);
        session.execute(drop);
        validateKeyspaceExists();
    }

    public void truncate() throws CassandraEvolutionException {

        logger.info("Cleaning {} ", getKeyspace());

        Session session = getSession();

        // Lets get a list of tables and drop them one by one.
        // This includes the evolution table
        for (String tableName : getTableNames()) {
            if (!"evolutions".equals(tableName)) {
                logger.info("    Truncating table {}", tableName);
                session.execute("TRUNCATE " + tableName);
            }
        }

    }

    private List<String> getTableNames() throws CassandraEvolutionException {
        Session session = getSession();

        KeyspaceMetadata keyspaceMD = session.getCluster().getMetadata().getKeyspace(getKeyspace());
        Collection<TableMetadata> tables = keyspaceMD.getTables();
        List<String> tableNames = new ArrayList<>(tables.size());

        for (TableMetadata table : tables) {
            tableNames.add(table.getName());
        }

        return tableNames;
    }

    /**
     * Check to see if the keyspace needs migrating.
     *
     * @return true if the keyspace needs migrating
     * @throws CassandraEvolutionException
     */
    public boolean needsMigrating() throws CassandraEvolutionException {
        return !evaluate().isEmpty();
    }

    /**
     * Return a list of scripts required to be executed in order for the
     * keyspace to be migrated.
     *
     * @return A list of scripts, or an empty list
     * @throws CassandraEvolutionException
     */
    public List<DiskMigrateScript> evaluate() throws CassandraEvolutionException {
        return evaluate(getDiskMigrateScripts(), getTableMigratedScripts());
    }

    /*
     * Starts the database migration. All pending migrations will be applied in order.
     * Calling migrate on an up-to-date database has no effect.
     *
     * @return The number of successfully applied migrations.
     * @throws CassandraMigrateException when the migration failed.
     */
    public int migrate() throws CassandraEvolutionException {

        Session session = getSession();

        // Lets see if a migrate a already running.
        List<DiskMigrateScript> scriptsToExecute = evaluate();
        int count = 0;

        try {
            for (DiskMigrateScript script : scriptsToExecute) {
                logger.info("Executing {} migrate script", script.getResourceFilename());
                //CassandraHelper.executeCqlStatements(session, script.getCommands());
                CassandraHelper.executeCqlMigrationStatements(session, script.getCommands());
                session.execute(EVOLUTION_INSERT_CQL, script.getName(), new Date(), script.getCRC());
                count++;
            }
        } catch (Exception ex) {
            logger.warn("Unable to perform migration", ex);
            throw new CassandraEvolutionException("Unable to perform migration", ex);
        }

        if (scriptsToExecute.isEmpty()) {
            logger.info("Schema for keyspace {} is up to date. No Migration necessary.", theCassandraKeyspaceConfig.getKeyspace());
        }

        return count;
    }

    /**
     * inserts static data from CQL scripts that match the STATIC_PATTERN - this
     * method works in conjuction with truncate() and static scripts will be
     * migrated in the normal way when migrate() is called
     *
     * @return
     * @throws CassandraEvolutionException
     */
    public int insertStaticData() throws CassandraEvolutionException {

        Session session = getSession();

        // Lets see if a migrate a already running.
        List<DiskMigrateScript> scriptsToExecute = getDiskMigrateScripts();
        int count = 0;

        try {
            for (DiskMigrateScript script : scriptsToExecute) {
                if (script.getResourceFilename().contains(STATIC_PATTERN)) {
                    logger.info("Executing {} migrate static script", script.getResourceFilename());
                    CassandraHelper.executeCqlStatements(session, script.getCommands());
                    session.execute(EVOLUTION_INSERT_CQL, script.getName(), new Date(), script.getCRC());
                    count++;
                }
            }
        } catch (Exception ex) {
            logger.warn("Unable to perform migration", ex);
            throw new CassandraEvolutionException("Unable to perform migration", ex);
        }

        if (scriptsToExecute.isEmpty()) {
            logger.info("Schema for keyspace {} is up to date. No static migration files exist.", getKeyspace());
        }

        return count;
    }

    public void setConfig(CassandraKeyspaceConfig cassandraKeyspaceConfig) {
        theCassandraKeyspaceConfig = cassandraKeyspaceConfig;
    }

    /**
     * Return a list of scripts to execute.
     *
     * @param migrateScripts  found using classpath
     * @param migratedScripts found in the database
     * @return List of scripts to execute
     */
    private List<DiskMigrateScript> evaluate(List<DiskMigrateScript> migrateScripts, List<TableMigrateScript> migratedScripts) {

        // Its clean or no migrations have been run
        if (migratedScripts.isEmpty()) {
            return migrateScripts;
        }

        List<DiskMigrateScript> scriptsToRun = new ArrayList<>();

        // get the latest version from the database
        double latestVersion = getHighestVersion(migratedScripts);

        for (DiskMigrateScript script : migrateScripts) {
            if (script.getVersion() > latestVersion) {
                scriptsToRun.add(script);
            }
        }

        return scriptsToRun;
    }

    private double getHighestVersion(List<? extends AbstractMigrateScript> scripts) {
        double version = 0;
        for (AbstractMigrateScript script : scripts) {
            if (script.getVersion() > version) {
                version = script.getVersion();
            }
        }

        return version;
    }

    private List<DiskMigrateScript> getDiskMigrateScripts() throws CassandraEvolutionException {

        List<DiskMigrateScript> migrateScripts = theMigrateScriptCache.get(getKeyspace());
        if (migrateScripts != null) {

            logger.info("Returning cached results");
            return migrateScripts;
        }

        // Lets see if there is anything to do.
        String resourceFolder = EVOLUTION_BASE_DIRECTORY + getKeyspace();

        migrateScripts = loadDiskMigrateScripts(resourceFolder);

        // Cache the results
        theMigrateScriptCache.put(getKeyspace(), migrateScripts);
        return migrateScripts;
    }

    private List<DiskMigrateScript> loadModeScripts(String resourceFolder) throws CassandraEvolutionException {
        List<DiskMigrateScript> migrateScripts = new ArrayList<>();
        for (String script : EvolutionResourceLocator.findModeScripts(resourceFolder, getKeyspace())) {
            try {
                migrateScripts.add(new DiskMigrateScript(resourceFolder, script));
            } catch (IOException ex) {
                // If we fail to load a script, make sure that no migrates happen.
                logger.error("Error qualifing migrate script {}", script, ex);
                migrateScripts.clear();
                return migrateScripts;
            }
        }
        return migrateScripts;
    }

    private List<DiskMigrateScript> loadDiskMigrateScripts(String resourceFolder) throws CassandraEvolutionException {
        List<DiskMigrateScript> migrateScripts = new ArrayList<>();
        for (String script : EvolutionResourceLocator.findMigrateScripts(resourceFolder)) {
            try {
                if (AbstractMigrateScript.isMigrationScript(script)) {
                    migrateScripts.add(new DiskMigrateScript(resourceFolder, script));
                }
            } catch (IOException ex) {
                // If we fail to load a script, make sure that no migrates happen.
                logger.error("Error qualifying migrate script {}", script, ex);
                migrateScripts.clear();
                return migrateScripts;
            }
        }

        // Check for duplicates
        if (migrateScripts.size() > 1) {

            Set<Double> versionSet = new HashSet<>(migrateScripts.size());

            for (DiskMigrateScript comp : migrateScripts) {
                if (versionSet.contains(comp.getVersion())) {
                    throw new CassandraEvolutionException(String.format("One or more scripts with the same version %s", comp.getResourceFilename()));
                }

                versionSet.add(comp.getVersion());
            }
            // Sort by version number 
            Collections.sort(migrateScripts);
        }
        return migrateScripts;
    }

    private List<TableMigrateScript> getTableMigratedScripts() throws CassandraEvolutionException {
        Session session = getSession();

        // Lets see if the 
        try {
            // First lets create, if required the Evoultion table
            session.execute(EVOLUTION_TABLE_CQL);
        } catch (Exception ex) {
            session.closeAsync();
            throw new CassandraEvolutionException("Unable load the evoultion table", ex);
        }

        ResultSet results = session.execute(EVOLUTION_SELECT_CQL);

        List<Row> rows = CassandraUtils.fetchAll(results);

        // Persisted Configs
        List<TableMigrateScript> migratedScripts = new ArrayList<>(rows.size());

        for (Row row : rows) {
            TableMigrateScript migrated = new TableMigrateScript(row);
            migratedScripts.add(migrated);
        }

        Collections.sort(migratedScripts);

        return migratedScripts;
    }

    private Session getSession() throws CassandraEvolutionException {
        if (theCassandraKeyspaceConfig == null) {
            throw new CassandraEvolutionException("setConfig not called");
        }

        Session session = theKeyspaceSession.get(getKeyspace());
        if (session == null) {
            session = getCluster().connect(getKeyspace());
            theKeyspaceSession.put(getKeyspace(), session);
        }

        return session;
    }

    @Override
    protected void finalize() throws Throwable {
        closeSessions();
        super.finalize();
    }

    public void closeSessions() {
        for (Session session : theKeyspaceSession.values()) {
            IOUtils.closeQuietly(session);
        }
        theKeyspaceSession.clear();
        if (theCluster != null) {
            theCluster.close();
        }
    }

    public void runModeScripts(String mode, String instance) throws CassandraEvolutionException {

        if (StringUtils.isBlank(mode)) {
            logger.warn("Null or empty mode no scripts will execute");
            return;
        }

        // Lets see if we can find any scripts.
        List<DiskMigrateScript> migrateScripts = loadModeScripts(MODE_BASE_DIRECTORY + mode.toLowerCase() + "/" + instance);

        try {
            for (DiskMigrateScript script : migrateScripts) {
                logger.info("Executing {} mode {} script", script.getResourceFilename(), mode);
                CassandraHelper.executeCqlStatements(getSession(), script.getCommands());
            }
        } catch (Exception ex) {
            logger.warn("Unable to perform mode scripts", ex);
            throw new CassandraEvolutionException("Unable to perform mode scripts", ex);
        }
    }

    public void runInstanceScripts(String instance) throws CassandraEvolutionException {

        if (StringUtils.isBlank(instance)) {
            logger.warn("Null or empty instance no scripts will execute");
            return;
        }

        // Lets see if we can find any scripts.
        List<DiskMigrateScript> migrateScripts = loadModeScripts(INSTANCE_BASE_DIRECTORY + instance.toLowerCase());

        try {
            for (DiskMigrateScript script : migrateScripts) {
                logger.info("Executing {} instance {} script", script.getResourceFilename(), instance);
                CassandraHelper.executeCqlStatements(getSession(), script.getCommands());
            }
        } catch (Exception ex) {
            logger.warn("Unable to perform instance scripts", ex);
            throw new CassandraEvolutionException("Unable to perform instance scripts", ex);
        }
    }

    private String getKeyspace() {
        return theCassandraKeyspaceConfig.getKeyspace();
    }

    private Cluster getCluster() {
        if (theCluster == null || theCluster.isClosed()) {
            // Creating a new cluster, all sessions are void
            closeSessions();

            ConsistencyLevel level = ConsistencyLevel.valueOf(theCassandraKeyspaceConfig.getConsistencyLevel());
            // Set the default consistency level.
            QueryOptions options = new QueryOptions();
            options.setConsistencyLevel(level);

            CassandraRetryPolicy retryPolicy = new CassandraRetryPolicy();
            retryPolicy.configure(theCassandraKeyspaceConfig.getRetryRead(), theCassandraKeyspaceConfig.getRetryWrite(),
                    theCassandraKeyspaceConfig.getRetryHostUnavailable(), theCassandraKeyspaceConfig.isDegradeConsistency(), 100, 10);

            List<InetSocketAddress> whiteList = new ArrayList<>(1);
            whiteList.add(new InetSocketAddress(theCassandraKeyspaceConfig.getArrayOfConnectionPoints()[0], DEFAULT_PORT));

            theCluster = Cluster.builder()
                    .addContactPoints(theCassandraKeyspaceConfig.getArrayOfConnectionPoints())
                    .withLoadBalancingPolicy(new WhiteListPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build()), whiteList))
                    .withCredentials(theCassandraKeyspaceConfig.getUsername(), theCassandraKeyspaceConfig.getPassword())
                    .withQueryOptions(options)
                    .withReconnectionPolicy(retryPolicy)
                    .withRetryPolicy(retryPolicy)
                    // .withPoolingOptions(new PoolingOptions().setHeartbeatIntervalSeconds(50))
                    .withPoolingOptions(new PoolingOptions()
                            .setConnectionsPerHost(HostDistance.LOCAL, 1, 2)
                            .setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
                            .setCoreConnectionsPerHost(HostDistance.LOCAL, 2))
                    .withTimestampGenerator(new AtomicMonotonicTimestampGenerator()) // ensure ordering of update statements
                    // Make unique to stop the JMX / Metrics grumbling about clashes
                    .withClusterName(UUID.randomUUID().toString())
                    .withMaxSchemaAgreementWaitSeconds(30)
                    .build();

            Configuration clusterConfig = theCluster.getConfiguration();
            clusterConfig.getSocketOptions().setReadTimeoutMillis(20000);
        }

        return theCluster;
    }
}
