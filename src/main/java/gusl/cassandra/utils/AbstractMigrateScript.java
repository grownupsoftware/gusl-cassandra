package gusl.cassandra.utils;

import lombok.CustomLog;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A Migrate Script may be on disk, or an entry in the evolutions table.
 *
 * @author dhudson
 */
@CustomLog
public abstract class AbstractMigrateScript implements Comparable<AbstractMigrateScript> {

    private static final Pattern theVersionPattern = Pattern.compile("^V(.+?)__");

    private final String theName;
    private final double theVersion;
    private final int theCRC;

    public AbstractMigrateScript(String scriptName) {
        theName = scriptName;
        theVersion = getVersionFromName(scriptName);
        // We are not using the CRC as yet, but its here for future proofing
        theCRC = 0;
    }

    public int getCRC() {
        return theCRC;
    }

    public double getVersion() {
        return theVersion;
    }

    public String getName() {
        return theName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractMigrateScript other = (AbstractMigrateScript) obj;

        // Enable this for CRC checking
//        if (this.theCRC != other.theCRC) {
//            return false;
//        }
        return getName().equals(other.getName());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(theName);
        return hash;
    }

    @Override
    public int compareTo(AbstractMigrateScript o) {
        if (theVersion < o.getVersion()) {
            return -1;
        }

        if (theVersion > o.getVersion()) {
            return 1;
        }

        return 0;
    }

    private static double getVersionFromName(String name) {
        Matcher matcher = theVersionPattern.matcher(name);
        if (matcher.find()) {
            String version = matcher.group(1);
            try {
                return Double.parseDouble(version);
            } catch (NumberFormatException ex) {
            }
        }
        return 0;
    }

    public static boolean isMigrationScript(String name) {
        double version = getVersionFromName(name);
        if (version == 0) {
            return false;
        }

        // We have a version, does it end in CQL
        return name.toLowerCase().endsWith(".cql");
    }
}
