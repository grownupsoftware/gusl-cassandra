package gusl.cassandra.utils;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.*;
import gusl.cassandra.builder.Column;
import gusl.core.exceptions.GUSLException;
import gusl.core.lambda.MutableBoolean;
import gusl.core.utils.Utils;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Objects.nonNull;

/**
 * @author dhudson
 */
public class CassandraUtils {

    public static final int TRANSACTIONS_FETCH_SIZE = 6000;
    public static final int TRANSACTIONS_FETCH_REFRESH = 1000;

    private CassandraUtils() {
    }

    /**
     * Check to see if throwable is a read timeout.
     * <p>
     * Sometimes the coordinator believes that there’s enough replicas alive to
     * process a query with the requested consistency level, but for some
     * reasons one or several nodes are too slow to answer within the predefined
     * timeout.
     *
     * @param t throwable to check
     * @return true if the throwable is a read timeout
     */
    public static boolean isReadTimeout(Throwable t) {
        return (t instanceof ReadTimeoutException);
    }

    /**
     * Check to see if throwable is a write timeout.
     * <p>
     * In a similar situation as above, if the query is a write, then a
     * WriteTimeoutException will be thrown. If the driver differentiates
     * between read and write timeout here, this is because a read is obviously
     * a non mutating operation, whereas a write is likely to be. If a write
     * timeouts at the coordinator level, there is no way to know whether the
     * mutation has been applied or not on the non-answering replica. As we’ll
     * see next, the way this error will be handled will thus depends on whether
     * the write operation was idempotent (which is the case of most statements
     * in CQL) or not (for counter updates, and append/prepend updates on
     * lists).
     *
     * @param t throwable to check
     * @return true if throwable is a write timeout
     */
    public static boolean isWriteTimeout(Throwable t) {
        return (t instanceof WriteTimeoutException);
    }

    /**
     * Check to see if throwable is read or write timeout.
     *
     * @param t throwable to check
     * @return true if throwable a read or write timeout
     */
    public static boolean isReadOrWriteTimeout(Throwable t) {
        return (isReadTimeout(t) || isWriteTimeout(t));
    }

    /**
     * Check to see if throwable is unavailable exception.
     * <p>
     * When a request reaches the coordinator and there is not enough replica
     * alive to achieve the requested consistency level, the driver will throw
     * an UnavailableException
     *
     * @param t
     * @return
     */
    public static boolean isUnavailableException(Throwable t) {
        return (t instanceof UnavailableException);
    }

    /**
     * Check to see if the throwable is a NoHostAvailableException.
     * <p>
     * In the situation where the driver is not able to establish a connection
     * to any Cassandra node, either at startup or at a later point in time, the
     * driver will throw a NoHostAvailableException.
     *
     * @param t
     * @return
     */
    public static boolean isNoHostUnavailableException(Throwable t) {
        return (t instanceof NoHostAvailableException);
    }

    /**
     * Check to see if the throwable is a Cassandra Driver Exception.
     * <p>
     * NB: This might not mean that Cassandra is unavailable, as it could be a
     * query exception.
     *
     * @param t throwable to check
     * @return true if its a sub class of DriverException
     */
    public static boolean isCassandraException(Throwable t) {
        return (t instanceof DriverException);
    }

    /**
     * Check to see if the cause of the exception was a Cassandra exception.
     * <p>
     * This will perform a nested search of the cause chain.
     *
     * @param t
     * @return true if any of the causes in the cause chain is a Cassandra
     * Exception
     */
    public static boolean isCauseCassandraException(Throwable t) {
        return GUSLException.containsInstanceOf(t, DriverException.class);
    }

    /**
     * Create a Bound Statement with the fetch size set.
     *
     * @param statement
     * @return bound statement
     */
    public static BoundStatement createBoundStatement(PreparedStatement statement) {
        BoundStatement boundStatement = new BoundStatement(statement);
        boundStatement.setFetchSize(TRANSACTIONS_FETCH_SIZE);
        return boundStatement;
    }

    /**
     * Return a List of type T for all of the rows in the result set.
     *
     * @param <T>
     * @param resultSet
     * @param mapper
     * @return List of type T which may be empty but not null
     */
    public static <T> List<T> fetchAll(ResultSet resultSet, RowMapper mapper, Class<T> retClass, List<Column> columns) {
        List<T> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            result.add(mapper.mapRow(row, retClass, columns));
        }

        return result;
    }

    public static <T> List<T> fetch(ResultSet resultSet, int numRows, RowMapper mapper, Class<T> retClass, List<Column> columns) {
        List<T> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        int rowCount = 0;
        while (rows.hasNext() && rowCount < numRows) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            result.add(mapper.mapRow(row, retClass, columns));
            rowCount++;
        }

        return result;
    }

    private static <T> boolean passesPredicates(T rowDo, List<Predicate<T>> predicates) {
        MutableBoolean passes = new MutableBoolean(true);
        Utils.safeStream(predicates).forEach(predicate -> {
            if (!predicate.test(rowDo)) {
                passes.set(false);
            }
        });
        return passes.get();
    }

    public static <T> long count(ResultSet resultSet, RowMapper mapper, Class<T> retClass, List<Column> columns, List<Predicate<T>> predicates) {
        Iterator<Row> rows = resultSet.iterator();

        boolean hasPredicates = nonNull(predicates) && !predicates.isEmpty();

        long rowCount = 0;
        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            final T rowDo = mapper.mapRow(row, retClass, columns);
            if (hasPredicates) {
                if (passesPredicates(rowDo, predicates)) {
                    rowCount++;
                }
            } else {
                rowCount++;
            }
        }

        return rowCount;
    }

    public static <T> List<T> fetch(ResultSet resultSet, int numRows, RowMapper mapper, Class<T> retClass, List<Column> columns, List<Predicate<T>> predicates) {
        List<T> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        boolean hasPredicates = nonNull(predicates) && !predicates.isEmpty();

        int rowCount = 0;
        while (rows.hasNext() && rowCount < numRows) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            final T rowDo = mapper.mapRow(row, retClass, columns);
            if (hasPredicates) {
                if (passesPredicates(rowDo, predicates)) {
                    result.add(rowDo);
                    rowCount++;
                }
            } else {
                result.add(rowDo);
                rowCount++;
            }
        }

        return result;
    }

    public static <T> Stream<T> stream(ResultSet resultSet, RowMapper mapper, Class<T> retClass, List<Column> columns) {
        Iterator<Row> rows = resultSet.iterator();
        Iterator<T> iter = new Iterator<T>() {
            @Override
            public boolean hasNext() {
                if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                    resultSet.fetchMoreResults();
                }

                return rows.hasNext();
            }

            @Override
            public T next() {
                return mapper.mapRow(rows.next(), retClass, columns);
            }
        };

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                iter, Spliterator.ORDERED | Spliterator.NONNULL), false);
    }

    /**
     * Return a list of type T from all the rows.
     * <p>
     * Each row is mapped first, and the given to the predicate for testing. If
     * the mapper is expensive, you might want to use a method where the
     * predicate is a row.
     *
     * @param <T>
     * @param resultSet
     * @param mapper
     * @param predicate
     * @return a list of T, can be empty but not null
     */
    public static <T> List<T> fetchAll(ResultSet resultSet, RowMapper mapper, Predicate<T> predicate, Class<T> retClass, List<Column> columns) {
        List<T> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            T mapped = mapper.mapRow(row, retClass, columns);
            if (predicate.test(mapped)) {
                result.add(mapped);
            }
        }

        return result;
    }

    /**
     * Return a list of T where the predicate matches and then the row is
     * mapped.
     *
     * @param <T>
     * @param resultSet
     * @param predicate
     * @param mapper
     * @return A list of T, may be empty but not null
     */
    public static <T> List<T> fetchAll(ResultSet resultSet, Predicate<Row> predicate, RowMapper mapper, Class<T> retClass, List<Column> columns) {
        List<T> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();

            if (predicate.test(row)) {
                result.add(mapper.mapRow(row, retClass, columns));
            }
        }

        return result;
    }

    /**
     * Return a list of rows where the predicate matches.
     *
     * @param resultSet
     * @param predicate
     * @return list of rows, could be empty, but not null
     */
    public static List<Row> fetchAll(ResultSet resultSet, Predicate<Row> predicate) {
        List<Row> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            if (predicate.test(row)) {
                result.add(row);
            }
        }

        return result;
    }

    /**
     * Return a list of Rows.
     *
     * @param resultSet
     * @return list of rows, might be empty but not null
     */
    public static List<Row> fetchAll(ResultSet resultSet) {
        List<Row> result = new ArrayList<>();
        Iterator<Row> rows = resultSet.iterator();

        while (rows.hasNext()) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            result.add(rows.next());
        }
        return result;
    }

    /**
     * Inline process all of the rows from the result where the predicate is
     * true.
     *
     * @param resultSet
     * @param predicate
     * @param processor
     */
    public static void processAll(ResultSet resultSet, Predicate<Row> predicate, RowProcessor<Row> processor) {
        Iterator<Row> rows = resultSet.iterator();
        boolean keepProcessing = true;
        while (rows.hasNext() && keepProcessing) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            if (predicate.test(row)) {
                keepProcessing = processor.process(row);
            }
        }
    }

    /**
     * Inline process all of the rows from the result set where the predicate is
     * true and map before the processor.
     *
     * @param <T>
     * @param resultSet
     * @param predicate
     * @param mapper
     * @param processor
     */
    public static <T> void processAll(ResultSet resultSet, Predicate<Row> predicate, RowMapper mapper, RowProcessor<T> processor, Class<T> retClass, List<Column> columns) {
        Iterator<Row> rows = resultSet.iterator();
        boolean keepProcessing = true;
        while (rows.hasNext() && keepProcessing) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            if (predicate.test(row)) {
                keepProcessing = processor.process(mapper.mapRow(row, retClass, columns));
            }
        }
    }

    /**
     * Inline process all of the rows from the result set where the predicate is
     * true and map before the processor.
     *
     * @param <T>
     * @param resultSet
     * @param mapper
     * @param predicate
     * @param processor
     */
    public static <T> void processAll(ResultSet resultSet, RowMapper mapper, Predicate<T> predicate, RowProcessor<T> processor, Class<T> retClass, List<Column> columns) {
        Iterator<Row> rows = resultSet.iterator();
        boolean keepProcessing = true;
        while (rows.hasNext() && keepProcessing) {
            if (resultSet.getAvailableWithoutFetching() == TRANSACTIONS_FETCH_REFRESH && !resultSet.isFullyFetched()) {
                resultSet.fetchMoreResults();
            }

            Row row = rows.next();
            T mapped = mapper.mapRow(row, retClass, columns);
            if (predicate.test(mapped)) {
                keepProcessing = processor.process(mapped);
            }
        }
    }

    public static <T> List<T> mapRows(List<Row> rows, RowMapper mapper, Class<T> retClass, List<Column> columns) {
        ArrayList<T> mapped = new ArrayList<>(rows.size());
        rows.forEach((row) -> {
            mapped.add(mapper.mapRow(row, retClass, columns));
        });

        return mapped;
    }
}
