package gusl.cassandra.utils;

import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Migrate script thats on disk.
 *
 * @author dhudson
 */
@CustomLog
public class DiskMigrateScript extends AbstractMigrateScript {

    private final String theResourceFolder;

    private final List<String> theCommands;

    public DiskMigrateScript(String resourceFolder, String fileName) throws IOException {
        super(fileName);
        theResourceFolder = resourceFolder;
        theCommands = new ArrayList<>();
        loadCommands();
    }

    public String getResourceFilename() {
        return theResourceFolder + "/" + getName();
    }

    private void calculateCrc() throws IOException {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        IOUtils.copyStream(IOUtils.getResourceAsStream(getResourceFilename()), baos);
//        final CRC32 crc32 = new CRC32();
//        crc32.update(baos.toByteArray());
//        setCRC((int) crc32.getValue());
//        setCRC(0);
    }

    public List<String> getCommands() {
        return theCommands;
    }

    private void loadCommands() throws IOException {

        try {
            InputStream is = IOUtils.getResourceAsStream(getResourceFilename(), DiskMigrateScript.class.getClassLoader());
            boolean comment = false;
            // instead of ' a field can be delimited by $$ ... used when many lines
            boolean dollar = false;

            try (BufferedReader in = new BufferedReader(new InputStreamReader(is))) {
                String line;
                StringBuilder statement = new StringBuilder(100);

                while ((line = in.readLine()) != null) {
                    if (line.startsWith("/*")) {
                        comment = true;
                    }
                    if (line.startsWith("$$")) {
                        dollar = !dollar;
                    }

                    if (!line.isEmpty() && !line.trim().startsWith("--") && !comment) {
                        statement.append(" ");
                        statement.append(line.trim());
                        if (dollar) {
                            continue;
                        }
                        if (!line.trim().endsWith(";")) {
                            // Keep looping until we find a ;
                            continue;
                        }

                        theCommands.add(statement.toString().trim());
                        statement.setLength(0);
                    }

                    if (comment) {
                        if (line.endsWith("*/") || line.startsWith("*/")) {
                            comment = false;
                        }
                    }
                }
            }
            IOUtils.closeQuietly(is);
        } catch (IOException ex) {
            logger.warn("Unable to load the CQL file {}", getResourceFilename());
            throw ex;
        }
    }
}
