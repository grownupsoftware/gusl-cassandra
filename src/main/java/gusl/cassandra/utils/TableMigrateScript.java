package gusl.cassandra.utils;

import com.datastax.driver.core.Row;

/**
 * Scripts from the evolve table.
 *
 * @author dhudson
 */
public class TableMigrateScript extends AbstractMigrateScript {

    public TableMigrateScript(Row row) {
        super(row.getString(0));
        //setCRC(row.getInt(2));
    }

}
