package gusl.cassandra.utils;

import com.datastax.driver.core.*;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.extras.codecs.jdk8.ZonedDateTimeCodec;
import gusl.cassandra.config.CassandraConnectionConfig;
import gusl.cassandra.config.CassandraKeyspaceConfig;
import gusl.core.utils.IOUtils;
import lombok.CustomLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.UUID;

/**
 * Helper class for use with Cassandra, to stop this stuff being scattered to
 * the four winds.
 *
 * @author dhudson
 */
@CustomLog
public class CassandraHelper {

    public static final String EXEC_COMMAND_STRING = "EXEC";

    private static Cluster theCluster;

    private CassandraHelper() {
        //  No, no...
    }

    /**
     * Check to see if the Cassandra is there.
     *
     * @param cassandraConnectionConfig
     * @return
     */
    public static boolean checkCassandraConnection(CassandraConnectionConfig cassandraConnectionConfig) {
        try {
            Session session = getCassandraSessionWithoutKeyspace(cassandraConnectionConfig);
            session.closeAsync();
        } catch (NoHostAvailableException ex) {
            return false;
        }

        return true;
    }

    public static void executeCassandraCql(CassandraConnectionConfig cassandraConnectionConfig, String cqlFilename) throws Exception {

        // Step 1 - check if we can find the file
        InputStream resourceAsStream;
        try {
            resourceAsStream = IOUtils.getResourceAsStream(cqlFilename, CassandraHelper.class.getClassLoader());
        } catch (IOException ex) {
            logger.error("Failed to find CQL file: [{}]", cqlFilename);
            throw ex;
        }

        logger.debug("Connecting to Cassandra on: {} ", cassandraConnectionConfig.getConnectionPoints());

        Session session = getCassandraSession(cassandraConnectionConfig);

        try {
            executeCql(session, resourceAsStream, cqlFilename);
        } finally {
            IOUtils.closeQuietly(resourceAsStream);
            session.closeAsync();
        }

        logger.info("Finished executing [{}]", cqlFilename);
    }

    public static Session getCassandraSession(CassandraConnectionConfig cassandraConnectionConfig) throws NoHostAvailableException {
        return getCluster(cassandraConnectionConfig).connect(cassandraConnectionConfig.getKeyspace());
    }

    public static Session getCassandraSessionWithoutKeyspace(CassandraConnectionConfig cassandraConnectionConfig) throws NoHostAvailableException {
        return getCluster(cassandraConnectionConfig).connect();
    }

    private static Cluster getCluster(CassandraConnectionConfig cassandraConnectionConfig) throws NoHostAvailableException {
        logger.debug("------------------------------ getCluster: {} ", cassandraConnectionConfig);
        if (theCluster != null && !theCluster.isClosed()) {
            return theCluster;
        }

        ConsistencyLevel level = ConsistencyLevel.valueOf(cassandraConnectionConfig.getConsistencyLevel());
        // Set the default consistency level.
        QueryOptions options = new QueryOptions();
        options.setConsistencyLevel(level);

        CassandraRetryPolicy retryPolicy = new CassandraRetryPolicy();
        retryPolicy.configure(cassandraConnectionConfig.getRetryRead(), cassandraConnectionConfig.getRetryWrite(),
                cassandraConnectionConfig.getRetryHostUnavailable(), cassandraConnectionConfig.isDegradeConsistency(), 100, 10);

        theCluster = Cluster.builder()
                .addContactPoints(cassandraConnectionConfig.getArrayOfConnectionPoints())
                .withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build()))
                .withCredentials(cassandraConnectionConfig.getUsername(), cassandraConnectionConfig.getPassword())
                .withQueryOptions(options)
                .withReconnectionPolicy(retryPolicy)
                .withRetryPolicy(retryPolicy)
//                .withPoolingOptions(
//                        new PoolingOptions().setHeartbeatIntervalSeconds(50))
                .withPoolingOptions(new PoolingOptions()
                        .setConnectionsPerHost(HostDistance.LOCAL, 1, 2)
                        .setMaxRequestsPerConnection(HostDistance.LOCAL, 32768)
                        .setCoreConnectionsPerHost(HostDistance.LOCAL, 2))
                .withTimestampGenerator(new AtomicMonotonicTimestampGenerator()) // ensure ordering of update statements
                // Make unique to stop the JMX / Metrics grumbling about clashes
                .withClusterName(UUID.randomUUID().toString())
                .withMaxSchemaAgreementWaitSeconds(30)
                .build();

        TupleType tupleType = theCluster.getMetadata()
                .newTupleType(DataType.timestamp(), DataType.varchar());
        theCluster.getConfiguration().getCodecRegistry()
                .register(new ZonedDateTimeCodec(tupleType));

        Configuration clusterConfig = theCluster.getConfiguration();
        clusterConfig.getSocketOptions().setReadTimeoutMillis(240000);

        return theCluster;
    }

    /**
     * Return the CassandraRetryPolicy.
     *
     * @return the policy, or null if not connected yet.
     */
    public static CassandraRetryPolicy getRetryPolicyFor() {
        if (theCluster != null) {
            return (CassandraRetryPolicy) theCluster.getConfiguration().getPolicies().getRetryPolicy();
        }

        return null;
    }

    public static void executeCassandraCql(Session session, String cqlFilename) throws Exception {
        InputStream is = IOUtils.getResourceAsStream(cqlFilename, CassandraHelper.class.getClassLoader());
        executeCql(session, is, cqlFilename);
        IOUtils.closeQuietly(is);
    }

    private static void executeCql(Session session, InputStream stream, String cqlFilename) throws Exception {

        try (BufferedReader in = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            StringBuilder statement = new StringBuilder(100);

            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (!line.isEmpty() && !line.startsWith("--")) {
                    statement.append(" ");
                    statement.append(line);
                    if (!line.trim().endsWith(";")) {
                        // Keep looping until we find a ;
                        continue;
                    }

                    String cqlStatement = statement.toString().trim();
                    // Nuke the buffer
                    statement.setLength(0);

                    if (cqlStatement.startsWith(EXEC_COMMAND_STRING)) {
                        String tmp = line.substring(EXEC_COMMAND_STRING.length()).trim();
                        tmp = tmp.substring(0, tmp.length() - 1);
                        String className = tmp;
                        logger.debug("Class name of {} found", className);
                        CassandraEvolve evolveClass;

                        try {
                            Class<?> clazz = Class.forName(className);
                            evolveClass = (CassandraEvolve) clazz.getConstructor().newInstance();
                            evolveClass.evolve(session);
                        } catch (Throwable t) {
                            logger.warn("Unable to execute class {}", className, t);
                            throw t;
                        }
                    } else {
                        logger.debug("{}", cqlStatement);
                        session.execute(cqlStatement);
                    }
                }
            }
            IOUtils.closeQuietly(in);
        } catch (IOException ex) {
            logger.error("Error processing file: {} {}", cqlFilename, ex);
            throw ex;
        }
    }

    public static void executeCqlStatements(Session session, List<String> statements) throws Exception {
        for (String statement : statements) {
            if (statement.startsWith(EXEC_COMMAND_STRING)) {
                String tmp = statement.substring(EXEC_COMMAND_STRING.length()).trim();
                tmp = tmp.substring(0, tmp.length() - 1);
                String className = tmp;
                logger.debug("Class name of {} found", className);
                CassandraEvolve evolveClass;

                try {
                    Class<?> clazz = Class.forName(className);
                    evolveClass = (CassandraEvolve) clazz.getConstructor().newInstance();
                    evolveClass.evolve(session);
                } catch (Throwable t) {
                    logger.warn("Unable to execute class {}", className, t);
                    throw t;
                }
            } else {
                session.execute(statement);
            }
        }
    }

    public static void executeCqlMigrationStatements(Session session, List<String> statements) throws Exception {
        for (String statement : statements) {
            if (statement.startsWith(EXEC_COMMAND_STRING)) {
                String tmp = statement.substring(EXEC_COMMAND_STRING.length()).trim();
                tmp = tmp.substring(0, tmp.length() - 1);
                String className = tmp;
                logger.debug("Class name of {} found", className);
                CassandraEvolve evolveClass;

                try {
                    Class<?> clazz = Class.forName(className);
                    evolveClass = (CassandraEvolve) clazz.getConstructor().newInstance();
                    evolveClass.evolve(session);
                } catch (Throwable t) {
                    logger.warn("Unable to execute class {}", className, t);
                    throw t;
                }
            } else {
                logger.debug("statement:\n{}", statement);
                Statement st = new SimpleStatement(statement);
                st.setConsistencyLevel(ConsistencyLevel.ALL);
                session.execute(st).getExecutionInfo();
            }
        }
    }

    /**
     * Check to see if the statement was applied.
     * <p>
     * When using IF statements in updates and inserts, this checks to see if
     * the statement was applied.
     *
     * @param session
     * @param cqlStatement
     * @return true if the statement was applied.
     */
    public static boolean wasApplied(Session session, String cqlStatement) {
        ResultSet result = session.execute(cqlStatement);
        if (result == null) {
            return false;
        }

        return result.wasApplied();
    }

    public static void evolve(CassandraKeyspaceConfig cassandraKeyspaceConfig) throws CassandraEvolutionException {
        CassandraEvolution cassandraEvolution = new CassandraEvolution();
        cassandraEvolution.setConfig(cassandraKeyspaceConfig);

        try {
            // This is painless if  nothing needs to happen
            cassandraEvolution.migrate();
        } finally {
            cassandraEvolution.closeSessions();
        }
    }
}
