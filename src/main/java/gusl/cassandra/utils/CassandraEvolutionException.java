package gusl.cassandra.utils;

import gusl.core.exceptions.GUSLException;

/**
 * Cassandra Migrate Exception.
 *
 * @author dhudson
 */
public class CassandraEvolutionException extends GUSLException {

    private static final long serialVersionUID = 1L;

    public CassandraEvolutionException(String message, Throwable ex) {
        super(message, ex);
    }

    public CassandraEvolutionException(String message) {
        super(message);
    }
}
