/* Copyright lottomart */
package gusl.cassandra.application;

import gusl.cassandra.config.CassandraKeyspaceConfig;
import gusl.cassandra.config.CassandraMigrateConfig;
import gusl.cassandra.utils.CassandraEvolution;
import gusl.cassandra.utils.CassandraEvolutionException;
import gusl.core.config.ConfigUtils;
import gusl.core.utils.SystemPropertyUtils;
import gusl.core.utils.Utils;
import lombok.CustomLog;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * @author grant
 */
@CustomLog
public class CassandraMigrate {

    private static final String HELP_ARG = "h";
    private static final String DEBUG_ARG = "debug";
    private static final String CLEAN_ARG = "clean";
    private static final String KEYSPACE_ARG = "keyspace";
    private static final String MODE_ARG = "mode";
    private static final String INSTANCE_ARG = "instance";
    private static final String RERUN_INSTANCE_ARG = "rerun_instance";

    private void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("cassandra-migrate", options);
    }

    private void migrate(MigrateConfig migrateConfig) throws IOException, CassandraEvolutionException {

        if (SystemPropertyUtils.hasConfigLocation()) {
            if (SystemPropertyUtils.getGivenConfig().toLowerCase().contains("prod")) {
                logger.info("--- Production Instance  --- ");
                if (migrateConfig.isClean()) {
                    logger.warn("Can't clean a PRODUCTION database .........");
                    return;
                }
            }
        }

        logger.info("Migrate: {}", migrateConfig.toString());

        CassandraMigrateConfig config = ConfigUtils.loadConfig(CassandraMigrateConfig.class);

        if (migrateConfig.getKeySpace() == null) {
            Utils.safeStream(config.getCassandraKeyspaceConfigs()).forEach(keyspaceConfig -> {
                try {
                    migrateKeyspace(migrateConfig, keyspaceConfig);
                } catch (CassandraEvolutionException ex) {
                    logger.error("Failed to migrate {}", keyspaceConfig.getKeyspace(), ex);
                }
            });
        } else {
            CassandraKeyspaceConfig cassandraKeyspaceConfig = findCassandraKeyspaceConfig(config, migrateConfig.getKeySpace());
            try {
                migrateKeyspace(migrateConfig, cassandraKeyspaceConfig);
            } catch (CassandraEvolutionException ex) {
                logger.error("Failed to migrate {}", cassandraKeyspaceConfig.getKeyspace(), ex);
            }
        }

    }

    private void truncateAll(CassandraKeyspaceConfig cassandraKeyspaceConfig) throws CassandraEvolutionException {
        CassandraEvolution cassandraEvolution = new CassandraEvolution();
        cassandraEvolution.setConfig(cassandraKeyspaceConfig);

        cassandraEvolution.truncate();
        cassandraEvolution.insertStaticData();
    }

    private void migrateKeyspace(MigrateConfig migrateConfig, CassandraKeyspaceConfig cassandraKeyspaceConfig) throws CassandraEvolutionException {
        CassandraEvolution cassandraEvolution = new CassandraEvolution();
        cassandraEvolution.setConfig(cassandraKeyspaceConfig);
        cassandraEvolution.validateKeyspaceExists();

        if (migrateConfig.isClean()) {
            cassandraEvolution.clean();
        }

        cassandraEvolution.migrate();

        if (migrateConfig.isClean()) {
            if (migrateConfig.getInstance() != null) {
                cassandraEvolution.runInstanceScripts(migrateConfig.getInstance());
            }

            if (migrateConfig.getMode() != null) {
                cassandraEvolution.runModeScripts(migrateConfig.getMode(), migrateConfig.getInstance());
            }
        } else if (migrateConfig.isRunInstanceScripts()) {
            if (migrateConfig.getInstance() != null) {
                logger.info("Running instance scripts {}", migrateConfig.getInstance());
                cassandraEvolution.runInstanceScripts(migrateConfig.getInstance());
            }

        }

        cassandraEvolution.closeSessions();
    }

    public void setup(String... args) throws IOException, CassandraEvolutionException {
        Options options = new Options();
        options.addOption(DEBUG_ARG, "Enable debugging");
        options.addOption(HELP_ARG, "Help");
        options.addOption(CLEAN_ARG, "Clean the keyspace");
        options.addOption(KEYSPACE_ARG, true, "Optional keyspace name");
        options.addOption(MODE_ARG, true, "Casanova Operations mode");
        options.addOption(INSTANCE_ARG, true, "Casanova Instance e.g. uk, ie or com");
        options.addOption(RERUN_INSTANCE_ARG, "Run the instance scripts regardless");

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException ex) {
            logger.error("Command Line Exception " + ex.getLocalizedMessage());
            printHelp(options);
            return;
        }

        if (commandLine.hasOption(HELP_ARG)) {
            System.err.println("No arguments supplied");
            printHelp(options);
            return;
        }

        MigrateConfig migrateConfig = new MigrateConfig();

        if (commandLine.hasOption(CLEAN_ARG)) {
            migrateConfig.setClean(true);
        }

        if (commandLine.hasOption(KEYSPACE_ARG)) {
            migrateConfig.setKeySpace(commandLine.getOptionValue(KEYSPACE_ARG));
        }

        if (commandLine.hasOption(MODE_ARG)) {
            migrateConfig.setMode(commandLine.getOptionValue(MODE_ARG));
        }

        if (commandLine.hasOption(INSTANCE_ARG)) {
            migrateConfig.setInstance(commandLine.getOptionValue(INSTANCE_ARG));
        }

        if (commandLine.hasOption(RERUN_INSTANCE_ARG)) {
            migrateConfig.setRunInstanceScripts(true);
        }

        migrate(migrateConfig);

    }

    public static CassandraKeyspaceConfig findCassandraKeyspaceConfig(CassandraMigrateConfig config, String keySpace) throws CassandraEvolutionException {
        Optional<CassandraKeyspaceConfig> optional = Utils.safeStream(config.getCassandraKeyspaceConfigs()).filter(keyspaceConfig -> keyspaceConfig.getKeyspace().equals(keySpace)).findFirst();
        if (optional.isPresent()) {
            return optional.get();
        }
        throw new CassandraEvolutionException("Failed to find cassandra keyspace config for keyspace: " + keySpace);
    }

//    public static CassandraKeyspaceConfig findCassandraKeyspaceConfig(CassandraMigrateConfig config, String keySpace) throws CassandraEvolutionException {
//        Optional<CassandraKeyspaceConfig> optional = Utils.safeStream(config.getCassandraKeyspaceConfigs()).filter(keyspaceConfig -> keyspaceConfig.getKeyspace().equals(keySpace.toLowerCase())).findFirst();
//        if (optional.isPresent()) {
//            return optional.get();
//        }
//        throw new CassandraEvolutionException("Failed to find cassandra keyspace config for keyspace: " + keySpace);
//    }

    private class MigrateConfig {

        private boolean clean;
        private String keySpace;
        private String mode;
        private String instance;
        private boolean runInstanceScripts;

        public String getKeySpace() {
            return keySpace;
        }

        public void setKeySpace(String keySpace) {
            this.keySpace = keySpace;
        }

        public boolean isClean() {
            return clean;
        }

        public void setClean(boolean clean) {
            this.clean = clean;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getInstance() {
            return instance;
        }

        public void setInstance(String instance) {
            this.instance = instance;
        }

        public boolean isRunInstanceScripts() {
            return runInstanceScripts;
        }

        public void setRunInstanceScripts(boolean runScripts) {
            this.runInstanceScripts = runScripts;
        }

        @Override
        public String toString() {
            return "MigrateConfig: \n" + "\tclean=" + clean + "\n\tkeySpace=" + keySpace + "\n\tmode=" + mode + "\n\tinstance=" + instance + "\n\trerun=" + runInstanceScripts;
        }

    }

    public static void main(String... args) {
        // Lets create the Log Directory before we do anything
        String logDir = SystemPropertyUtils.getLogBase();
        if (logDir != null) {
            File file = new File(logDir);
            file.mkdirs();
        } else {
            logger.warn("System Property {} not set", SystemPropertyUtils.LOGS_LOCATION_KEY);
        }

        try {
            CassandraMigrate cassandraMigrate = new CassandraMigrate();
            cassandraMigrate.setup(args);
        } catch (IOException | CassandraEvolutionException ex) {
            logger.error("Failed to evolve cassandra", ex);
        }
    }
}
